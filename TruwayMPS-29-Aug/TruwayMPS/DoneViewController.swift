//
//  DoneViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 16/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class DoneViewController: UIViewController {
    
    @IBOutlet weak var DoneButton : UIButton?
    @IBOutlet var ThankYou_label : UILabel?
    var fromScreen : NSString = ""
    
    override func viewWillAppear(_ animated: Bool) {
        if fromScreen == "Card Vault"
        {
            DoneButton?.setTitle("DONE", for: UIControlState.normal)
            DoneButton?.tag = 1
            ThankYou_label?.isHidden = true
        }
        else{
            DoneButton?.tag = 0
            ThankYou_label?.isHidden = false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
 //MARK: Start App Button Click
    @IBAction func StartApp()
    {
        if DoneButton?.tag == 1
        { //From card Vault screen
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let CardVaultVC = storyboard.instantiateViewController(withIdentifier: "CardVaultVC") as! CardVaultVC
            CardVaultVC.fromScreen = "CardAddedScreen"
            self.present(CardVaultVC, animated:false, completion:nil)
        }
        
        else{ // from verification screen
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.present(SWRevealViewController, animated:false, completion:nil)  }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
