//
//  ViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 05/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KeychainAccess
import Firebase

class ViewController: UIViewController, UIScrollViewDelegate, apiManagerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var Register_Btn: UIButton!
    @IBOutlet var SignIn_Btn: UIButton!
    var appConstants: AppConstants = AppConstants()
    var indicator = UIActivityIndicatorView()
    var apiManager : ApiManager = ApiManager()
    var appDelegate : AppDelegate = AppDelegate()
    var countryNameArray : NSMutableArray = []
    var countryCodeArray : NSMutableArray = []
    
    override func viewWillAppear(_ animated: Bool) {
        
        Register_Btn.isHidden = true
        SignIn_Btn.isHidden = true
        Register_Btn.layer.cornerRadius = 10.0
        SignIn_Btn.layer.cornerRadius = 10.0
        
        let bundleID = Bundle.main.bundleIdentifier!
        let keychain = Keychain(service: bundleID)
        print(keychain)
        
        let token : String = try! keychain.get(bundleID)!
        print("token : \(token)")
        print(keychain)
       appConstants.defaults.set(token, forKey: "deviceID")
        
        //Call Api to get App Status
        GetAppStatusApiCall()
        appConstants.defaults.set("", forKey: "country")
        appConstants.defaults.set("", forKey: "code")
        appConstants.defaults.set("", forKey: "mobile")
        appConstants.defaults.set("", forKey: "email")
        appConstants.defaults.set("", forKey: "dob")
        appConstants.defaults.set("", forKey: "password")
        appConstants.defaults.set("", forKey: "confirm_password")
        appConstants.defaults.set("", forKey: "countryList")
    }
    //MARK: To get app active status
    func GetAppStatusApiCall()
    {
      var device_id = appConstants.defaults.value(forKey: "deviceID")
        print("device_id from keychain \(String(describing: device_id))")
        if(device_id == nil)
        {
        device_id = UIDevice.current.identifierForVendor!.uuidString
        appConstants.defaults.set(device_id, forKey: "deviceID")
        }
    self.apiManager.ApiAppStatusCall(action:"activeAppStatus",device_id:device_id as! String)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiManager = ApiManager()
        apiManager.delegate = self
        
        self.scrollView.frame = CGRect(x:0, y:0, width:appConstants.screenWidth, height:appConstants.screenHeight)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        //2
        
        //3
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgOne.image = UIImage(named: "slide1")
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "slide2")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "slide3")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
                //4
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * 3, height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func moveToNextPage (){
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
    }
    //MARK: UIScrollView Delegate
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        
           }
    //To Move to Registration Page
    
@IBAction  func moveToRegistrationPage(sender: UIButton) {
        self.apiManager.ApiCountryListCall()
        }
    
    @IBAction  func moveToSignInPage(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let LoginScreenViewController = storyboard.instantiateViewController(withIdentifier: "LoginScreenViewController") as! LoginScreenViewController
        self.present(LoginScreenViewController, animated:false, completion:nil)
    }
    
    
//MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        let logInStatus = appConstants.defaults.value(forKey: "logInStatus")
        
        for (key, value) in response {
            print("key=\(key), value=\(value)")
           if let responseArray : NSArray = value as? NSArray
           {
            for dict in responseArray {
                print(dict)
                
                let mainList:NSDictionary = dict as! NSDictionary
                print("mainList = \(mainList)")
                let countryName : NSString = mainList .value(forKey: "countryName") as! NSString
                let countryCode : NSString = mainList .value(forKey: "countryCode") as! NSString
                countryNameArray.add(countryName)
                countryCodeArray.add(countryCode)
            }
            //MARK: After getting response move to registration page
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let RegisterPageViewController = storyboard.instantiateViewController(withIdentifier: "RegisterPageViewController") as! RegisterPageViewController
            RegisterPageViewController.CodeArrayList = countryCodeArray
            RegisterPageViewController.CountryArrayList = countryNameArray
            self.present(RegisterPageViewController, animated:false, completion:nil)
            
            print(countryNameArray.count)
            print(countryCodeArray.count)
        }
            
        else{
            print("appstatusApi")
            let responseDictionary : NSDictionary = value as! NSDictionary
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                Register_Btn.isHidden = true
                SignIn_Btn.isHidden = false
            }
            else{
                SignIn_Btn.isHidden = true
                Register_Btn.isHidden = false
            }
        }
        }
    }
    
//MARK: Notification methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

