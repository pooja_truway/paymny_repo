//
//  AddToFavoritesVC.swift
//  TruwayMPS
//
//  Created by Pooja on 08/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

protocol addToFavoritesVCDelegate:NSObjectProtocol {
    
    func apiSuccessResponse(_ response : Int)
}
extension addToFavoritesVCDelegate {
    func apiSuccessResponse(_ response : Int){
        print ("hello favorite \(response)")
    }
}



let myNotificationKey = "com.bobthedeveloper.notificationKey"

class AddToFavoritesVC: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource,addToFavoritesVCDelegate {

     @IBOutlet var AddToFavoriteCollectionView : UICollectionView!
     @IBOutlet var cancelButton : UIButton!
    
    var delegate : addToFavoritesVCDelegate?
    var appConstants : AppConstants = AppConstants()
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    
    var TopCellitems = ["Pay/Receive","Scan QR Code","UPI","Card Vault","Foreign Exchange","Loyalty Offer","Electricity","DTH","Metro","Broadband","Water","Landlvar","Gas","help","Shopping","Offer Zone","Send","Receive","Utility Bill","Sign Out"]
    var TopCellImageArray = [UIImage(named:"ic_pay_receive_cell"), UIImage(named: "ic_fav_menu_qr_code"), UIImage(named: "ic_fav_menu_upi"), UIImage(named: "ic_fav_menu_card"), UIImage(named: "ic_fav_menu_foreign_exchange"), UIImage(named: "ic_loyalty_offer"),
                                UIImage(named: "ic_electricity"),UIImage(named: "ic_fav_dth"),UIImage(named: "ic_fav_metro"),UIImage(named: "ic_fav_broadband"),UIImage(named: "ic_fav_water"),UIImage(named: "ic_fav_landline"),UIImage(named: "ic_fav_gas"),UIImage(named: "ic_help"),UIImage(named: "ic_fav_menu_shopping"),UIImage(named: "ic_fav_menu_offer"),UIImage(named: "ic_fav_menu_send_money"),
                                UIImage(named: "ic_fav_menu_request_money"),UIImage(named: "ic_utility"),UIImage(named: "ic_sign_out")]

    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        AddToFavoriteCollectionView.layer.cornerRadius = 5.0
    }
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        	NotificationCenter.default.addObserver(self, selector: #selector(doThisWhenNotify), name: NSNotification.Name(rawValue: myNotificationKey), object: nil)

        // Do any additional setup after loading the view.
    }

    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.TopCellImageArray.count
          }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
            // get a reference to our storyboard cell
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! TopCollectionViewCell
            
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
            cellA.TopCellTitleLabel.text = self.TopCellitems[indexPath.item]
            cell = cellA
            cellA.TopCellImage.image = self.TopCellImageArray[indexPath.item]
            return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
       print("You selected cell #\(indexPath.item)!")
         appConstants.defaults.set(indexPath.row, forKey: "itemIndex")
     
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: myNotificationKey), object: nil)
        
       
        
        
   //     self.delegate?.apiSuccessResponse(indexPath.item)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//        MainScreenViewController.itemsToAdded = self.TopCellitems[indexPath.item] as NSString
//        MainScreenViewController.indexSelected = (indexPath.item)
//        MainScreenViewController.fromScreen = "AddItems"
//        self.navigationController?.pushViewController(MainScreenViewController, animated: false)
        
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
   //MARK: Cancel Button Action
    @IBAction func cancelButtonClicked(sender: UIButton)
    {    print("cancel pressed")
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    func doThisWhenNotify() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
         MainScreenViewController.indexSelected = 2
        print("I've successfully sent a spark!")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
