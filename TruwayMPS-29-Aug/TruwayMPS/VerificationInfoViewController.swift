//
//  VerificationInfoViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 15/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class VerificationInfoViewController: UIViewController,UITableViewDelegate, UITableViewDataSource ,UITextFieldDelegate ,UIScrollViewDelegate , apiManagerDelegate{
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var backgrounf_image : UIImageView!
    
    @IBOutlet var DropDownButton1 : UIButton!
    @IBOutlet var DropDownButton2 : UIButton!
    @IBOutlet var DropDownButton3 : UIButton!
    @IBOutlet var FirstQuestion_textField : UITextField?
    @IBOutlet var SecondQuestion_textField : UITextField?
    @IBOutlet var ThirdQuestion_textField : UITextField?
    @IBOutlet var FourthQuestion_textField : UITextField?
    
    @IBOutlet var FirstAnswer_textField : UITextField?
    @IBOutlet var SecondAnswer_textField : UITextField?
    @IBOutlet var ThirdAnswer_textField : UITextField?
    @IBOutlet var FourthAnswer_textField : UITextField?
    @IBOutlet var BackButton : UIButton?
    
    var questn1 : String = ""
    var ans1 : String = ""
    var questn2 :String = ""
    var ans2 : String = ""
    var questn3 :String = ""
    var ans3 : String = ""
    var questn4 : String = ""
    var ans4 : String = ""
    var MessageString : String = ""
    
    var FirstQuestion_tableView: UITableView  =   UITableView()
    var SecondQuestion_tableView: UITableView  =   UITableView()
    var ThirdQuestion_tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    let FirstQuestionArrayList : NSArray = ["What is the name of your favorite childhood friend?","What is the name of your favorite teacher's name?","What is the name of your pet?","What is your favorite movie?","What was your favorite sport in high school?","What was the make and model of your first car?","In which town was your first job?","Who is your first teacher?"]
    let QuestionIdList : NSArray = ["1","2","3","4","5","6","7","8"]
    
  //MARK: ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        //Hide/unhide back button
        if let manageBackButton : NSString = appConstants.defaults.value(forKey: "BackHidden") as? NSString
        {
        if (manageBackButton == "FromLogin")
        {
         BackButton?.isHidden = true
        }
        else{
             BackButton?.isHidden = false
            }
        }
        
        //MARK: First Table
        FirstQuestion_tableView = UITableView(frame: CGRect(x: 20, y: 150, width: appConstants.screenWidth-40, height: 300))
        FirstQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.FirstQuestion_tableView.dataSource = self
        self.FirstQuestion_tableView.delegate = self
        self.view.addSubview(FirstQuestion_tableView)
        FirstQuestion_tableView.backgroundColor = UIColor.white
        FirstQuestion_tableView.isHidden = true
        FirstQuestion_tableView.tag = 1
        
        //MARK: Second Table
        
        SecondQuestion_tableView = UITableView(frame: CGRect(x: 20, y: 250, width: appConstants.screenWidth-40, height: 300))
        SecondQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.SecondQuestion_tableView.dataSource = self
        self.SecondQuestion_tableView.delegate = self
        self.view.addSubview(SecondQuestion_tableView)
        SecondQuestion_tableView.backgroundColor = UIColor.white
        SecondQuestion_tableView.isHidden = true
        SecondQuestion_tableView.tag = 2
        
        //MARK: Third Table
        ThirdQuestion_tableView = UITableView(frame: CGRect(x: 30, y: 350, width: appConstants.screenWidth-40, height: 300))
        ThirdQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.ThirdQuestion_tableView.dataSource = self
        self.ThirdQuestion_tableView.delegate = self
        self.view.addSubview(ThirdQuestion_tableView)
        ThirdQuestion_tableView.backgroundColor = UIColor.white
        ThirdQuestion_tableView.isHidden = true
        ThirdQuestion_tableView.tag = 3
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        // Do any additional setup after loading the view.
    }
    
    //MARK: Show Question 1  list
    @IBAction func GetFirstQuestionListAction()
    {
        FirstQuestion_tableView.isHidden = false
        scrollView.isUserInteractionEnabled = false
    }
    
    //MARK: Show Question 2
    @IBAction func GetSecondQuestionListAction()
    {
        SecondQuestion_tableView.isHidden = false
        scrollView.isUserInteractionEnabled = false
    }
    
    //MARK: Show Question 3
    @IBAction func GetThirdQuestionListAction()
    {
        ThirdQuestion_tableView.isHidden = false
        scrollView.isUserInteractionEnabled = false
    }
    
    //MARK: Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirstQuestionArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FirstQuestion_tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(FirstQuestionArrayList[indexPath.row])"
        cell.textLabel!.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        scrollView.isUserInteractionEnabled = true
        if(tableView.tag == 1){
            FirstQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn1 = (FirstQuestion_textField?.text)!
            FirstQuestion_tableView.isHidden = true
            FirstAnswer_textField?.becomeFirstResponder()
        }
        else if(tableView.tag == 2)
        {
            SecondQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn2 = (SecondQuestion_textField?.text)!
            SecondQuestion_tableView.isHidden = true
            SecondAnswer_textField?.becomeFirstResponder()
        }
        else{
            ThirdQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn3 = (ThirdQuestion_textField?.text)!
            ThirdQuestion_tableView.isHidden = true
            ThirdAnswer_textField?.becomeFirstResponder()
        }
    }
    
    //MARK: TextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print(textField.tag)
        switch (textField.tag)
        {
        case 1:
            if(FirstQuestion_textField?.text == ""){
            // Question 1
            FirstAnswer_textField?.isUserInteractionEnabled = false
            MessageString = " Please select a question first"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                FirstAnswer_textField?.isUserInteractionEnabled = true
            }
            break
                case 3:
            // Question 2
                    if(SecondQuestion_textField?.text == ""){
                    SecondQuestion_textField?.isUserInteractionEnabled = false
                    MessageString = " Please select a question first"
                        appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                    }
                    else{
                        SecondQuestion_textField?.isUserInteractionEnabled = true
                    }
            break
        case 5:
            // Question 3
            if(ThirdQuestion_textField?.text == ""){
            ThirdQuestion_textField?.isUserInteractionEnabled = false
            MessageString = " Please select a question first"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                ThirdQuestion_textField?.isUserInteractionEnabled = true
            }
            break
        case 7:
            // Question
             break
                default:
             break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK: CONTINUE BUTTON CLICK - Hit webservice
    @IBAction func ContinueButtonClick()
    {
        
        print(questn2)
        print(ans2)
        ans1 = (FirstAnswer_textField?.text)!
        ans2 = (SecondAnswer_textField?.text)!
        ans3 = (ThirdAnswer_textField?.text)!
        ans4 = (FourthAnswer_textField?.text)!
        questn4 = (FourthQuestion_textField?.text)!
        
        if(questn1 == "")||(ans1 == "")
        {
        MessageString = " Please select first security question/answer"
        appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn2 == "")||(ans2 == "")
        {
            MessageString = " Please select second security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn3 == "")||(ans3 == "")
        {
            MessageString = " Please select third security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn4 == "")||(ans4 == "")
        {
            MessageString = " Please select fourth security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
            
            else if(questn1 == questn2)||(questn2 == questn3)||(questn1 == questn3)
        {
            MessageString = " Please select different set of security questions"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else{
            VerificationInfoApiCall()
        }

    }
//MARK: API Call
    func VerificationInfoApiCall()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        
        self.apiManager.ApiVerificationDetailCall (action:"verificationDetails",userId:userId as! String,q1:questn1 ,a1:ans1,q2:questn2,a2:ans2,q3:questn3,a3:ans3,q4:questn4,a4:ans4)
    }
    
//MARK: To get Api Success Response
func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let DoneViewController = storyboard.instantiateViewController(withIdentifier: "DoneViewController") as! DoneViewController
                self.present(DoneViewController, animated:false, completion:nil)
            }
            else{
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
        }
    }

    
//MARK: Back Button Action
    @IBAction func BackButtonClick()
    {
     self.dismiss(animated: false, completion: {})
    }
}
