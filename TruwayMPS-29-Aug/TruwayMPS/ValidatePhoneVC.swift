//
//  ValidatePhoneVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Pooja. All rights reserved.
//

import UIKit

class ValidatePhoneVC: UIViewController {
    
    var fromScreen : NSString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Button Action
    @IBAction func BackButtonClick()
    {
        self.dismiss(animated: false, completion: {})
    }
    
    //MARK: Continue Button clicked
    @IBAction func ContinueClicked (sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let DoneViewController = storyboard.instantiateViewController(withIdentifier: "DoneViewController") as! DoneViewController
        DoneViewController.fromScreen = "Card Vault"
        self.present(DoneViewController, animated:false, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
