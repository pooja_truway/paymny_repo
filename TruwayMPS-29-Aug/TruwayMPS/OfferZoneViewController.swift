//
//  OfferZoneViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 25/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import SDWebImage

class OfferZoneViewController: UIViewController,UITabBarDelegate,UITableViewDelegate,UITableViewDataSource,apiManagerDelegate {
    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var menuButton:UIBarButtonItem!
     @IBOutlet weak var tableView: UITableView?
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    var fromScreen : NSString = ""
    
    let offerImageArray : NSMutableArray =  [UIImage(named:"ic_offer1"), UIImage(named: "ic_offer2"), UIImage(named: "ic_offer3")]
    
    var offerImageArr : NSMutableArray = []
    var offerMessage : NSMutableArray = []

    override func viewWillAppear(_ animated: Bool) {
        print("Offer Zone Page opened")
        
        if(fromScreen == "Main")
        {
            tabBar.isHidden = true
        }
        else{
        tabBar.selectedItem = tabBar.items![2] as UITabBarItem
        }
        
        getOffers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 250
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.apiManager = ApiManager()
        apiManager.delegate = self
        // Do any additional setup after loading the view.
    }
    //MARK: Hit API to get offer list
    func getOffers()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiOfferZoneList (action:"offerzone",userId:userId as! String)
    }
    
    //MARK: - Tab Bar Delegate- called when user changes tab.
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0)
        {
            print("Home tab is selected")
            OpenHomePage()
        }
        else if(item.tag == 1)
        {
            print("Offer Zone tab is selected")
            OpenProfilePage()
        }
        else if(item.tag == 3)
        {
            print("Notification tab is selected")
            OpenNotificationPage()
        }
        else if(item.tag == 4)
        {
            print("Notification tab is selected")
            OpenForeignExchangePage()
        }
        
    }
    
    func OpenHomePage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        self.navigationController?.pushViewController(MainScreenViewController, animated: false)
    }
    
    func OpenProfilePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(ProfileViewController, animated: false)
    }
    
    func OpenNotificationPage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let NotificationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    func OpenForeignExchangePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForeignExchangeViewController = storyboard.instantiateViewController(withIdentifier: "ForeignExchangeViewController") as! ForeignExchangeViewController
        self.navigationController?.pushViewController(ForeignExchangeViewController, animated: false)
    }

    
    // MARK:  TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return offerImageArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! OfferZoneCell!
      
        let imgURL : NSString = offerImageArr[indexPath.row]
            as! NSString
        let baseString : NSString = "https://"
        let imageUrl : NSString = "\(baseString)\(imgURL)" as NSString
        
        let url = URL.init(string: imageUrl as String)
        
        cell?.offerBannerImage?.sd_setImage(with: url , placeholderImage: nil)
        
        cell?.textMessage?.text = offerMessage[indexPath.row] as! String
        cell?.offerBannerImage?.layer.cornerRadius = 5.0
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 310.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if(indexPath.row == 0)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let WebOfferViewController = storyboard.instantiateViewController(withIdentifier: "WebOfferViewController") as! WebOfferViewController
            self.navigationController?.pushViewController(WebOfferViewController, animated: false)
        }
        else if(indexPath.row == 1)
        {
            let MessageString = "Copied text : PayMnyPromoCode"
            appConstants.showAlert(title: "", message: MessageString as String, controller: self)
        }
        
        print(indexPath.row)
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            appConstants.hideLoadingHUD(for_view: self.view)
            if let responseArray : NSArray = value as? NSArray
            {
                print("responseArray : \(responseArray)")
                
               // let responseDict : NSDictionary = (responseDictionary .value(forKey: "0") as? NSDictionary)!
                
            //    print("responseDict : \(responseDict)")
                
                for dict in responseArray
                {
                    let dict1 : NSDictionary = dict as! NSDictionary
                    if let image_name : NSString = dict1.value(forKey: "image_name") as? NSString
                    {
                        print("image_name: \(image_name)")
                        
                        offerImageArr.insert(image_name, at: 0)
                    }
                    
                    if let messageText : NSString = dict1.value(forKey : "message") as? NSString
                    {
                        offerMessage.insert(messageText, at: 0)
                    }
                    
                    tableView?.reloadData()
                }
              offerImageArr = offerImageArr.reversed() as! NSMutableArray
              offerMessage =  offerMessage.reversed() as! NSMutableArray
                
              }
            else{
                
                }
        }
    }

       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
