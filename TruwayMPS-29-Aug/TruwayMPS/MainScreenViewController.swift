//
//  MainScreenViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 23/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class MainScreenViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource,UIScrollViewDelegate,UITabBarDelegate, UIGestureRecognizerDelegate{
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var TopCollectionView : UICollectionView!
    @IBOutlet var MiddleCollectionView : UICollectionView!
    @IBOutlet var BottomCollectionView : UICollectionView!
    
    var fromScreen : NSString = ""
    var indexSelected : Int = 0
    var TopCellitems : NSMutableArray = ["Pay/Receive","","","","","","",""]
    private var isMenuOpen = false
    
    @IBOutlet var ImageSliderView: UIScrollView!
     var appConstants: AppConstants = AppConstants()
     var addToFavoritesVC : AddToFavoritesVC = AddToFavoritesVC()
    
    let TopCellitemsAdded : NSMutableArray = ["Pay/Receive","Scan QR Code","UPI","Card Vault","Foreign Exchange","Loyalty Offer","Electricity","DTH","Metro","Broadband","Water","Landlvar","Gas","help","Shopping","Offer Zone","Send","Receive","Utility Bill","Sign Out"]
    
    let FavoritrCellImageArray : NSMutableArray =  [UIImage(named:"ic_pay_receive_cell"), UIImage(named: "ic_fav_menu_qr_code"), UIImage(named: "ic_fav_menu_upi"), UIImage(named: "ic_fav_menu_card"), UIImage(named: "ic_fav_menu_foreign_exchange"), UIImage(named: "ic_loyalty_offer"),
                                                    UIImage(named: "ic_electricity"),UIImage(named: "ic_fav_dth"),UIImage(named: "ic_fav_metro"),UIImage(named: "ic_fav_broadband"),UIImage(named: "ic_fav_water"),UIImage(named: "ic_fav_landline"),UIImage(named: "ic_fav_gas"),UIImage(named: "ic_help"),UIImage(named: "ic_fav_menu_shopping"),UIImage(named: "ic_fav_menu_offer"),UIImage(named: "ic_fav_menu_send_money"),
                                                    UIImage(named: "ic_fav_menu_request_money"),UIImage(named: "ic_utility"),UIImage(named: "ic_sign_out")]
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        if fromScreen == "SEND PAYMENT SUCCESSFUL"{
            print("FROMSCREEN SEND PAYMENT")
        }
        
        print("Home Screen itemsImageToAdded \(indexSelected)")
        if(fromScreen == "AddItems")
        {
        AddItems()
        }
        else{
          // Perform normal flow
        }
        tabBar.selectedItem = tabBar.items![0] as UITabBarItem
    }
     //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.TopCollectionView.addGestureRecognizer(lpgr)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doSomethingAfterNotified),
                                               name: NSNotification.Name(rawValue: myNotificationKey),
                                               object: nil)
        
        
        //MARK: - To handle side menu bar
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 270
            
            //MARK: To handle screen after send/request payment 
            
            print("hello \(revealViewController().fromScreen)")
            if let screen = revealViewController().fromScreen
            {
                if screen == "SEND PAYMENT SUCCESSFUL"
                {
                    print("show success pop up")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let ResponsePopUpViewController = storyboard.instantiateViewController(withIdentifier: "ResponsePopUpViewController") as! ResponsePopUpViewController
                    ResponsePopUpViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
                    ResponsePopUpViewController.fromScreen = "send Payment"
                    self.addChildViewController(ResponsePopUpViewController)
                    self.view.addSubview(ResponsePopUpViewController.view)
                    ResponsePopUpViewController.didMove(toParentViewController: self)
                }
                    
                    
                    else if( screen == "SEND PAYMENT FAILED")
                {
                        print("show failure pop up")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let ResponsePopUpViewController = storyboard.instantiateViewController(withIdentifier: "ResponsePopUpViewController") as! ResponsePopUpViewController
                        ResponsePopUpViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
                        ResponsePopUpViewController.fromScreen = "Send Payment Failed"
                        self.addChildViewController(ResponsePopUpViewController)
                        self.view.addSubview(ResponsePopUpViewController.view)
                        ResponsePopUpViewController.didMove(toParentViewController: self)
                }
                else if (screen == "SEND REQUEST SUCCESSFUL"){
                     print("show payment requested screen")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let ResponsePopUpViewController = storyboard.instantiateViewController(withIdentifier: "ResponsePopUpViewController") as! ResponsePopUpViewController
                    ResponsePopUpViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
                    ResponsePopUpViewController.fromScreen = "request Payment"
                    self.addChildViewController(ResponsePopUpViewController)
                    self.view.addSubview(ResponsePopUpViewController.view)
                    ResponsePopUpViewController.didMove(toParentViewController: self)
                }
                
                else if (screen == "SEND REQUEST FAILED"){
                    print("show payment requested screen")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let ResponsePopUpViewController = storyboard.instantiateViewController(withIdentifier: "ResponsePopUpViewController") as! ResponsePopUpViewController
                    ResponsePopUpViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
                    ResponsePopUpViewController.fromScreen = "request Payment Failed"
                    self.addChildViewController(ResponsePopUpViewController)
                    self.view.addSubview(ResponsePopUpViewController.view)
                    ResponsePopUpViewController.didMove(toParentViewController: self)
                }
            }
            else{
                 print("screen value is nil")
               }
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
            tapRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
            self.view.addGestureRecognizer(tapRecognizer)
        }
       //MARK: - Slider Image
            self.ImageSliderView.frame = CGRect(x:0, y:80, width:appConstants.screenWidth, height:161)
            let ImageSliderViewWidth:CGFloat = self.ImageSliderView.frame.width
            let ImageSliderViewHeight:CGFloat = self.ImageSliderView.frame.height
        
            let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:ImageSliderViewWidth, height:ImageSliderViewHeight))
            imgOne.image = UIImage(named: "ic_slider1")
            let imgTwo = UIImageView(frame: CGRect(x:ImageSliderViewWidth, y:0,width:ImageSliderViewWidth, height:ImageSliderViewHeight))
            imgTwo.image = UIImage(named: "ic_slider2")
        
            self.ImageSliderView.addSubview(imgOne)
            self.ImageSliderView.addSubview(imgTwo)
           
            self.ImageSliderView.contentSize = CGSize(width:self.ImageSliderView.frame.width * 2, height:self.ImageSliderView.frame.height)
            self.ImageSliderView.delegate = self
           // self.pageControl.currentPage = 0
            
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        print("viewDidLoad")
     }
    //MARK: - To scroll image
    func moveToNextPage (){
        let pageWidth:CGFloat = self.ImageSliderView.frame.width
        let maxWidth:CGFloat = pageWidth * 2
        let contentOffset:CGFloat = self.ImageSliderView.contentOffset.x
        var slideToX = contentOffset + pageWidth
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        
    self.ImageSliderView.scrollRectToVisible(CGRect(x:slideToX, y:80, width:pageWidth, height:self.ImageSliderView.frame.height), animated: true)
    }
    //MARK: UIScrollView Delegate
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((ImageSliderView.contentOffset.x-pageWidth/2)/pageWidth)+1
          }
//MARK: Get notified when favorite item is selected from list
func doSomethingAfterNotified() {
        appConstants.defaults.value(forKey: "itemIndex")
        fromScreen = "AddItems"
        viewWillAppear(true)
    }
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
   
    
   
    var TopCellImageArray : NSMutableArray = [UIImage(named: "ic_pay_receive_cell")!,
                      UIImage(named: "ic_add_icon")!,
                      UIImage(named: "ic_add_icon")!,
                      UIImage(named: "ic_add_icon")!,UIImage(named: "ic_add_icon")!,UIImage(named: "ic_add_icon")!,UIImage(named: "ic_add_icon")!,UIImage(named: "ic_add_icon")!]
    
     var MiddleCellItems = ["Mobile","Electricity","DTH","Metro","Broadband","Water","Landline","Gas"]
     var MiddleCellImageArray = [UIImage(named:"ic_mobile-1"),
         UIImage(named: "ic_electricity"),UIImage(named: "ic_dth"),UIImage(named: "ic_metro"),UIImage(named: "ic_broadband"),UIImage(named: "ic_water"),UIImage(named: "ic_landline"),UIImage(named: "ic_gas")]
    
    
     let BottomCellImageArray = [UIImage(named: "ic_loyalty_cell"),
                             UIImage(named: "ic_offer_cell"),
                             UIImage(named: "ic_shopping_cell")]
     var BottomCellItems = ["Loyalty Offers","Offer Zone","Shopping History"]
    
    var FavoriteCellItems = ["Mobile","Electricity","DTH","Metro","Broadband","Water","Landline","Gas"]
    var FavoriteCellImageArray = [UIImage(named:"ic_mobile-1"),
                                UIImage(named: "ic_electricity"),UIImage(named: "ic_dth"),UIImage(named: "ic_metro"),UIImage(named: "ic_broadband"),UIImage(named: "ic_water"),UIImage(named: "ic_landline"),UIImage(named: "ic_gas")]
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == 0){
            return 8
        }
        else if(collectionView.tag == 1)
        {
        return self.MiddleCellItems.count
        }
        else{
        return self.BottomCellImageArray.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if(collectionView.tag == 0){
        // get a reference to our storyboard cell
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! TopCollectionViewCell
            
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cellA.TopCellTitleLabel.text = TopCellitems[indexPath.item] as? String
            cell = cellA
        cellA.TopCellImage.image = self.TopCellImageArray[indexPath.item] as? UIImage
    // cellA.TopCellImage.image = UIImage(named:"ic_add_icon")
        }
        else if (collectionView.tag == 1)
        {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MiddleCollectionViewCell
           
            cellB.MiddleCellTitleLabel.text = self.MiddleCellItems[indexPath.item]
             cellB.MiddleCellImage.image = self.MiddleCellImageArray[indexPath.item]
            cell = cellB
        }
        else{
            let cellC = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! BottomCollectionViewCell
            
            cellC.BottomCellImage.image = self.BottomCellImageArray[indexPath.item]
            cellC.BottomCellTitleLabel.text = self.BottomCellItems[indexPath.row]
            cell = cellC
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
         if(collectionView.tag == 0)
         {
        let str : String = TopCellitems.object(at: indexPath.item) as! String
        print(str)
        if str == ""
        {
            AddItemToRow(indexPath.item)
            appConstants.defaults.set(indexPath.item, forKey: "selectedCell")
            appConstants.defaults.synchronize()
         }
        else{
            print("already selected")
            //Open corresponding page
        }
    }
        
        else if(collectionView.tag == 1)
         {
            if(indexPath.item == 0)
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let MobileBillViewController = storyboard.instantiateViewController(withIdentifier: "MobileBillViewController") as! MobileBillViewController
                MobileBillViewController.fromScreen = "Main"
                self.present(MobileBillViewController, animated: false, completion: nil)
            }
            if(indexPath.item == 1)
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ElectricityBillViewController = storyboard.instantiateViewController(withIdentifier: "ElectricityBillViewController") as! ElectricityBillViewController
                ElectricityBillViewController.fromScreen = "Main"
                self.present(ElectricityBillViewController, animated: false, completion: nil)
            }
            
            if(indexPath.item == 2)
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let DTHViewController = storyboard.instantiateViewController(withIdentifier: "DTHViewController") as! DTHViewController
                DTHViewController.fromScreen = "Main"
                self.present(DTHViewController, animated: false, completion: nil)
            }


        }
        
        else if(collectionView.tag == 2)
         {
            if(indexPath.item == 1)
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let OfferZoneViewController1 = storyboard.instantiateViewController(withIdentifier: "OfferZoneViewController1") as! OfferZoneViewController1
                OfferZoneViewController1.fromScreen = "Main"
               // self.present(OfferZoneViewController, animated: false, completion: nil)
                self.navigationController?.pushViewController(OfferZoneViewController1, animated: false)
               
            }
        }
}
    
    //MARK: - Tab Bar Delegate- called when user changes tab.
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0)
        {
            print("Home tab is selected")
        }
      else if(item.tag == 1)
       {
        print("profile tab is selected")
        OpenProfilePage()
        }
        else if(item.tag == 2)
       {
        print("Offer Zone tab is selected")
        OpenOfferZonePage()
        }
        else if(item.tag == 3)
       {
        print("Notification tab is selected")
        }
        else if(item.tag == 4)
        {
            print("Foreign Exchange tab is selected")
            OpenForeignExchangePage()
        }
    }
   
    //MARK: Open Profile Page
    func OpenProfilePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(ProfileViewController, animated: false)
    }
    
    //MARK: Open Offer Zone Page
    func OpenOfferZonePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OfferZoneViewController = storyboard.instantiateViewController(withIdentifier: "OfferZoneViewController") as! OfferZoneViewController
        self.navigationController?.pushViewController(OfferZoneViewController, animated: false)
    }
    
    func OpenForeignExchangePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForeignExchangeViewController = storyboard.instantiateViewController(withIdentifier: "ForeignExchangeViewController") as! ForeignExchangeViewController
        self.navigationController?.pushViewController(ForeignExchangeViewController, animated: false)
    }
 //MARK: To add items
    func AddItemToRow(_: Int)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let AddToFavoritesVC = storyboard.instantiateViewController(withIdentifier: "AddToFavoritesVC") as! AddToFavoritesVC
        AddToFavoritesVC.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(AddToFavoritesVC)
        self.view.addSubview(AddToFavoritesVC.view)
        AddToFavoritesVC.didMove(toParentViewController: self)
    }
    
    //MARK: Add Items to array of favorites
    func AddItems ()
    { // Index of item selected to be in favorite list
        if let index = appConstants.defaults.value(forKey: "itemIndex")
    {
        let itemToInserted = TopCellitemsAdded.object(at: index as! Int)
        let ImageToInserted = FavoritrCellImageArray.object(at: index as! Int)
        print(itemToInserted)
        
        //Index of selected top view collection cell
        let selectedCell : Int = appConstants.defaults.value(forKey: "selectedCell") as! Int
        print(selectedCell)
        
        TopCellitems.replaceObject(at: selectedCell, with: (itemToInserted as! String) as String) // For label
        
        TopCellImageArray.replaceObject(at: selectedCell, with: (ImageToInserted as? UIImage)!)
        
        print("TopCellitems favourites array \(TopCellitems)")
    }
        TopCollectionView.reloadData()
    }

//MARK: Long Gesture Action
func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
    print("long gesture tapped")
        if gestureReconizer.state != UIGestureRecognizerState.ended {
            return
        }
        let p = gestureReconizer.location(in: self.TopCollectionView)
        let indexPath = self.TopCollectionView.indexPathForItem(at: p)
        
        if let index = indexPath {
         //   var cell = self.TopCollectionView.cellForItem(at: index)
            // do stuff with your cell, for example print the indexPath
            
            let index1 : Int = index.row
            print(index.row)
            AddItemToRow(index1)
            appConstants.defaults.set(index1, forKey: "selectedCell")
            appConstants.defaults.synchronize()
        } else {
            print("Could not find index path")
        }
    }
    
    
    func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.ended {
            let revealController = self.revealViewController()
            
            if isMenuOpen {
                revealController?.rearViewRevealWidth = 50.0
            } else {
                revealController?.rearViewRevealWidth = 600.0
            }
            
            revealController?.revealToggle(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
