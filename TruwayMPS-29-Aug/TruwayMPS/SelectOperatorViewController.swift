//
//  SelectOperatorViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 21/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SelectOperatorViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet var Operator_View : UIView!
    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet var Cancel_btn : UIButton!
    
    var fromScreen : NSString = ""
    
    var appConstants : AppConstants = AppConstants()
    
     let reuseIdentifier = "cell"
    var CellImageArray : NSMutableArray = []
    var CellTitleArray : NSMutableArray = []
    @IBOutlet weak var collectionViewHeightConstraint : NSLayoutConstraint?
   

    
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        Operator_View.layer.cornerRadius = 10
        Operator_View.addSubview(collectionView!)
         appConstants.defaults.set("", forKey: "operatorSelected")
        
       if fromScreen == "Mobile Bill"
       {
        CellImageArray = [
            UIImage(named: "ic_airtel"),UIImage(named: "ic_vodafone"),UIImage(named: "ic_idea"),UIImage(named: "ic_jio"),UIImage(named: "ic_bsnl"),UIImage(named: "ic_mtnl"),UIImage(named: "ic_aircel"),UIImage(named: "ic_tata")]
        
         CellTitleArray  =  ["AIRTEL","VODAFONE","IDEA","JIO","BSNL","MTNL","AIRCEL","TATA DOCOMO"]
        collectionViewHeightConstraint?.constant = 280
        }
        else if(fromScreen == "Electricity")
       {
        CellImageArray = [
            UIImage(named: "ic_bses"),UIImage(named: "ic_tatapower")]
        
        CellTitleArray  =  ["BSES","TATA POWER"]
        collectionViewHeightConstraint?.constant = 100
        }
        
       else if(fromScreen == "DTH")
       {
        CellImageArray = [
            UIImage(named: "ic_airteldigital"),UIImage(named: "ic_tatasky"),UIImage(named: "ic_videocon"),UIImage(named: "ic_reliancedigital"),UIImage(named: "ic_dish")]
        
        CellTitleArray  =  ["AIRTEL DIGITAL","TATASKY","VIDEOCON","RELIANCE DIGITAL","DISH TV"]
        }
        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return CellImageArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! OperatorCell
        
        cell = cellA
        cellA.imageView?.image = self.CellImageArray[indexPath.item] as? UIImage
       cellA.titleLabel?.text = self.CellTitleArray[indexPath.item] as? String
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
         appConstants.defaults.set(indexPath.row, forKey: "operatorSelected")
        print("You selected cell #\(indexPath.item)!")
        
        if(fromScreen == "Mobile Bill")
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MobileBillViewController = storyboard.instantiateViewController(withIdentifier: "MobileBillViewController") as! MobileBillViewController
        MobileBillViewController.operatorString = (CellTitleArray.object(at: indexPath.item) as? NSString)!
        MobileBillViewController.index = indexPath.item
        self.present(MobileBillViewController, animated: false, completion: nil)
        }
        else if(fromScreen == "Electricity")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ElectricityBillViewController = storyboard.instantiateViewController(withIdentifier: "ElectricityBillViewController") as! ElectricityBillViewController
            ElectricityBillViewController.operatorString = (CellTitleArray.object(at: indexPath.item) as? NSString)!
            ElectricityBillViewController.index = indexPath.item
             self.present(ElectricityBillViewController, animated: false, completion: nil)
        }
        
        else if(fromScreen == "DTH")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let DTHViewController = storyboard.instantiateViewController(withIdentifier: "DTHViewController") as! DTHViewController
            DTHViewController.operatorString = (CellTitleArray.object(at: indexPath.item) as? NSString)!
            DTHViewController.index = indexPath.item
            self.present(DTHViewController, animated: false, completion: nil)
        }
    }
    
    func doThisWhenNotify() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MobileBillViewController = storyboard.instantiateViewController(withIdentifier: "MobileBillViewController") as! MobileBillViewController
       // MobileBillViewController.indexSelected = 2
        print("I've successfully sent a spark!")
    }

     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
