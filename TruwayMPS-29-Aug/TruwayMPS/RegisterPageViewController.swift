//
//  RegisterPageViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 18/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class RegisterPageViewController: UIViewController,UITableViewDelegate, UITableViewDataSource ,UITextFieldDelegate ,UIScrollViewDelegate ,apiManagerDelegate{
    
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var backgrounf_image : UIImageView!
    
    @IBOutlet var DropDownButton1 : UIButton!
    
    @IBOutlet var Country_View : UIView? = UIView()
    @IBOutlet var Code_View : UIView? = UIView()
    @IBOutlet var Mobile_View : UIView? = UIView()
    @IBOutlet var Email_View : UIView? = UIView()
    @IBOutlet var Dob_View : UIView? = UIView()
    @IBOutlet var Password_View : UIView? = UIView()
    @IBOutlet var ConfirmPassword_View : UIView? = UIView()
    
    @IBOutlet var Country_textField: UITextField!
    @IBOutlet var Code_textField: UITextField!
    @IBOutlet var Mobile_textField: UITextField!
    @IBOutlet var Email_textField: UITextField!
    @IBOutlet var dob_textField: UITextField!
    @IBOutlet var Password_textField: UITextField!
    @IBOutlet var ConfirmPassword_textField: UITextField!
    @IBOutlet var txtlabelLine1: UILabel!
    @IBOutlet var txtlabelLine2: UILabel!
    
    
    @IBOutlet var GetCountryList_Btn: UIButton!
    @IBOutlet var TermsAndConditions_Btn : UIButton!
    @IBOutlet var PrivacyPolicy_btn : UIButton!
    @IBOutlet var Register_btn : UIButton!
    @IBOutlet var Agree_btn : UIButton!
    @IBOutlet var show_passwordbtn : UIButton!
    @IBOutlet var show_confirmpasswordbtn : UIButton!
    @IBOutlet var and_Label : UILabel!
    @IBOutlet var IAgree_label : UILabel!
    
    //String values for all fields
    var country_string : String = ""
    var code_string : NSString = ""
    var mobile_string: NSString = ""
    var email_string : NSString = ""
    var dob_string : NSString = ""
    var password_string : NSString = ""
    var confirmPassword_string : NSString = ""
    var MessageString : String = ""
    
    var CountryList_tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    var CountryArrayList : NSMutableArray = []
    var CodeArrayList : NSMutableArray = []
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        CountryList_tableView = UITableView(frame: CGRect(x: 20, y: 170, width: appConstants.screenWidth-100, height: 300))
        CountryList_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.CountryList_tableView.dataSource = self
        self.CountryList_tableView.delegate = self
        self.view.addSubview(CountryList_tableView)
        CountryList_tableView.backgroundColor = UIColor.white
        CountryList_tableView.isHidden = true
        CountryList_tableView.tag = 1
        
        // To set the alpha component of views
        Country_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Mobile_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Email_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Dob_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Password_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        ConfirmPassword_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        
        Register_btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(0.4)
        fillFieldsFromDefaults()
    }
    
//MARK: Fill Fields
    func fillFieldsFromDefaults()
    {
        Country_textField.text = appConstants.defaults.value(forKey: "country") as? String
        Code_textField.text = appConstants.defaults.value(forKey: "code") as? String
        Mobile_textField.text = appConstants.defaults.value(forKey: "mobile") as? String
        Email_textField.text = appConstants.defaults.value(forKey: "email") as? String
        dob_textField.text = appConstants.defaults.value(forKey: "dob") as? String
        Password_textField.text = appConstants.defaults.value(forKey: "password") as? String
        ConfirmPassword_textField.text = appConstants.defaults.value(forKey: "confirm_password") as? String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
    }
    
    @IBAction func GetCountryListAction()
    {
        CountryList_tableView.isHidden = false
        scrollView.isUserInteractionEnabled = false
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CountryArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CountryList_tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(CountryArrayList[indexPath.row])"
        cell.textLabel!.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        scrollView.isUserInteractionEnabled = true
        Country_textField?.text = CountryArrayList[indexPath.row] as? String
        Code_textField.text = CodeArrayList[indexPath.row] as? String
        CountryList_tableView.isHidden = true
    }
 //MARK: Back Button Action
    @IBAction func Back_Button()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(ViewController, animated:false, completion:nil)
    }

    //MARK: TextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print(textField.tag)
        switch (textField.tag)
        {
        case 0:
            print("country")
            break
        case 1:
            print("code")
            break
        case 2:
            print("mobile")
            break
        case 3:
            print("email")
            break
        case 4:
            // DOB
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
            break
        case 5:
            print("password")
            break
        case 6:
            print("confirm Password")
            break
        default:
            print("default")
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    //MARK: Date Picker
    func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_GB") as Locale! // using Great Britain for 24 hr format
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/YYYY"
        dob_textField.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: Password Validation
    func isPwdCorrect(password: String , confirmPassword : String) -> Bool {
        if password.characters.count <= 4 && confirmPassword.characters.count <= 4{
            return true
        }
        else{
            return false
        }
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 2: //for mobile number
            let maxLength = 10
            let currentString: NSString = Mobile_textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 5: // for password
            let maxLength = 6
            let currentString: NSString = Password_textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 6: // for confirm password
            let maxLength = 6
            let currentString: NSString = ConfirmPassword_textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            default:
            break
        }
        return true
    }
    //MARK: Check for all field inputs for registration validation
    func validateInput()
    {
        country_string = Country_textField.text!
        code_string = Code_textField.text! as NSString
        mobile_string = Mobile_textField.text! as NSString
        email_string = Email_textField.text! as NSString
        dob_string = dob_textField.text! as NSString
        password_string = Password_textField.text! as NSString
        confirmPassword_string = ConfirmPassword_textField.text! as NSString
        print("mobile \(mobile_string)")
        
if(country_string == "")
 {
    MessageString = " Please select your country"
    appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
}
        
    if(mobile_string.length > 0)||(mobile_string.length <= 10 )
        {
         if validate(value: mobile_string as String) == true
                        
            {
              if(email_string != "")||(email_string.length > 0)
                {
                  if isValidEmail(testStr: Email_textField.text!) == true
                    {
                      if(country_string != "")
                        {
                          if(dob_string != "")
                            {
                              let age =  calcAge(birthday: dob_string as String)
                               print("age \(age)")
        
                                if(age<18)
                                 {
                                   MessageString = "Sorry !! you are not eligible to use TruwayMPS "
                                            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                  }
                                if( password_string != "")
                                {
                                  if(password_string.length<6)
                                    {
                                      MessageString = " password must be of 6 characters"
                                                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                    }
                                }
                                else{
                                     MessageString = " Please enter password"
                                            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                     }
        
                                if(password_string == confirmPassword_string)&&( password_string != "")
                                    {
                                    //call the signup api
                                RegisterApiCall()
                                      //  showOTPView()
                                    }
                                        else
                                        {
                                            MessageString = " Password and Confirm password does not match"
                                            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                        }
                                    }
                                    else
                                    {
                                        MessageString = " Please enter DOB"
                                        appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                    }
                                }
                                else
                                {
                                    MessageString = " Please select your country"
                                    appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                                }
                            }
                            else
                            {
                                MessageString = " Please enter Email Id"
                                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                            }
                        }
                        else
                        {
                            MessageString = "Email field should not be empty"
                            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                        }
                    }
                    else
                    {
                        MessageString = "Please enter correct mobile number"
                        appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                    }
                }
                else
                {
                    MessageString = "Mobile number should not be empty or it should not exceeds 12 numbers."
                    appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                }
        print("reistration process complete")
    }
    //MARK: Register Button Clicked
    @IBAction func Register_ButtonClick()
    {
        validateInput()
        appConstants.defaults.set(country_string, forKey: "country")
        appConstants.defaults.set(code_string, forKey: "code")
        appConstants.defaults.set(mobile_string, forKey: "mobile")
        appConstants.defaults.set(email_string, forKey: "email")
        appConstants.defaults.set(dob_string, forKey: "dob")
        appConstants.defaults.set(password_string, forKey: "password")
        appConstants.defaults.set(confirmPassword_string, forKey: "confirm_password")
        appConstants.defaults.set(CountryArrayList, forKey: "countryList")
    }
    
    func  RegisterApiCall()
    {
        let deviceId = appConstants.defaults.value(forKey: "deviceID")
        let app_secure_id = deviceId as! String+(mobile_string as String)
        print("app_secure_id \(app_secure_id)")
       let notification_token = appConstants.defaults.value(forKey: "notification_token")
       
        
        self.apiManager.ApiRegistrationCall (action:"Registration",app_secure_id:app_secure_id ,countryName:country_string ,countryCode:code_string as String,mobile:mobile_string as String,emailId:email_string as String,dob:dob_string as String,password:password_string as String,device_id:deviceId as! String,device_token: notification_token as! String)
    }
    
//MARK: To get Api Success Response
func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
           
        let responseDictionary : NSDictionary = value as! NSDictionary
        let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage == "success")
            {
        let responseData : NSDictionary = responseDictionary .value(forKey: "userData") as! NSDictionary
       
            if responseData.value(forKey: ("userId" as NSString) as String) != nil
            {
                 let userId : NSString = responseData.value(forKey: "userId") as! NSString
                appConstants.defaults.set(userId, forKey: "userId")
                appConstants.defaults.set(Country_textField?.text, forKey: "CountryName") // Save userId and countryNmae to user defaults
                let user_id = appConstants.defaults.value(forKey: "userId")
                self.apiManager.ApiOTPRequestCall (action:"OTPRequest",userId:user_id as! String ) //TO Request OTP -API Call
            }
            else{ //To handle otp response
                let otp = responseData.value(forKey: "otp") as! NSString
                print(otp)
                appConstants.defaults.set(otp, forKey: "OTP")
                showOTPView() // call this on success response
                }
        print("Registration Response Message :\(responseMessage)")
        
            }
            else{
            MessageString = ("Registration Failed.Message from server : \(responseMessage)")
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                }
    }
    }

    //MARK: SHOW OTP POP UP
    func showOTPView()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OTPViewController = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        OTPViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(OTPViewController)
        OTPViewController.fromScreen = "Registration"
        self.view.addSubview(OTPViewController.view)
        OTPViewController.didMove(toParentViewController: self)
    }
    
//MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }
    
//MARK: To validate email id
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let range = testStr.range(of: emailRegEx,options:.regularExpression)
        let result = range != nil ? true : false
        return result
        
    }
    
//MARK: Agree button check
    var pressed = false
    @IBAction func AgreeBtnpressed(sender: AnyObject) {
        
        if !pressed {
            let image = UIImage(named: "checkbox_unselected.png") as UIImage!
            Agree_btn.setImage(image, for: .normal)
            Register_btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(0.4)
            Register_btn.isUserInteractionEnabled = false
            pressed = true
        } else {
            let image = UIImage(named: "checkbox_selected.png") as UIImage!
            Agree_btn.setImage(image, for: .normal)
            Register_btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1)
            Register_btn.isUserInteractionEnabled = true
            pressed = false
        }
    }
    
//MARK: - To Calculate Age of user
    func calcAge(birthday:String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/YYYY"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let now: NSDate! = NSDate()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now as Date, options: [])
        let age = calcAge.year
        return age!
    }
//MARK: - To hide-Unhide password
    var showPasswordPressed = false
    @IBAction func Showpressed(sender: AnyObject) {
        if !showPasswordPressed {
            let image = UIImage(named: "show.png") as UIImage!
            show_passwordbtn.setImage(image, for: .normal)
            self.Password_textField.isSecureTextEntry = false
            showPasswordPressed = true
        } else {
            let image = UIImage(named: "hide.png") as UIImage!
            show_passwordbtn.setImage(image, for: .normal)
            self.Password_textField.isSecureTextEntry = true
            showPasswordPressed = false
        }
    }
    
//MARK: - To hide/unhide confirm password
    var showConfirmPasswordPressed = false
    @IBAction func ShowConfirmBtnpressed(sender: AnyObject) {
        if !showConfirmPasswordPressed {
            let image = UIImage(named: "show.png") as UIImage!
            show_confirmpasswordbtn.setImage(image, for: .normal)
            self.ConfirmPassword_textField.isSecureTextEntry = false
            showConfirmPasswordPressed = true
        } else {
            let image = UIImage(named: "hide.png") as UIImage!
            show_confirmpasswordbtn.setImage(image, for: .normal)
            self.ConfirmPassword_textField.isSecureTextEntry = true
            showConfirmPasswordPressed = false
        }
    }
    
//MARK: Move to additional Detail Page
    @IBAction func MoveToAdditionalDetailPage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let AdditionalDetailViewController = storyboard.instantiateViewController(withIdentifier: "AdditionalDetailViewController") as! AdditionalDetailViewController
        self.present(AdditionalDetailViewController, animated:false, completion:nil)
    }
    
//MARK: Terms & Conditions Page
    @IBAction func TermsAndConditionsPage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let TermsAndConditionsViewController = storyboard.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.present(TermsAndConditionsViewController, animated:false, completion:nil)
    }

    
//MARK: To Stop Horizontal Scroll of Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
        else{
            scrollView.isDirectionalLockEnabled = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
