//
//  AppConstants.swift
//  TruwayMPS
//
//  Created by pooja on 05/05/17.
//  Copyright © 2017 Truway India. All rights reserved.

// This class contains all the constant variables and methods used in the project

import Foundation
import UIKit

var customView = UIView()

class AppConstants {

// MARK: - Application Constants
var screenWidth = UIScreen.main.bounds.width
var screenHeight = UIScreen.main.bounds.height
let defaults = UserDefaults.standard
    
    
//MARK: - Color Codes
let CornerRadius: CGFloat = 5.0
let SignIn_button_color = "0x1d5fd5"
let Red_color = "#e01f25"
    
//MARK: Base URL
let BaseURL = "http://www.paymny.today/controlPanel/api/webservices"
    
//MARK: for QR code/mobile payment options
    
    let Tag_Payment_Request_Mode_Phone = "3" //requested by phone
    let Tag_Payment_Request_Mode_QRCODE = "4" //requested by qr code
    let Tag_Payment_Response_Web = "2" //response by web
    let Tag_Payment_Response_Phone = "1"
    let Tag_Payment_Response_QRCode = "5"
    

    
//MARK: - To show Alert View
func showAlert(title: String , message: String, controller: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
 
//MARK: To show progress HUD
func showLoadingHUD(to_view: UIView) {
        let hud = MBProgressHUD.showAdded(to: to_view, animated: true)
       // hud.label.text = "Loading..."
    }
    
//MARK: To hide progress HUD
func hideLoadingHUD(for_view: UIView) {
        MBProgressHUD.hideAllHUDs(for: for_view, animated: true)
    }
  
    
 //MARK: String to dictionary
func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    

}
