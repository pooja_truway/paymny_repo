//
//  MobileBillViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 21/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import ContactsUI

class MobileBillViewController: UIViewController,CNContactPickerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var mobileNumber_TextField : UITextField?
    @IBOutlet weak var paymentButton : UIButton?
    @IBOutlet weak var amount_TextField : UITextField?
    @IBOutlet weak var operator_TextField : UITextField?
    @IBOutlet weak var operator_Image : UIImageView?
    
    //Radio Buttons
    @IBOutlet weak var prepaidButton : UIButton?
    @IBOutlet weak var postpaidButton : UIButton?
    @IBOutlet weak var landlineButton : UIButton?
    
    var appConstants : AppConstants = AppConstants()
    
    var operatorString : NSString = ""
    var index : Int = 0
    var MessageString : String = ""
    var mobile_string : String = ""
    var fromScreen : NSString = ""
   
    let CellImageArray : NSMutableArray =  [UIImage(named: "ic_airtel_image"),
        UIImage(named: "ic_airtel"),UIImage(named: "ic_vodafone"),UIImage(named: "ic_idea"),UIImage(named: "ic_jio"),UIImage(named: "ic_bsnl"),UIImage(named: "ic_mtnl"),UIImage(named: "ic_aircel"),UIImage(named: "ic_tata")]
    
    let CellTitleArray : NSMutableArray =  ["AIRTEL","VODAFONE","IDEA","JIO","BSNL","MTNL","AIRCEL","TATA DOCOMO"]

    
    override func viewWillAppear(_ animated: Bool) {
        
        if let index : Int = appConstants.defaults.value(forKey: "operatorSelected")as? Int
         {
            print("index from operator \(index)")
            operator_Image?.image = CellImageArray.object(at: index) as? UIImage
         }
    
        operator_TextField?.text = operatorString as String
        print("index : \(index)")
        if(index == 0)
        {
            
        }
        else{
            index = index + 1
            operator_Image?.image = CellImageArray.object(at: index) as? UIImage
        }
       
       if let mobileToBeRecharged = appConstants.defaults.value(forKey: "mobileToBeRecharged")
       {
        mobileNumber_TextField?.text = mobileToBeRecharged as? String
        }
        
        paymentButton?.layer.cornerRadius = 10.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Action
    @IBAction func BackClicked(sender : UIButton)
    {
        
        if(fromScreen == "Main")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.present(SWRevealViewController, animated:false, completion:nil)
        }
        else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let UtilityBillViewController = storyboard.instantiateViewController(withIdentifier: "UtilityBillViewController") as! UtilityBillViewController
        self.present(UtilityBillViewController, animated: false, completion: nil)
        }
    }
    
    @IBAction func click_Contact(_ sender: UIButton) {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        
        print(sender.tag)
       
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addNewContact"), object: nil, userInfo: ["contactToAdd": contact])
        
                if contact.phoneNumbers.count > 0 {
                    self.mobileNumber_TextField?.text = (contact.phoneNumbers[0].value ).value(forKey: "digits") as? String
                mobile_string = (mobileNumber_TextField?.text)!
                appConstants.defaults.set(mobile_string, forKey: "mobileToBeRecharged")
                }
        }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Radio button click actions
    
    var prepaidBtn = false
    var postpaidBtn = false
    var landlineBtn = false
    @IBAction func prepaidButtonClicked(sender: AnyObject) {
        
        if !prepaidBtn {
            let image = UIImage(named: "ic_radio_btn_selected") as UIImage!
            prepaidButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            postpaidButton?.setImage(image1, for: .normal)
            landlineButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            prepaidBtn = true
           // duration = "not sure"
            postpaidBtn = false
            landlineBtn = false
            paymentButton?.setTitle("RECHARGE", for: UIControlState.normal)
        } else {
            
            let image = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            prepaidButton?.setImage(image, for: .normal)
            prepaidBtn = false
        }
    }
    
    @IBAction func postpaidButtonClicked(sender: AnyObject) {
        
        if !postpaidBtn {
            let image = UIImage(named: "ic_radio_btn_selected") as UIImage!
            postpaidButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            prepaidButton?.setImage(image1, for: .normal)
            landlineButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            prepaidBtn = false
            postpaidBtn = true
          //  duration = "less than 3 months"
            landlineBtn = false
             paymentButton?.setTitle("PAY BILL", for: UIControlState.normal)
        } else {
            
            let image = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            prepaidButton?.setImage(image, for: .normal)
            postpaidBtn = false
        }
    }
    
    @IBAction func landlineButtonClicked(sender: AnyObject) {
        
        if !landlineBtn {
            let image = UIImage(named: "ic_radio_btn_selected") as UIImage!
            landlineButton?.setImage(image, for: .normal)
            
            let image1 = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            prepaidButton?.setImage(image1, for: .normal)
            postpaidButton?.setImage(image1, for: .normal)
            print("Check box 1 selected")
            landlineBtn = true
            postpaidBtn = false
           // duration = "more than 3 months"
            prepaidBtn = false
             paymentButton?.setTitle("PAY BILL", for: UIControlState.normal)
        } else {
            
            let image = UIImage(named: "ic_radio_btn_unselected") as UIImage!
            landlineButton?.setImage(image, for: .normal)
            landlineBtn = false
        }
    }
    
    //MARK: Select Operator Button Clicked
    @IBAction func GetOperatorList(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SelectOperatorViewController = storyboard.instantiateViewController(withIdentifier: "SelectOperatorViewController") as! SelectOperatorViewController
        SelectOperatorViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(SelectOperatorViewController)
        SelectOperatorViewController.fromScreen = "Mobile Bill"
        self.view.addSubview(SelectOperatorViewController.view)
        SelectOperatorViewController.didMove(toParentViewController: self)

    }
    
    //MARK: Get notified when favorite item is selected from list
    func doSomethingAfterNotified() {
        appConstants.defaults.value(forKey: "operatorSelected")
      //  fromScreen = "AddItems"
        viewWillAppear(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        appConstants.defaults.set("", forKey: "operatorSelected")
    }
    
    //MARk: Pay Bill
    @IBAction func proceedToRecharge(sender : UIButton)
    {
        
     mobile_string = (mobileNumber_TextField?.text)!
     appConstants.defaults.set(mobile_string, forKey: "mobileToBeRecharged")
        
      let operator_string = operator_TextField?.text
      let amount_string = amount_TextField?.text
        if(mobile_string == "")
        {
            MessageString = "Please enter mobile number"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(operator_string == "")
        {
            MessageString = "Please select the operator"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(amount_string == "")
        {
            MessageString = "Please enter the amount"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else{
            //move to payment screen
        appConstants.defaults.removeObject(forKey:"mobileToBeRecharged")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let PaymentViewController = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            self.present(PaymentViewController, animated: false, completion: nil)
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
