//
//  ForgotPassword_OTPViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 17/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ForgotPassword_OTPViewController: UIViewController {
    
    @IBOutlet var BackgroundView : UIView!
    @IBOutlet var cancel_button : UIButton!
    @IBOutlet var OK_button : UIButton!
    
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        BackgroundView.layer.cornerRadius = 10

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
