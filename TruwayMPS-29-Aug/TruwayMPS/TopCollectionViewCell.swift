//
//  TopCollectionViewCell.swift
//  TruwayMPS
//
//  Created by Pooja on 24/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class TopCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var TopCellImage: UIImageView!
    @IBOutlet weak var TopCellTitleLabel: UILabel!
}
