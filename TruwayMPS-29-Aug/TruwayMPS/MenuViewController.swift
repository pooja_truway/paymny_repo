//
//  MenuViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 26/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, apiManagerDelegate {
    @IBOutlet weak var tableView : UITableView?
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    var mainscreen : MainScreenViewController = MainScreenViewController()
    
    var menuListText = ["Send Payment","Request Payment","UPI","Scan QR Code","Card Vault","Utility Bill","Foreign Exchange","Transaction History","Loyalty Offer","Offer Zone","Help","Sign Out"]
    
    var menuListImages =  [UIImage(named:"ic_send_payment"),UIImage(named: ""),UIImage(named: ""),
                           UIImage(named:"ic_request_payment"),
                           UIImage(named: ""),UIImage(named: ""),
                           UIImage(named: "ic_upi"),
                           UIImage(named:"ic_qr_code"),
                           UIImage(named: "ic_card_vault"),
                           UIImage(named:"ic_utility"),
                           UIImage(named: "ic_foreign_exchange"),
                           UIImage(named: "ic_shopping_history"),
                           UIImage(named: "ic_loyalty_offer"),
                           UIImage(named: "ic_offer_zone"),
                           UIImage(named: "ic_help"),
                           UIImage(named: "ic_sign_out")]
    
    struct Section {
        var name: String!
        var items: [String]!
        var image: UIImage
        var collapsed: Bool!
        
        init(name: String, items: [String],image : UIImage, collapsed: Bool = true) {
            self.name = name
            self.items = items
            self.image = image
            self.collapsed = collapsed
        }
    }
    var sections = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let usermobile : NSString = appConstants.defaults.value(forKey: "mobile") as? NSString{
        self.navigationItem.title = "Hello \(usermobile)!"
        }
        else{
            self.navigationItem.title = "Hello !"
        }
        self.apiManager = ApiManager()
        apiManager.delegate = self
        print("menu opened")
        
        sections = [
            Section(name: "Send Payment", items: ["To Phone Number", "Scan QR Code"], image: UIImage(named: "ic_sign_out")!),
            Section(name: "Request Payment", items: ["To Phone Number", "Generate QR Code"], image: UIImage(named: "ic_sign_out")!),
            Section(name: "UPI", items: [], image:  UIImage(named: "ic_sign_out")!),
             Section(name: "My QR Code", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Card Vault", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Utility Bill", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Foreign Exchange", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Transaction History", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Loyalty Offer", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Offer Zone", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Help", items: [], image: UIImage(named: "ic_sign_out")!),
             Section(name: "Sign Out", items: [], image: UIImage(named: "ic_sign_out")!)
        ]

        // Register the table view cell class and its reuse id
     }
    
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:  TableView Datasource
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        // For section 1, the total count is items count plus the number of headers
        var count = sections.count
        
        for section in sections {
            count += section.items.count
        }
        return count
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 0
        }
        
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        // Header has fixed height
        if row == 0 {
            return 50.0
        }
        return sections[section].collapsed! ? 0 : 44.0
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as UITableViewCell!
            return cell!
        }
        
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        if row == 0 {
            print(indexPath.row)
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! MenuTableViewCell
            cell.CellTitleLabel.text = sections[section].name
           
            cell.toggleButton.tag = section
            if(indexPath.row == 0)||(indexPath.row == 3)
            {
                let image = UIImage(named: "dropdown") as UIImage?
                cell.toggleButton.setImage(image,for: UIControlState())
            }
            else{
                let image = UIImage(named: "") as UIImage?
                cell.toggleButton.setImage(image,for: UIControlState())
                cell.toggleButton.isHidden = true
            }
            cell.CellImage.image = menuListImages[indexPath.row]
            cell.toggleButton.addTarget(self, action: #selector(MenuViewController.toggleCollapse), for: .touchUpInside)
          
            return cell
        } else {
            //MARK: To show expandable cell
             let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as! ExpandableTableViewCell
            cell.CellTitleLabel.text = sections[section].items[row - 1]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
       
       print(indexPath.row)

        switch (indexPath.row){
        case 0:  // To show expandable list
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! MenuTableViewCell
            self.toggleCollapse(cell.toggleButton)
            
        case 3:  // To show expandable list
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! MenuTableViewCell
            self.toggleCollapse(cell.toggleButton)
         
           
        case 1: // To Send payment to mobile
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SendPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "SendPaymentToMobileVC") as! SendPaymentToMobileVC
             self.present(SendPaymentToMobileVC, animated: false, completion: nil)
            
        case 2: // Scan QR Code
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ScanQRCodeVC = storyboard.instantiateViewController(withIdentifier: "ScanQRCodeVC") as! ScanQRCodeVC
            self.present(ScanQRCodeVC, animated: false, completion: nil)
            
            
        case 7: // My QR Code
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let MyQRCodeVC = storyboard.instantiateViewController(withIdentifier: "MyQRCodeVC") as! MyQRCodeVC
            self.present(MyQRCodeVC, animated: false, completion: nil)

            
            
        case 4: // To Request payment from mobile
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let RequestPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "RequestPaymentToMobileVC") as! RequestPaymentToMobileVC
             self.present(RequestPaymentToMobileVC, animated: false, completion: nil)
        case 5: // To Request payment from QR Code
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let RequestPaymentToQRCodeVC = storyboard.instantiateViewController(withIdentifier: "RequestPaymentToQRCodeVC") as! RequestPaymentToQRCodeVC
            self.present(RequestPaymentToQRCodeVC, animated: false, completion: nil)
        
        case 8:  //Card Vault
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let CardVaultVC = storyboard.instantiateViewController(withIdentifier: "CardVaultVC") as! CardVaultVC
            self.present(CardVaultVC, animated: false, completion: nil)
            
        case 11:  //Transaction History
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let TransactionHistoryVCViewController = storyboard.instantiateViewController(withIdentifier: "TransactionHistoryVCViewController") as! TransactionHistoryVCViewController
            self.present(TransactionHistoryVCViewController, animated: false, completion: nil)
            
        case 9: //Utility Bill
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let UtilityBillViewController = storyboard.instantiateViewController(withIdentifier: "UtilityBillViewController") as! UtilityBillViewController
            self.present(UtilityBillViewController, animated: false, completion: nil)

            
        case 15:  //Sign Out
            let MessageString = "Are you sure you want to sign out ?"
            self.LogOutAlert(title: "THANK YOU!!!", message: MessageString, controller: self)// for password
            
        default:
            break
        }
    }

    // MARK: - Event Handlers
    func toggleCollapse(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        tableView?.beginUpdates()
        for i in start ..< end + 1 {
            tableView?.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
        }
        tableView?.endUpdates()
    }
    // MARK: - Helper Functions
    func getSectionIndex(_ row: NSInteger) -> Int {
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        return -1
    }
    
    func getRowIndex(_ row: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        return indices
    }
    
//MARK: To show alert on log out
    func LogOutAlert(title: String , message: String, controller: UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("LogOutPressed")
            //call log out api
            self.LogOutApiCall()
        }
        let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Present the controller
        controller.present(alertController, animated: true, completion: nil)
    }
    
 //MARK: Call Log out Api
func LogOutApiCall()
    {
    let userId = appConstants.defaults.value(forKey: "userId")
        print("userId : \(String(describing: userId))")
    appConstants.showLoadingHUD(to_view: self.view)
    self.apiManager.ApiLogOutCall(action:"logOut",userId: userId  as! String)
    }
//MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            appConstants.hideLoadingHUD(for_view: self.view)
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                
               // appConstants.defaults.removeObject(forKey: "userId")
                appConstants.defaults.removeObject(forKey: "logInStatus")
                self.MovetoSliderPage()

        }
            else{
               let MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
        }
    }
 //MARK: Move to Slider Page
    func  MovetoSliderPage()
    {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    self.present(ViewController, animated:false, completion:nil)
    }
}
