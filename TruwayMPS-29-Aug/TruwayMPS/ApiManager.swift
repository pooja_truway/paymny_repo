//
//  ApiManager.swift
//  TruwayMPS
//
//  Created by Pooja on 29/05/17.
//  Copyright © 2017 Truway India. All rights reserved.


import UIKit
import Alamofire
import SwiftyJSON

protocol apiManagerDelegate:NSObjectProtocol {
    
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>)
    func APIFailureResponse(_ msgError: String)
}

extension apiManagerDelegate {
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        print ("hello apimanager \(response)")
    }
    
    func APIFailureResponse(_ msgError: String){
        // leaving this empty
    }
}

class ApiManager: NSObject,apiManagerDelegate {
    
    fileprivate let API_STATUS = "error_status"
    var delegate :apiManagerDelegate?
    var appConstants: AppConstants = AppConstants()
    
    // MARK: HUD Utility
    func showHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 2.0)
    }
    func showlongHud()  {
        // HUD.flash(.rotatingImage(UIImage(named: "progress")), delay: 5.0)
    }
    
    func hideHud()  {
        
    }
    //MARK: Get App Status
func ApiAppStatusCall (action:String,device_id:String){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"device_id":device_id], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
//MARK: To Get Country List
    func ApiCountryListCall (){
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":"countryList"], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                  self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
 //MARK: Registration API
    func ApiRegistrationCall (action:String,app_secure_id:String,countryName:String,countryCode:String,mobile:String,emailId:String,dob:String,password:String,device_id:String,device_token:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters: ["action":action,"app_secure_id":app_secure_id,"countryName":countryName,"countryCode":countryCode,"mobile":mobile,"emailId":emailId,"dob":dob,"password":password,"device_id":device_id,"device_token":device_token], encoding: JSONEncoding.default, headers: nil).responseJSON{
    
    response in
    do {
    if let mdata = response.data{
    if mdata.isEmpty
    {
    print("no data available")
    return
    }
    }
    // Parsing Data
    let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
    
    print(responseArray)
    if  responseArray.count > 0
    {
    self.delegate?.apiSuccessResponse(responseArray)
    }
    else{
    self.delegate?.APIFailureResponse("Error...")
    }
    } catch{
    print("error")
    }
  }
}
    
//MARK: Login API
    func ApiLoginCall (action:String,mobile:String,password:String,app_secure_id:String,device_token:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"mobile":mobile,"password":password,"app_secure_id":app_secure_id,"device_token":device_token], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
//MARK: Additional Detail API
    func ApiAdditionalDetailCall (action:String,userId:String,firstName:String,lastName:String,address:String,city:String,state:String,postalCode:String,countryName:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId,"firstName":firstName,"lastName":lastName,"address":address,"city":city,"state":state,"postalCode":postalCode,"countryName":countryName], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
//MARK: Security Question API
func ApiSecurityQuestionCall (action:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                   self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
   
 //MARK: Verification Detail API
    func ApiVerificationDetailCall(action:String,userId:String,q1:String,a1:String,q2:String,a2:String,q3:String,a3:String,q4:String,a4:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId,"q1":q1,"a1":a1,"q2":q2,"a2":a2,"q3":q3,"a3":a3,"q4":q4,"a4":a4], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
//MARK: OTP Request Api Call
func ApiOTPRequestCall(action:String,userId:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
//MARK: OTP Verification APICall
func ApiOTPVerificationCall(action:String,userId:String,otp:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId,"otp":otp], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
//MARK: Log Out Api 
func ApiLogOutCall(action: String,userId:String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
  
    }
 //MARK: Forgot Password Api
    func ApiForgotPasswordCall(action: String,deviceId:String,mobile:String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"device_id":deviceId,"mobile":mobile], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
   //MARK: Reset Password OTP Api Call
    func ApiResetPasswordOTPCall(action: String,deviceId:String,userId:String,otp:String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"device_id":deviceId,"userId":userId,"otp":otp], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
 //MARK: Update Password Api Call
    func ApiUpdatePasswordCall(action: String,deviceId:String,userId:String,password:String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"device_id":deviceId,"userId":userId,"password":password], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Request Money From Mobile Api
    func ApiRequestMoneyFromMobileCall(action: String,amount:String,payeeMobile:String,comments:String,userId:String,mobile:String,paymentType: String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"amount_requested":amount,"payee_mobile":payeeMobile,"comments":comments,"userId":userId,"mobile":mobile,"paymentMethod":paymentType], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Send Money To Mobile Api
    func ApiSendMoneyToMobileCall(action: String,amount:String,receiverMobile:String,comments:String,userId:String,mobile:String,paymentType:String,transactionId:String)
    {
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"send_amount":amount,"receiver_mobile":receiverMobile,"comments":comments,"userId":userId,"mobile":mobile,"paymentMethod":paymentType,"transactionId":transactionId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Verification Detail Update API
    func ApiVerificationDetailUpdateCall(action:String,userId:String,q1:String,a1:String,q2:String,a2:String,q3:String,a3:String,q4:String,a4:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId,"q1":q1,"a1":a1,"q2":q2,"a2":a2,"q3":q3,"a3":a3,"q4":q4,"a4":a4], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: SAVE DEMO NUMBER
    func SaveNumberDemo(action:String,OS_Type:String,mobile:String,device_id:String,device_token:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"OS_Type":OS_Type,"OS_Type":OS_Type,"device_id":device_id,"device_token":device_token], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    
    
    //MARK: ApiSendMoneyDemo
    func ApiSendMoneyDemo(action:String,userId:String,mobile:String,payee_mobile:String,amount:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId,"mobile":mobile,"amount":amount,"payee_mobile":payee_mobile], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: ApiProfileInfoCall
    func ApiProfileInfoCall(action:String,userId:String) {
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
  //MARK: cancel payment api
    func ApiCancelRequestCall(action: String,amount:String,requested_mobile:String,userId:String,mobile:String,transactionId: String)
    {
        let paymentMethod : NSString = "3"
        let parameters = ["action":action,"amount":amount,"requested_mobile":requested_mobile,"userId":userId,"mobile":mobile,"transactionId":transactionId,"paymentMethod":paymentMethod] as [String : Any]
        print(parameters)
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"amount":amount,"requested_mobile":requested_mobile,"userId":userId,"mobile":mobile,"transactionId":transactionId,"paymentMethod":paymentMethod], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,AnyObject>
                
                print(responseArray)
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Transaction History
    
    func ApiTransactionHistoryCall(action: String,userId:String)
    {
        
        let parameters = ["action":action,"userId":userId]
        print(parameters)
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,Any>
                
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray as Dictionary<String, AnyObject>)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }
    
    //MARK: Offer Zone List
    
    func ApiOfferZoneList(action: String,userId:String)
    {
        
        let parameters = ["action":action,"userId":userId]
        print(parameters)
        
        Alamofire.request(appConstants.BaseURL, method: .post, parameters:["action":action,"userId":userId], encoding: JSONEncoding.default, headers: nil).responseJSON{
            
            response in
            do {
                if let mdata = response.data{
                    if mdata.isEmpty
                    {
                        print("no data available")
                        return
                    }
                }
                // Parsing Data
                let responseArray =  try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions()) as! Dictionary<String,Any>
                
                if  responseArray.count > 0
                {
                    self.delegate?.apiSuccessResponse(responseArray as Dictionary<String, AnyObject>)
                }
                else{
                    self.delegate?.APIFailureResponse("Error...")
                }
            } catch{
                print("error")
            }
        }
    }


 }
