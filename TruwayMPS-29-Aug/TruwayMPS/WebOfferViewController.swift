//
//  WebOfferViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 18/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class WebOfferViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet var webView : UIWebView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var weblink : NSString = "www.google.com"


    override func viewWillAppear(_ animated: Bool) {
       
        activity.isHidden = true
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView?.delegate = self
        
        print("web page opened")
      //  webView?.loadRequest(URLRequest(url: google.com))
        webView.loadRequest(URLRequest(url: NSURL(string: "http://www.google.com")! as URL))
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ :UIWebView){
        
        activity.isHidden = false
        activity.startAnimating()
        
    }
    func webViewDidFinishLoad(_ :UIWebView){
        
        activity.stopAnimating()
        activity.isHidden = true
        
      }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
