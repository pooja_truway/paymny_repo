//
//  PaymentOptionsTableViewCell.swift
//  TruwayMPS
//
//  Created by Truway India on 13/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class PaymentOptionsTableViewCell: UITableViewCell {
      @IBOutlet weak var PaymentOptionsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
