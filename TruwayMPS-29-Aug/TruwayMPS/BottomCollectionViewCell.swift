//
//  BottomCollectionViewCell.swift
//  TruwayMPS
//
//  Created by Pooja on 24/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class BottomCollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var BottomCellImage: UIImageView!
     @IBOutlet weak var BottomCellTitleLabel: UILabel!
}
