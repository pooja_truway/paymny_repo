//
//  NotificationViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 25/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController,UITabBarDelegate {

    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var menuButton:UIBarButtonItem!
    
    override func viewWillAppear(_ animated: Bool) {
        print("Notification Page opened")
        tabBar.selectedItem = tabBar.items![3] as UITabBarItem
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 250
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Tab Bar Delegate- called when user changes tab.
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0)
        {
            print("Home tab is selected")
            OpenHomePage()
        }
        else if(item.tag == 1)
        {
            print("Profile tab is selected")
            OpenProfilePage()
        }
        else if(item.tag == 2)
        {
            print("Offer Zone tab is selected")
            OpenOfferZonePage()
        }
        else if(item.tag == 4)
        {
            print("Notification tab is selected")
            OpenForeignExchangePage()
        }
        
    }
    
    func OpenHomePage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        self.navigationController?.pushViewController(MainScreenViewController, animated: false)
    }
    
    func OpenProfilePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(ProfileViewController, animated: false)
    }
    
    func OpenOfferZonePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OfferZoneViewController = storyboard.instantiateViewController(withIdentifier: "OfferZoneViewController") as! OfferZoneViewController
        self.navigationController?.pushViewController(OfferZoneViewController, animated: false)
    }
    
    func OpenForeignExchangePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForeignExchangeViewController = storyboard.instantiateViewController(withIdentifier: "ForeignExchangeViewController") as! ForeignExchangeViewController
        self.navigationController?.pushViewController(ForeignExchangeViewController, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
