//
//  CardListTableViewCell.swift
//  TruwayMPS
//
//  Created by Truway India on 13/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class CardListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var CardNameLabel: UILabel!
    @IBOutlet weak var radioButton : UIButton?
    @IBOutlet weak var deleteButton : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
