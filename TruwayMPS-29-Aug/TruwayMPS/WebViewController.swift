//
//  WebViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 20/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webview : UIWebView?
    @IBOutlet weak var imageview : UIImageView?
    var weblink : NSString = ""
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var popUpView : UIView?
    @IBOutlet weak var messageLabel : UILabel?
    @IBOutlet weak var StatusLabel : UILabel?
    @IBOutlet weak var OkButton : UIButton?
    @IBOutlet weak var backButton : UIButton?
    var fromScreen : NSString = ""
    
    var appConstants : AppConstants = AppConstants()

    
    
    override func viewWillAppear(_ animated: Bool) {
        print("weblink : \(weblink)")
        activity.isHidden = true
        imageview?.isHidden = true
        popUpView?.isHidden = true
        self.imageview?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        webview?.scrollView.bounces = false
        backButton?.isHidden = false
        
        let mobile  = appConstants.defaults.value(forKey: "receiversMobile") as? String
        let amount  = appConstants.defaults.value(forKey: "amountToSend") as? String
        print("mobile number : \(String(describing: mobile))")
        print("amountv to be send : \(String(describing: amount))")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        webview?.delegate = self
        
        print("web page opened")
        webview?.loadRequest(URLRequest(url: URL(string: weblink as String)!))
        // Do any additional setup after loading the view.
    }
    
  func webViewDidStartLoad(_ :UIWebView){
    
    activity.isHidden = false
    activity.startAnimating()
    
    let str = webview?.request?.url
    print("url >>>>  \(String(describing: str))")
    NSLog("Webview load has started")
    
    }
   func webViewDidFinishLoad(_ :UIWebView){
    
    activity.stopAnimating()
    activity.isHidden = true
    
    let str = webview?.request?.url
    print("url finished loading >>>>  \(String(describing: str))")
    
    if (str?.lastPathComponent.contains("responseSuccessURL.php"))!
    {
       print("transaction completed")
        webview?.isHidden = true
        imageview?.isHidden = false
        popUpView?.isHidden = false
        backButton?.isHidden = true
        let amount : String = (appConstants.defaults.value(forKey: "amountToSend")as? String)!
        let mobile : String = (appConstants.defaults.value(forKey: "receiversMobile") as? String)!
        StatusLabel?.text = "CONFIRMED"
        popUpView?.backgroundColor = UIColor.blue
        messageLabel?.text = "Your payment to \(mobile) for \( amount) has been confirmed"
        
        appConstants.defaults.set("", forKey: "amountToSend")
        appConstants.defaults.set("", forKey: "receiversMobile")
    }
    else if (str?.lastPathComponent.contains("responseFailURL.php"))!
    {
        print("transaction failed")
         webview?.isHidden = true
        imageview?.isHidden = false
        popUpView?.isHidden = false
        backButton?.isHidden = true
        let amount : String = (appConstants.defaults.value(forKey: "amountToSend")as? String)!
        let mobile : String = (appConstants.defaults.value(forKey: "receiversMobile") as? String)!
        StatusLabel?.text = "FAILED"
        popUpView?.backgroundColor = UIColor.red
        messageLabel?.text = "Your payment to \(mobile) for \(amount) has been failed"
        
        appConstants.defaults.set("", forKey: "amountToSend")
        appConstants.defaults.set("", forKey: "receiversMobile")
    }
}
    
    //MARK: Back button
    @IBAction func back()
    {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    //MARK: OK button Clicked
    @IBAction func OkClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OptionsViewController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
        self.present(OptionsViewController, animated:false, completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
