//
//  RequestPaymentViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 19/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class RequestPaymentViewController: UIViewController, UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet weak var mobileTextField : UITextField?
    @IBOutlet weak var amountTextField : UITextField?
    var mobile_string : String = ""
    var amount_string : String = ""
    
    var MessageString = ""
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func back ()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ApiCall(sender : UIButton)
    {
         mobile_string = (mobileTextField?.text)!
         amount_string  = (amountTextField?.text)!
        print("mobile : \(mobile_string)")
        print("amount : \(amount_string)")
        
        if (amount_string != "")
        {
            if(mobile_string != "")
            {
                CallApiToRequestMoneyDemo()
            }
        }
    }
    
    
    func CallApiToRequestMoneyDemo()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        let mobile = appConstants.defaults.value(forKey: "mobile") as? String
        
         self.apiManager.ApiRequestMoneyFromMobileCall (action:"paymentRequest",amount:amount_string as String,payeeMobile:mobile_string as String,comments:"",userId:userId as! String,mobile:mobile!, paymentType: "")
    }
    
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            
            print("responseMessage: \(responseMessage)")
            if(responseMessage == "success")
            {
                MessageString = "Requested Successfully"
                appConstants.showAlert(title: "", message: MessageString, controller: self)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let OptionsViewController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
                self.present(OptionsViewController, animated:false, completion:nil)
            }
            
            else{
                MessageString = "Request Failed"
                appConstants.showAlert(title: "", message: MessageString, controller: self)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let OptionsViewController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
                self.present(OptionsViewController, animated:false, completion:nil)
            }
        }
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 0: //for mobile number
            let maxLength = 10
            let currentString: NSString = mobileTextField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
