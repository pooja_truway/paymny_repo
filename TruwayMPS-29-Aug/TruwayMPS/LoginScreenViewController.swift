//
//  LoginScreenViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 09/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController ,UITextFieldDelegate, apiManagerDelegate{
    
    @IBOutlet var SignIn_Btn: UIButton!
    @IBOutlet var ShareApp_Btn: UIButton!
    @IBOutlet var Mobile_View : UIView? = UIView()
    @IBOutlet var Password_View : UIView? = UIView()
    @IBOutlet var Mobile_textField: UITextField!
    @IBOutlet var Password_textField: UITextField!
    @IBOutlet var notRegistered_btn : UIButton!
    
    @IBOutlet weak var forgotPassword_btn : UIButton?
    
    var mobile_string : NSString = ""
    var password_string : NSString = ""
    var MessageString : String = ""
    let i : Int = 0
   
    var forgotpassword : ForgotPassword1ViewController = ForgotPassword1ViewController()
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //MARK: To assign colors to buttons
        notRegistered_btn.isHidden = true
        SignIn_Btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color)
        ShareApp_Btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.Red_color)
        Mobile_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Password_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        
        //MARK: To Set corner Radius of Buttons
        SignIn_Btn.layer.cornerRadius = appConstants.CornerRadius
        ShareApp_Btn.layer.cornerRadius = appConstants.CornerRadius
        
        
        //To hide forgot password for poc
        forgotPassword_btn?.isHidden = false
        
      }
    //MARK: ViewDidLoad
   override func viewDidLoad() {
        super.viewDidLoad()
    self.apiManager = ApiManager()
    apiManager.delegate = self
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK: - FORGOT PASSWORD
@IBAction  func forgotPasswordEvent(sender: UIButton) {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let ForgotPassword1ViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPassword1ViewController") as! ForgotPassword1ViewController
    ForgotPassword1ViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
    self.addChildViewController(ForgotPassword1ViewController)
        self.view.addSubview(ForgotPassword1ViewController.view)
    ForgotPassword1ViewController.didMove(toParentViewController: self)
}
 
//MARK: OTP Screen For Forgot Password
 func EnterOTPForChangePasswordEvent() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForgotPassword_OTPViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPassword_OTPViewController") as! ForgotPassword_OTPViewController
        ForgotPassword_OTPViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(ForgotPassword_OTPViewController)
        self.view.addSubview(ForgotPassword_OTPViewController.view)
        ForgotPassword_OTPViewController.didMove(toParentViewController: self)
    }

    
//MARK: - Registration Page
@IBAction func MoveToRegitrationPage(sender : UIButton){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let RegisterPageViewController = storyboard.instantiateViewController(withIdentifier: "RegisterPageViewController") as! RegisterPageViewController
    self.present(RegisterPageViewController, animated:false, completion:nil)
    }

//MARK: Sign In Action
@IBAction func SignIn(sender : UIButton){
    CheckFieldsForInput()
}
    
//MARK: Check VAlidation
func CheckFieldsForInput()
{
    
    if(Mobile_textField.text != "")
    {
     mobile_string = Mobile_textField.text! as NSString
   }
    if(Password_textField.text != "")
    {
      password_string = Password_textField.text! as NSString
    }
    
    if(mobile_string.length > 0)||(mobile_string.length <= 12)
    {
        if validate(value: mobile_string as String) == true
        {
            if( password_string != "")
            {
                if(password_string.length<6)
                {
                    MessageString = "Incorrect Password  "
                    appConstants.showAlert(title: "Warning", message: MessageString , controller: self)
                }
                else{
                    MessageString = "Logged In"
                    appConstants.defaults.set(password_string, forKey: "password")
                    LoginApiCall() //Login Api Call
                }
            }
            else{
                MessageString = "Please Enter Correct Password "
                appConstants.showAlert(title: "Warning", message: MessageString , controller: self)
            }
        }
        else{
        MessageString = "Please Enter Correct Mobile Number "
            appConstants.showAlert(title: "Warning", message: MessageString , controller: self)
        }
    }
       //To Do : Call webservice for sign In
}
    
//MARK: Login Api Call
    func LoginApiCall()
{
    let device_id = appConstants.defaults.value(forKey: "deviceID")
    let app_secure_id = device_id as! String+(mobile_string as String)
    let notification_token = appConstants.defaults.value(forKey: "notification_token")
    
    print(mobile_string)
    self.apiManager.ApiLoginCall (action:"login",mobile:mobile_string as String,password:password_string as String,app_secure_id:app_secure_id,device_token: notification_token as! String )
}
    
//MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                let responseData : NSDictionary = responseDictionary .value(forKey: "userData") as! NSDictionary
                 let userId : NSString = responseData.value(forKey: "userId") as! NSString
                 appConstants.defaults.set(userId, forKey: "userId")
                let usermobile : NSString = responseData.value(forKey: "mobile") as! NSString
                appConstants.defaults.set(usermobile, forKey: "mobile")
                let screenToBeShown : NSString = responseData.value(forKey: "registrationComleteStep") as! NSString
                if(i == 1)  // To open temporay UI pages
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let OptionsViewController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
                    self.present(OptionsViewController, animated:false, completion:nil)
                }
                
               else if(screenToBeShown == "1")
                {
                    moveToOTPScreen() //GO TO OTP SCREEN
                }
                else if(screenToBeShown == "2")
                {
                   moveToAdditionalDetailScreen() //GO to Additional Detail Page
                }
                else if(screenToBeShown == "3")
                {
                   moveToVerificationInfoScreen() //Go to Verification Info page
                  appConstants.defaults.set("FromLogin", forKey: "BackHidden")
                }
                else{
                moveToMainScreen()
                }// call this on success response
            }
            else{
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            for dict in responseDictionary {
                print(dict)
                
            }
        }
    }
 //MARK: Move to Main Screen
    func moveToMainScreen()
    {
        
       if let ifNotification = appConstants.defaults.value(forKey: "FromNotifications")
       {
        print("ifNotification : \(String(describing: ifNotification))")
        let Notificationstatus : NSString = ifNotification as! NSString
        if Notificationstatus == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ConfirmSendPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "ConfirmSendPaymentToMobileVC")
            self.present(ConfirmSendPaymentToMobileVC, animated:false, completion:nil)
            appConstants.defaults.set("0", forKey: "FromNotifications")
            let LoginStatus: NSString = "true"
            appConstants.defaults.set(LoginStatus, forKey: "logInStatus")

           
        }
       
       else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
        let LoginStatus: NSString = "true"
        appConstants.defaults.set(LoginStatus, forKey: "logInStatus")
        }
    }
        
        else
       {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
        let LoginStatus: NSString = "true"
        appConstants.defaults.set(LoginStatus, forKey: "logInStatus")
        }
}
 //MARK: Move to OTP Screen
    func moveToOTPScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let LoginOTPViewController = storyboard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
        self.present(LoginOTPViewController, animated:false, completion:nil)
    }
 //MARK: Move to Additional Detail Screen
    func moveToAdditionalDetailScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let AdditionalDetailViewController = storyboard.instantiateViewController(withIdentifier: "AdditionalDetailViewController") as! AdditionalDetailViewController
        self.present(AdditionalDetailViewController, animated:false, completion:nil)
    }
 //MARK: Move to Verification Info Screen
    func moveToVerificationInfoScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VerificationInfoViewController = storyboard.instantiateViewController(withIdentifier: "VerificationInfoViewController") as! VerificationInfoViewController
        self.present(VerificationInfoViewController, animated:false, completion:nil)
    }
    

    
    
//MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 1: //for mobile number
            let maxLength = 10
            let currentString: NSString = Mobile_textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 2: // for password
            let maxLength = 6
            let currentString: NSString = Password_textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
            default:
            break
        }
        return true
    }


//MARK: - To validate mobile
func validate(value: String) -> Bool {
        let PHONE_REGEX = "(\\+[0-9]+[\\- \\.]*)?" + "(\\([0-9]+\\)[\\- \\.]*)?" + "([0-9][0-9\\- \\.]+[0-9])"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
}
