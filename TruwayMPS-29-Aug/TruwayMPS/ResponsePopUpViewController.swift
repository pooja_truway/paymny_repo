//
//  ResponsePopUpViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 14/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ResponsePopUpViewController: UIViewController {
    
    @IBOutlet weak var popUpView : UIView?
    @IBOutlet weak var messageLabel : UILabel?
    @IBOutlet weak var StatusLabel : UILabel?
    @IBOutlet weak var OkButton : UIButton?
    @IBOutlet weak var lineView : UIView?
    
    @IBOutlet weak var InviteButton : UIButton?
    var fromScreen : NSString = ""
    
    var appConstants : AppConstants = AppConstants()
    
    override func viewWillAppear(_ animated: Bool) {
         self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        popUpView?.layer.cornerRadius = 10.0
        InviteButton?.isHidden = true
        
        
        if fromScreen == "send Payment"
        {
            let amount = appConstants.defaults.value(forKey: "amountToSend")as! String
            let mobile = appConstants.defaults.value(forKey: "receiversMobile") as! String
            popUpView?.backgroundColor = UIColor.blue
            StatusLabel?.text = "CONFIRMED"
        messageLabel?.text = "Your payment to \(mobile) for \(amount) has been confirmed"
        }
           
        else if (fromScreen == "Send Payment Failed")
        {
            let amount = appConstants.defaults.value(forKey: "amountToSend")as! String
            let mobile = appConstants.defaults.value(forKey: "receiversMobile") as! String
             popUpView?.backgroundColor = UIColor.red
            StatusLabel?.text = "FAILED"
            messageLabel?.text = "Your payment to \(mobile) for \(amount) has Failed"
        }
         else if (fromScreen == "request Payment"){
            let amount = appConstants.defaults.value(forKey: "amountToRequest")as! String
            let mobile = appConstants.defaults.value(forKey: "payeeMobile") as! String
            StatusLabel?.text = "CONFIRMED"
             popUpView?.backgroundColor = UIColor.blue
            messageLabel?.text = "Payment request for amount \(amount) successfully send to \(mobile)"
        }
        else{
            let amount = appConstants.defaults.value(forKey: "amountToRequest")as! String
            let mobile = appConstants.defaults.value(forKey: "payeeMobile") as! String
            StatusLabel?.text = "FAILED"
            InviteButton?.isHidden = false
            InviteButton?.backgroundColor = UIColor.red
            popUpView?.backgroundColor = UIColor.red
            OkButton?.isHidden = true
            lineView?.isHidden = true
           // messageLabel?.text = "Payment request for amount \(amount) to \(mobile) has failed"
            messageLabel?.text = "This user \(mobile) is not registered with PayMny App.                   Please click on below button to invite the user to download the App"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: OK button Clicked
    @IBAction func OkClicked(sender : UIButton)
    {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
