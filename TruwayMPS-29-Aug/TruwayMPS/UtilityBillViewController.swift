//
//  UtilityBillViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 18/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class UtilityBillViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{
    @IBOutlet var MenuCollectionView : UICollectionView!
     let reuseIdentifier = "cell"
    
    
    let CellImageArray : NSMutableArray =  [
  UIImage(named: "ic_fav_menu_send_money"),UIImage(named: "ic_fav_electricity"),UIImage(named: "ic_fav_dth"),UIImage(named: "ic_fav_metro"),UIImage(named: "ic_fav_broadband"),UIImage(named: "ic_fav_water"),UIImage(named: "ic_fav_landline"),UIImage(named: "ic_fav_gas")]
    
    
    let CellTitleArray : NSMutableArray =  ["MOBILE","ELECTRICITY","DTH","METRO","BROADBAND","WATER","LANDLINE","GAS"]
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 8
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! UtilityMenuCell
        
            cell = cellA
            cellA.imageView?.image = self.CellImageArray[indexPath.item] as? UIImage
        cellA.titleLabel?.text = self.CellTitleArray[indexPath.item] as? String
    
               return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        switch(indexPath.item)
        {
        case 0 :
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let MobileBillViewController = storyboard.instantiateViewController(withIdentifier: "MobileBillViewController") as! MobileBillViewController
            self.present(MobileBillViewController, animated: false, completion: nil)
            
        case 1 :
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ElectricityBillViewController = storyboard.instantiateViewController(withIdentifier: "ElectricityBillViewController") as! ElectricityBillViewController
            self.present(ElectricityBillViewController, animated: false, completion: nil)
            
        case 2 :
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let DTHViewController = storyboard.instantiateViewController(withIdentifier: "DTHViewController") as! DTHViewController
            self.present(DTHViewController, animated: false, completion: nil)

            
        default :
            break
        }
}
    
   //MARK: Back Button Clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }

}
