//
//  UtilityMenuCell.swift
//  TruwayMPS
//
//  Created by Truway India on 18/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class UtilityMenuCell: UICollectionViewCell {
    @IBOutlet weak var view : UIView?
    @IBOutlet weak var imageView : UIImageView?
    @IBOutlet weak var titleLabel : UILabel?
    

override func awakeFromNib() {
    super.awakeFromNib()
    view?.addSubview(imageView!)
    applyPlainShadow(view: view!)
    // Initialization code
}
    
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 6)
        layer.shadowOpacity = 0.12
        layer.shadowRadius = 2
    }

}
