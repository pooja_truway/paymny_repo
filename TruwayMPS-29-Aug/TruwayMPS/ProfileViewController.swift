//
//  ProfileViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 25/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController ,UITabBarDelegate,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,apiManagerDelegate,UITextViewDelegate{
    
     @IBOutlet var menuButton:UIBarButtonItem!
     @IBOutlet var tabBar: UITabBar!
     @IBOutlet weak var scrollview : UIScrollView?
     @IBOutlet weak var ImageView : UIImageView?
     @IBOutlet weak var nameView : UIView?
    @IBOutlet weak var addressView : UIView?
    @IBOutlet weak var emailView : UIView?
    @IBOutlet weak var mobileView : UIView?
    @IBOutlet weak var dobView : UIView?
    
    @IBOutlet weak var passcodeView : UIView?
    
    @IBOutlet var DropDownButton1 : UIButton!
    @IBOutlet var DropDownButton2 : UIButton!
    @IBOutlet var DropDownButton3 : UIButton!
    @IBOutlet var FirstQuestion_textField : UITextField?
    @IBOutlet var SecondQuestion_textField : UITextField?
    @IBOutlet var ThirdQuestion_textField : UITextField?
    @IBOutlet var FourthQuestion_textField : UITextField?
    
    @IBOutlet var FirstAnswer_textField : UITextField?
    @IBOutlet var SecondAnswer_textField : UITextField?
    @IBOutlet var ThirdAnswer_textField : UITextField?
    @IBOutlet var FourthAnswer_textField : UITextField?
    
    @IBOutlet weak var editBtn1 : UIButton?
    @IBOutlet weak var editBtn2 : UIButton?
    @IBOutlet weak var editBtn3 : UIButton?
    
    @IBOutlet weak var nameTextField : UITextField?
    @IBOutlet weak var addressTextView : UITextView?
    @IBOutlet weak var emailTextField : UITextField?
    @IBOutlet weak var mobileTextField : UITextField?
    @IBOutlet weak var dobTextField : UITextField?
    
    @IBOutlet weak var passwordTextField : UITextField?
    var password_string : NSString = ""
    
    @IBOutlet weak var updateButton : UIButton?
    var MessageString  : String = ""
    var questn1 : String = ""
    var ans1 : String = ""
    var questn2 :String = ""
    var ans2 : String = ""
    var questn3 :String = ""
    var ans3 : String = ""
    var questn4 : String = ""

    var ans4 : String = ""
    var name_string : String = ""
    var address_string : String = ""
    var questionArray : NSMutableArray = []
    var answerArray : NSMutableArray = []
    
    
    var FirstQuestion_tableView: UITableView  =   UITableView()
    var SecondQuestion_tableView: UITableView  =   UITableView()
    var ThirdQuestion_tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    let FirstQuestionArrayList : NSArray = ["What is the name of your favorite childhood friend?","What is the name of your favorite teacher's name?","What is the name of your pet?","What is your favorite movie?","What was your favorite sport in high school?","What was the make and model of your first car?","In which town was your first job?","Who is your first teacher?"]
    let QuestionIdList : NSArray = ["1","2","3","4","5","6","7","8"]
   
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        print("Profile Page opened")
        print("Home Screen")
        tabBar.selectedItem = tabBar.items![1] as UITabBarItem
        scrollview?.contentSize = CGSize(width: self.view.frame.size.width, height: 950)
        ImageView?.addSubview(scrollview!)
        scrollview?.addSubview(nameView!)
        nameTextField?.isUserInteractionEnabled = false
        addressTextView?.isUserInteractionEnabled = false
        emailTextField?.isUserInteractionEnabled = false
        mobileTextField?.isUserInteractionEnabled = false
        dobTextField?.isUserInteractionEnabled = false
        passwordTextField?.isUserInteractionEnabled = false
        updateButton?.isHidden = true
        
        DropDownButton1.isUserInteractionEnabled = false
        DropDownButton2.isUserInteractionEnabled = false
        DropDownButton3.isUserInteractionEnabled = false
        
        FirstQuestion_textField?.isUserInteractionEnabled = false
        FirstAnswer_textField?.isUserInteractionEnabled = false
        SecondQuestion_textField?.isUserInteractionEnabled = false
        SecondAnswer_textField?.isUserInteractionEnabled = false
        ThirdQuestion_textField?.isUserInteractionEnabled = false
        ThirdAnswer_textField?.isUserInteractionEnabled = false
        FourthQuestion_textField?.isUserInteractionEnabled = false
        FourthAnswer_textField?.isUserInteractionEnabled = false
        
        //MARK: First Table
        FirstQuestion_tableView = UITableView(frame: CGRect(x: 20, y: 150, width: appConstants.screenWidth-40, height: 300))
        FirstQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.FirstQuestion_tableView.dataSource = self
        self.FirstQuestion_tableView.delegate = self
        self.view.addSubview(FirstQuestion_tableView)
        FirstQuestion_tableView.backgroundColor = UIColor.white
        FirstQuestion_tableView.isHidden = true
        FirstQuestion_tableView.tag = 1
        
        //MARK: Second Table
        
        SecondQuestion_tableView = UITableView(frame: CGRect(x: 20, y: 250, width: appConstants.screenWidth-40, height: 300))
        SecondQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.SecondQuestion_tableView.dataSource = self
        self.SecondQuestion_tableView.delegate = self
        self.view.addSubview(SecondQuestion_tableView)
        SecondQuestion_tableView.backgroundColor = UIColor.white
        SecondQuestion_tableView.isHidden = true
        SecondQuestion_tableView.tag = 2
        
        //MARK: Third Table
        ThirdQuestion_tableView = UITableView(frame: CGRect(x: 30, y: 350, width: appConstants.screenWidth-40, height: 300))
        ThirdQuestion_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.ThirdQuestion_tableView.dataSource = self
        self.ThirdQuestion_tableView.delegate = self
        self.view.addSubview(ThirdQuestion_tableView)
        ThirdQuestion_tableView.backgroundColor = UIColor.white
        ThirdQuestion_tableView.isHidden = true
        ThirdQuestion_tableView.tag = 3
        
        getProfileInfo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self

        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 250
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK: Api To get profile info
    func getProfileInfo()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
       appConstants.showLoadingHUD(to_view: self.view)
        apiManager.ApiProfileInfoCall(action:"profileInfo",userId:userId as! String)
    }
    
    //MARK:  Tab Bar Delegate- called when user changes tab.
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(item.tag == 0)
        {
            print("Home tab is selected")
            OpenHomePage()
        }
        else if(item.tag == 2)
        {
            print("Offer Zone tab is selected")
            OpenOfferZonePage()
        }
        else if(item.tag == 3)
        {
            print("Notification tab is selected")
            OpenNotificationPage()
        }
        else if(item.tag == 4)
        {
            print("Notification tab is selected")
            OpenForeignExchangePage()
        }
        
    }
    
    //MARK: Show Question 1  list
    @IBAction func GetFirstQuestionListAction()
    {
        FirstQuestion_tableView.isHidden = false
        scrollview?.isUserInteractionEnabled = false
    }
    
    //MARK: Show Question 2
    @IBAction func GetSecondQuestionListAction()
    {
        SecondQuestion_tableView.isHidden = false
        scrollview?.isUserInteractionEnabled = false
    }
    
    //MARK: Show Question 3
    @IBAction func GetThirdQuestionListAction()
    {
        ThirdQuestion_tableView.isHidden = false
        scrollview?.isUserInteractionEnabled = false
    }
    
    //MARK: Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirstQuestionArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FirstQuestion_tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(FirstQuestionArrayList[indexPath.row])"
        cell.textLabel!.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        scrollview?.isUserInteractionEnabled = true
        if(tableView.tag == 1){
            FirstQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn1 = (FirstQuestion_textField?.text)!
            FirstQuestion_tableView.isHidden = true
            FirstAnswer_textField?.becomeFirstResponder()
        }
        else if(tableView.tag == 2)
        {
            SecondQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn2 = (SecondQuestion_textField?.text)!
            SecondQuestion_tableView.isHidden = true
            SecondAnswer_textField?.becomeFirstResponder()
        }
        else{
            ThirdQuestion_textField?.text = FirstQuestionArrayList[indexPath.row] as? String
            questn3 = (ThirdQuestion_textField?.text)!
            ThirdQuestion_tableView.isHidden = true
            ThirdAnswer_textField?.becomeFirstResponder()
        }
    }
    
    //MARK: TextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print(textField.tag)
        switch (textField.tag)
        {
        case 1:
            if(FirstQuestion_textField?.text == ""){
                // Question 1
                FirstAnswer_textField?.isUserInteractionEnabled = false
               MessageString = " Please select a question first"
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                FirstAnswer_textField?.isUserInteractionEnabled = true
            }
            break
        case 3:
            // Question 2
            if(SecondQuestion_textField?.text == ""){
                SecondQuestion_textField?.isUserInteractionEnabled = false
                MessageString = " Please select a question first"
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                SecondQuestion_textField?.isUserInteractionEnabled = true
            }
            break
        case 5:
            // Question 3
            if(ThirdQuestion_textField?.text == ""){
                ThirdQuestion_textField?.isUserInteractionEnabled = false
                MessageString = " Please select a question first"
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                ThirdQuestion_textField?.isUserInteractionEnabled = true
            }
            break
        case 7:
            // Question
            break
            
        case 10 : //DOB
            let datePickerView: UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    //MARK: Date Picker
    func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_GB") as Locale! // using Great Britain for 24 hr format
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/YYYY"
        dobTextField?.text = dateFormatter.string(from: sender.date)
    }

   //MARK: To check all question fields
    func checkQuestions()
    {
        ans1 = (FirstAnswer_textField?.text)!
        ans2 = (SecondAnswer_textField?.text)!
        ans3 = (ThirdAnswer_textField?.text)!
        ans4 = (FourthAnswer_textField?.text)!
        questn4 = (FourthQuestion_textField?.text)!
        
        if(questn1 == "")||(ans1 == "")
        {
            MessageString = " Please select first security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn2 == "")||(ans2 == "")
        {
            MessageString = " Please select second security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn3 == "")||(ans3 == "")
        {
            MessageString = " Please select third security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else if(questn4 == "")||(ans4 == "")
        {
            MessageString = " Please select fourth security question/answer"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
            
        else if(questn1 == questn2)||(questn2 == questn3)||(questn1 == questn3)
        {
            MessageString = " Please select different set of security questions"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
        else{
            VerificationInfoApiCall()
        }
    }
    
    //MARK: Call Verification Question Update Api
    func VerificationInfoApiCall()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
         appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiVerificationDetailUpdateCall (action:"verificationDetails",userId:userId as! String,q1:questn1 ,a1:ans1,q2:questn2,a2:ans2,q3:questn3,a3:ans3,q4:questn4,a4:ans4)
    }
    
    
    //MARK: Api Call For Personal Info Edit
    func  AdditionalDetailApiCall()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
      //  let countryname_str = appConstants.defaults.value(forKey: "CountryName") as? NSString
      //  print(countryname_str)
         name_string = (nameTextField?.text!)!
         address_string = (addressTextView?.text!)!
         appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiAdditionalDetailCall (action:"updateAdditionalDetails",userId:userId as! String,firstName:name_string ,lastName:"",address:address_string,city:"",state:"",postalCode:"",countryName:"" )
    }
    
    //MARK: Change Password Api
    func ChangePasswordApiCall()
    {
    let deviceId = appConstants.defaults.value(forKey: "deviceID")
    let userId = appConstants.defaults.value(forKey: "userId")
        appConstants.showLoadingHUD(to_view: self.view)
    self.apiManager.ApiUpdatePasswordCall (action:"updatePassword",deviceId:deviceId as! String,userId: userId as! String,password:password_string as String)
    }
    

    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            appConstants.hideLoadingHUD(for_view: self.view)
           if let responseDictionary : NSDictionary = value as? NSDictionary
           {
            print("responseDictionary : \(responseDictionary)")
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                MessageString = "Information Updated"
                appConstants.showAlert(title: "", message: MessageString, controller: self)
            }
            else{
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
        }
           else{
            let arr : NSArray = (value as? NSArray)!
            print(arr.count)
            for dict in arr{
                let profileDict : NSDictionary = dict as! NSDictionary
                print("dict in array : \(profileDict)")
                name_string = profileDict.value(forKey: "firstName") as! String
                let lastName_string : String = profileDict.value(forKey: "lastName") as! String
                let fullName = "\(name_string) \(lastName_string)"
                address_string = profileDict.value(forKey: "address") as! String
                nameTextField?.text = fullName
                addressTextView?.text = address_string
               
                let dob_string = profileDict.value(forKey: "dob")
                dobTextField?.text = dob_string as? String
                let email_string = profileDict.value(forKey: "emailId")
                emailTextField?.text = email_string as? String
                let mobile_string = profileDict.value(forKey: "mobile")
                mobileTextField?.text = mobile_string as? String
                let question_string = profileDict.value(forKey: "question")
                questionArray.add(question_string)
                let answer_string = profileDict.value(forKey: "answer")
                answerArray.add(answer_string)
                
                
            }
            print(questionArray)
            print(answerArray)
            FirstQuestion_textField?.text = questionArray.object(at: 0) as? String
            questn1 = (questionArray.object(at: 0) as? String)!
            SecondQuestion_textField?.text = questionArray.object(at: 1) as? String
            questn2 = (questionArray.object(at: 1) as? String)!

            ThirdQuestion_textField?.text = questionArray.object(at: 2) as? String
            questn3 = (questionArray.object(at: 2) as? String)!
            FourthQuestion_textField?.text = questionArray.object(at: 3) as? String
            questn4 = (questionArray.object(at: 3) as? String)!

            
            FirstAnswer_textField?.text = answerArray.object(at: 0) as? String
            SecondAnswer_textField?.text = answerArray.object(at: 1) as? String
            ThirdAnswer_textField?.text = answerArray.object(at: 2) as? String
            FourthAnswer_textField?.text = answerArray.object(at: 3) as? String

        }
        
        }
    }
    
    //MARK: Edit button check Personal Info
    var PersonalInfopressed = false
    var personalinfoEdited = false
    @IBAction func EditBtnpressed(sender: AnyObject) {
        
        if !PersonalInfopressed {
            let image = UIImage(named: "ic_edit.png") as UIImage!
            editBtn1?.setImage(image, for: .normal)
            nameTextField?.isUserInteractionEnabled = false
            addressTextView?.isUserInteractionEnabled = false
            
            if personalinfoEdited
            {
            if (nameTextField?.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (addressTextView?.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
            {
                MessageString = "All fields are mandatory"
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            else{
                AdditionalDetailApiCall()
            }
             print("personal info Updated")
            }
            PersonalInfopressed = true
        } else {
            let image = UIImage(named: "ic_tick.png") as UIImage!
            editBtn1?.setImage(image, for: .normal)
            nameTextField?.isUserInteractionEnabled = true
            addressTextView?.isUserInteractionEnabled = true
            emailTextField?.isUserInteractionEnabled = false
            mobileTextField?.isUserInteractionEnabled = false
            dobTextField?.isUserInteractionEnabled = false
            updateButton?.isHidden = false
            updateButton?.tag = 1
            print("personal info edit button clicked")
            nameTextField?.becomeFirstResponder()

            PersonalInfopressed = false
            personalinfoEdited = true
        }
    }
    
    //MARK: Password Edit button 
    var EditPasswordpressed = false
    @IBAction func PasswordEditBtnpressed(sender: AnyObject) {
        
        if !EditPasswordpressed {
            let image = UIImage(named: "ic_edit.png") as UIImage!
            editBtn2?.setImage(image, for: .normal)
            print("Edit Password")
            password_string = (passwordTextField?.text! as NSString?)!
            
            if( password_string != "")
            {
                if(password_string.length<6)
                {
                    MessageString = " password must be of 6 characters"
                    appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
                }
                else{
                    ChangePasswordApiCall() //call change password api
                }
            }
            print("password Updated")
            EditPasswordpressed = true
        } else {
            let image = UIImage(named: "ic_tick.png") as UIImage!
            editBtn2?.setImage(image, for: .normal)
            
            passwordTextField?.isUserInteractionEnabled = true
            print("password edit button clicked")
            passwordTextField?.becomeFirstResponder()
            EditPasswordpressed = false
        }
    }
    
    //MARK: Security Questions Edit button
    var EditSecurityQuestionspressed = false
    var questionsSelected = false
    @IBAction func EditSecurityQuestionspressed(sender: AnyObject) {
        
        if !EditSecurityQuestionspressed {
            let image = UIImage(named: "ic_edit.png") as UIImage!
            editBtn3?.setImage(image, for: .normal)
            print("edit security Questions")
            print(questionsSelected)
            if questionsSelected
            {
            checkQuestions()
            }
            EditSecurityQuestionspressed = true
        } else {
            let image = UIImage(named: "ic_tick.png") as UIImage!
            editBtn3?.setImage(image, for: .normal)
            
            FirstAnswer_textField?.becomeFirstResponder()
            FirstQuestion_textField?.isUserInteractionEnabled = true
            FirstAnswer_textField?.isUserInteractionEnabled = true
            SecondQuestion_textField?.isUserInteractionEnabled = true
            SecondAnswer_textField?.isUserInteractionEnabled = true
            ThirdQuestion_textField?.isUserInteractionEnabled = true
            ThirdAnswer_textField?.isUserInteractionEnabled = true
            FourthQuestion_textField?.isUserInteractionEnabled = true
            FourthAnswer_textField?.isUserInteractionEnabled = true
            updateButton?.isHidden = false
            updateButton?.tag = 3
            
            DropDownButton1.isUserInteractionEnabled = true
            DropDownButton2.isUserInteractionEnabled = true
            DropDownButton3.isUserInteractionEnabled = true
            
            print("security question edit button clicked")
            print("security questions button clicked")
            questionsSelected = true
            EditSecurityQuestionspressed = false
        }
    }
    
    
    //MARK: To Stop Horizontal Scroll of Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
        else{
            scrollView.isDirectionalLockEnabled = true
        }
    }

    func OpenHomePage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        self.navigationController?.pushViewController(MainScreenViewController, animated: false)
        }
    
    func OpenOfferZonePage()
    {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let OfferZoneViewController = storyboard.instantiateViewController(withIdentifier: "OfferZoneViewController") as! OfferZoneViewController
            self.navigationController?.pushViewController(OfferZoneViewController, animated: false)
    }
    
    func OpenNotificationPage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let NotificationViewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    func OpenForeignExchangePage()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForeignExchangeViewController = storyboard.instantiateViewController(withIdentifier: "ForeignExchangeViewController") as! ForeignExchangeViewController
        self.navigationController?.pushViewController(ForeignExchangeViewController, animated: false)
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
            
        case 5: // for password
            let maxLength = 6
            let currentString: NSString = passwordTextField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        default:
            break
        }
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
