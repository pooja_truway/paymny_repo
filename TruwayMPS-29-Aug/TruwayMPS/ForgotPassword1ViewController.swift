//
//  ForgotPassword1ViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 16/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ForgotPassword1ViewController: UIViewController, UITextFieldDelegate,apiManagerDelegate{
    //MARK: Enter Mobile
    @IBOutlet var BackgroundView_Mobile : UIView!
    @IBOutlet var Mobile_textField : UITextField?
    @IBOutlet var cancel_button_MobileView : UIButton!
    @IBOutlet var OK_button_MobileView : UIButton!
     var mobile_string: NSString = ""
    //MARK: Enter OTP
    @IBOutlet var BackgroungView_OTP : UIView!
    @IBOutlet var OTP_textField : UITextField?
    @IBOutlet var ResendCode_button_OTPView : UIButton!
    @IBOutlet var cancel_button_OTPView : UIButton!
    @IBOutlet var OK_button_OTPView : UIButton!
    //MARK: Reset Password
    @IBOutlet var BackgroungView_ResetPassword : UIView!
    @IBOutlet var Password_textField : UITextField?
    @IBOutlet var ConfirmPassword_textField : UITextField?
    @IBOutlet var cancel_button_ResetView : UIButton!
    @IBOutlet var OK_button_ResetView : UIButton!
    var password_string : NSString = ""
    var confirmPassword_string : NSString = ""
    
    var MessageString : String = ""
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
     override func viewWillAppear(_ animated: Bool) {
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        BackgroundView_Mobile.layer.cornerRadius = 10
        BackgroungView_OTP.layer.cornerRadius = 10
        BackgroungView_ResetPassword.layer.cornerRadius = 10
        BackgroungView_OTP.isHidden = true
        BackgroungView_ResetPassword.isHidden = true
        }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("child view")
        self.apiManager = ApiManager()
        apiManager.delegate = self
       
        // Do any additional setup after loading the view.
    }
    
    //MARK: to cancel view
  @IBAction func cancelButtonClicked(sender: UIButton)
    {    print("cancel")
        self.view.removeFromSuperview()
    }
    //MARK: OK Button Click
    @IBAction func OKButtonClicked(sender: UIButton)
    {    print("ok")
        mobile_string = (Mobile_textField?.text)! as String as NSString
        if(mobile_string.length > 0)||(mobile_string.length <= 12 )
        {
            if validate(value: mobile_string as String) == true{
       
            //APi call for forgot password
                ForgotPasswordApiCall()
                
            }
        else{
                MessageString = " Please enter correct mobile number"
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
           }
    }
    
   //MARK: Forgot Password Api Call
    func ForgotPasswordApiCall()
    {
        let deviceId = appConstants.defaults.value(forKey: "deviceID")
        self.apiManager.ApiForgotPasswordCall (action:"forgotPassword",deviceId:deviceId as! String,mobile: mobile_string as String)
        }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                if let responseData : NSDictionary = responseDictionary .value(forKey: "userData") as? NSDictionary
                {
            // Add userId,mobile and countryName to defaults
                let userId : NSString = responseData.value(forKey: "userId") as! NSString
                appConstants.defaults.set(userId, forKey: "userId")
                let mobile : NSString = responseData.value(forKey: "mobile") as! NSString
                appConstants.defaults.set(mobile, forKey: "mobile")
                let country : NSString = responseData.value(forKey: "countryName") as! NSString
                appConstants.defaults.set(country, forKey: "countryName")

                BackgroundView_Mobile.isHidden = true
                BackgroungView_OTP.isHidden = false
                }
                else{
                let screen : NSString = appConstants.defaults.value(forKey: "checkscreen") as! NSString
                    if(screen == "OTP")
                    {
                    print("OTP Verification Done")
                    BackgroundView_Mobile.isHidden = true
                    BackgroungView_OTP.isHidden = true
                    BackgroungView_ResetPassword.isHidden = false
                    }
                    else{
                       
                        MessageString = "Password Updated Successfully"
                        appConstants.showAlert(title: "", message: MessageString, controller: self)
                        self.view.removeFromSuperview() //show login screen
                    }
                }
                
            }
            else{
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
          
        }
    }

    
    //MARK: OK Button Click of Enter OTP
    @IBAction func OKButtonFor_EnterOTP(sender: UIButton)
    {
         ForgotPasswordOTPApiCall()
    }
    
    //MARK: Forgot Password OTp Api Call
    func ForgotPasswordOTPApiCall()
    {
        let screen = "OTP"
        appConstants.defaults.set(screen, forKey: "checkscreen")
        appConstants.defaults.synchronize()
        let deviceId = appConstants.defaults.value(forKey: "deviceID")
        let userId = appConstants.defaults.value(forKey: "userId")
        let otpString = OTP_textField?.text
        self.apiManager.ApiResetPasswordOTPCall (action:"resetPasswordOTP",deviceId:deviceId as! String,userId: userId as! String,otp:otpString!)
    }
    
    //MARK: Ok Button Click for Reset Password
    @IBAction func OKButtonFor_ResetPassword(sender: UIButton)
    {
        password_string = (Password_textField?.text)! as String as NSString
        confirmPassword_string = (ConfirmPassword_textField?.text)! as String as NSString
        if(password_string == confirmPassword_string)&&( password_string != ""){
            //To Do - call webservice for reset Password
            UpdatePasswordApiCall()
            
       // self.view.removeFromSuperview()
        }
        else{
            MessageString = " Password and Confirm Password does not match"
            appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
        }
    }
    
//MARK: Update Password Api Call
    func UpdatePasswordApiCall()
    {
        let screen = "UpdatePassword"
        appConstants.defaults.set(screen, forKey: "checkscreen")
        appConstants.defaults.synchronize()
        let deviceId = appConstants.defaults.value(forKey: "deviceID")
        let userId = appConstants.defaults.value(forKey: "userId")
        self.apiManager.ApiUpdatePasswordCall (action:"updatePassword",deviceId:deviceId as! String,userId: userId as! String,password:password_string as String)
    }
    
    //MARK: Resend Code Action
    @IBAction func ResendCode(sender: UIButton)
    {
        //To Do
    }
    
    //MARK: Password Validation
    func isPwdCorrect(password: String , confirmPassword : String) -> Bool {
        if password.characters.count <= 6 && confirmPassword.characters.count <= 6{
            return true
        }
        else{
            return false
        }
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 1: //for mobile number
            let maxLength = 10
            let currentString: NSString = Mobile_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 3: // for password
            let maxLength = 6
            let currentString: NSString = Password_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 4: // for confirm password
            let maxLength = 6
            let currentString: NSString = ConfirmPassword_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 2: // for OTP
            let maxLength = 6
            let currentString: NSString = OTP_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }
    
    //MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
}
