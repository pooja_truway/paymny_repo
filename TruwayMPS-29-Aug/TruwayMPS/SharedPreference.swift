//
//  SharedPreference.swift
//  TruwayMPS
//
//  Created by Pooja on 05/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SharedPreference: NSObject {
    var strSample = NSString()
    static let sharedInstance:SharedPreference = {
        let instance = SharedPreference ()
        return instance
    } ()
    
    // MARK: Init
    override init() {
        print("My Class Initialized")
        // initialized with variable or property
        strSample = "My String"
    }


}
