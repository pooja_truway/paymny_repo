//
//  ScanQRCodeVC.swift
//  TruwayMPS
//
//  Created by Pooja on 04/07/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import AVFoundation

class ScanQRCodeVC: UIViewController , AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var messageLabel:UILabel!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    var appConstants : AppConstants = AppConstants()
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            // Detect all the supported bar code
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResize
            videoPreviewLayer?.frame = CGRect(x:0 , y:60, width:self.view.frame.width, height: self.view.frame.height-60)
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture
            captureSession?.startRunning()
            
            // Move the message label to the top view
            view.bringSubview(toFront: messageLabel)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
    }
    
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "No barcode/QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedBarCodes.contains(metadataObj.type) {
        
        let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
        qrCodeFrameView?.frame = barCodeObject!.bounds
            
            
        if metadataObj.stringValue != nil {
        print(metadataObj.stringValue)
                
        let metadataObjStr : String = metadataObj.stringValue! as String
        print("string: \(metadataObjStr)")
                
        let data1 = metadataObjStr.data(using: .utf8)
        print("data1 :\(data1)")
                
            
    let resultData = NSData(base64Encoded: data1!, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
            
        //Convert NSData to NSString
    let decodedNSString = NSString(data: resultData! as Data, encoding: String.Encoding.utf8.rawValue)!
            
        print("decodedNSString :\(decodedNSString)")
       
        self.convertStringToDictionary(text: decodedNSString as String)
            
            let data : NSMutableDictionary = self.convertStringToDictionary(text:  decodedNSString as String) as! NSMutableDictionary
                
            print("abc \(data)")
            let userId = data.value(forKey: "userId")
            let mobile_String = data.value(forKey: "mobile")
            let amount_string = data.value(forKey: "amount")
                
            appConstants.defaults.set(mobile_String, forKey: "receiversMobile")
            appConstants.defaults.setValue(amount_string, forKey: "amountToSend")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ConfirmSendPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "ConfirmSendPaymentToMobileVC") as! ConfirmSendPaymentToMobileVC
              appConstants.defaults.set("5", forKey: "paymentMethod")
                self.present(ConfirmSendPaymentToMobileVC, animated:false, completion:nil)
           
            }
        }
       
    }
    
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }


}
