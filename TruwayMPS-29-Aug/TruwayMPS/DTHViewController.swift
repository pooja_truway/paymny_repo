//
//  DTHViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 23/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class DTHViewController: UIViewController {
    
    @IBOutlet weak var mobile_TextField : UITextField?
    @IBOutlet weak var paymentButton : UIButton?
    @IBOutlet weak var operator_TextField : UITextField?
    @IBOutlet weak var amount_TextField : UITextField?
    
    @IBOutlet weak var operator_Image : UIImageView?
    
    var operatorString : NSString = ""
    var index : Int = 0
    var MessageString : String = ""
    var fromScreen : NSString = ""

    
    override func viewWillAppear(_ animated: Bool) {
        operator_TextField?.text = operatorString as String

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Action
    @IBAction func BackClicked(sender : UIButton)
    {
       if(fromScreen == "Main")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.present(SWRevealViewController, animated:false, completion:nil)
        }
       else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let UtilityBillViewController = storyboard.instantiateViewController(withIdentifier: "UtilityBillViewController") as! UtilityBillViewController
        self.present(UtilityBillViewController, animated: false, completion: nil)
        }
    }
    
    //MARK: Select Operator Button Clicked
    @IBAction func GetOperatorList(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SelectOperatorViewController = storyboard.instantiateViewController(withIdentifier: "SelectOperatorViewController") as! SelectOperatorViewController
        SelectOperatorViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(SelectOperatorViewController)
        SelectOperatorViewController.fromScreen = "DTH"
        self.view.addSubview(SelectOperatorViewController.view)
        SelectOperatorViewController.didMove(toParentViewController: self)
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
