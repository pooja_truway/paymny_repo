//
//  AddCardDetailsVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//
import UIKit

class AddCardDetailsVC: UIViewController,UITextFieldDelegate {
    @IBOutlet var cardName_textField : UITextField!
    @IBOutlet var cardNumber_textField : UITextField!
    @IBOutlet var expiryDate_textField : UITextField!
    @IBOutlet var cvv_textField : UITextField!
    @IBOutlet var saveCardName_textField : UITextField!
    
    var CardInfoDictionary:Dictionary<String,String> = [:]
   // var CardListArray : NSMutableArray = []
    var CardListArray : NSMutableArray = NSMutableArray()
    
    var cardNameString : NSString = ""
    var cardNumberString : NSString = ""
    var expiryDateString : NSString = ""
    var cvvCodeString : NSString = ""
    var MessageString : NSString = ""
    var SaveCardAsString : NSString = ""
    
    @IBOutlet var doneButton: UIButton!
    
    var appConstants : AppConstants = AppConstants()
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        doneButton.layer.cornerRadius = 15.0
        
    }
    
    @IBAction func cancelButtonClicked(sender: UIButton)
    {    print("cancel pressed")
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    //MARK: TextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print(textField.tag)
        switch (textField.tag)
        {
        case 0:
            print("name")
            break
        case 1:
            print("card number")
            break
        case 2:
            print("expiry date")
            let datePickerView: MonthYearPickerView = MonthYearPickerView()
            textField.inputView = datePickerView
            datePickerView.onDateSelected = { (month: Int, year: Int) in
            let expiryDate = String(format: "%02d/%d", month, year)
            print("expiryDate \(expiryDate)")
            self.expiryDate_textField.text = expiryDate
            }
            break

        case 3:
            print("cvv")
            break
            
        case 4:
            print("save card as")
            break
       
        default:
            print("default")
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
      
 //MARK: Check Validation
    func validateInputFields()
    {
        cardNameString = cardName_textField.text! as NSString
        cardNumberString = cardNumber_textField.text! as NSString
        expiryDateString = expiryDate_textField.text! as NSString
        cvvCodeString = cvv_textField.text! as NSString
        SaveCardAsString = saveCardName_textField.text! as NSString
        
if(cardNameString != "")
    {
    if(cardNumberString != "" )
    {
      if(expiryDateString != "")
        {
          if(cvvCodeString != "")
            {
                if(SaveCardAsString != "")
                {
                  MoveToValidateScreen()
                }
                else{
                    MessageString = "Please Enter Save Card As"
                    appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
                }
            }
          else{
            MessageString = "Please Enter CVV code"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
        }
        
      else{
        MessageString = "Please Enter Expiry Date of Card"
        appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
    }
    else{
        MessageString = "Please Enter Valid Card Number"
        appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
}
else{
  MessageString = "Please Enter Card Holder Name"
  appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
    }
}
  
    //MARK: Done Button Clicked
    @IBAction func DoneButtonClicked(sender: UIButton)
    {    print("move to validate screen ")
        validateInputFields()
    }
    //MARK: Move to Validate Screen
    func MoveToValidateScreen()
    {
      //Add Input values to dictionary
        
        CardInfoDictionary.updateValue(cardNameString as String, forKey: "cardHolderName")
        CardInfoDictionary.updateValue(cardNumberString as String, forKey: "cardNumber")
        CardInfoDictionary.updateValue(expiryDateString as String, forKey: "cardExpiryDate")
        CardInfoDictionary.updateValue(cvvCodeString as String, forKey: "cardCVVCode")
        CardInfoDictionary.updateValue(SaveCardAsString as String, forKey: "cardSavedAs")
        print("card detail dictionary: \(CardInfoDictionary)")
        
        
        if appConstants.defaults.value(forKey:"cardListArray") != nil
        {
            let array = appConstants.defaults.value(forKey: "cardListArray") as! NSArray
            self.CardListArray = NSMutableArray(array: array)
            
             print(">>>>>>>>>>>>>>:\(CardListArray)")
            
           CardListArray.insert(CardInfoDictionary, at: 0)
          print("card list from defaults is not 0")
        
        }
        else{
        CardListArray.insert(CardInfoDictionary, at: 0)
             print("card list from defaults is empty")
        }
        print("array \(CardListArray)")
        
        appConstants.defaults.set(CardListArray, forKey: "cardListArray")
        print("array count \(CardListArray.count)")
        
       if let arr  = (appConstants.defaults.value(forKey: "cardListArray") )
       {
        print("save as \(String(describing: (arr as AnyObject).value(forKey: "cardSavedAs")))")//save card as in defaults
        }
        
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let ValidatePhoneVC = storyboard.instantiateViewController(withIdentifier: "ValidatePhoneVC") as! ValidatePhoneVC
    self.present(ValidatePhoneVC, animated:false, completion:nil)
     }
    
//MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 1: //for card number
            let maxLength = 16
            let currentString: NSString = cardNumber_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 3: // for CVV
            let maxLength = 3
            let currentString: NSString = cvv_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
              default:
            break
        }
        return true
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
