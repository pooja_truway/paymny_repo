//
//  SendPaymentToMobileVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Pooja. All rights reserved.
//

import UIKit

class SendPaymentToMobileVC: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var amount_textField : UITextField?
    @IBOutlet weak var receiverMobile_textFiled : UITextField?
    @IBOutlet weak var comment_textField : UITextField?
    @IBOutlet weak var next_Button : UIButton?
    
    // @IBOutlet var menuButton:UIBarButtonItem!
    
    var amount_String : NSString = ""
    var mobile_String : NSString = ""
    var comments_String : NSString = ""
    var MessageString : NSString = ""
    
    var appConstants : AppConstants = AppConstants()

    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 0
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
            tapRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
            self.view.addGestureRecognizer(tapRecognizer)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func NextButtonClicked (sender: UIButton)
    {
      amount_String = amount_textField!.text! as NSString
        mobile_String = receiverMobile_textFiled!.text! as NSString
        appConstants.defaults.set(amount_String, forKey: "amountToSend")
        appConstants.defaults.set(mobile_String, forKey: "receiversMobile")
        
        let myDouble = NumberFormatter().number(from: amount_String as String)?.doubleValue
        if(myDouble?.isLessThanOrEqualTo(1.0))!
        {
            print("value is less than 0")
            MessageString = "Please Enter Valid Amount"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }

        
       else if (amount_String != "")
     {
        
        if(mobile_String.length > 0)||(mobile_String.length <= 10 )
        {
            if validate(value: mobile_String as String) == true
            {
                 MoveToNextScreen() // to move to next view
            }
            else
            {
                MessageString = "Please Enter Receiver's Mobile Number"
                appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
        }
           }
     else{
        MessageString = "Please Enter Amount To Send"
        appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
}
    func MoveToNextScreen()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ConfirmSendPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "ConfirmSendPaymentToMobileVC") as! ConfirmSendPaymentToMobileVC
          appConstants.defaults.set("1", forKey: "paymentMethod")
        self.present(ConfirmSendPaymentToMobileVC, animated:false, completion:nil)

    }
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        print("BACK PRESSED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
            
        case 0: //for amount
            let maxLength = 5
            let currentString: NSString = amount_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 1: //for mobile number
            let maxLength = 10
            let currentString: NSString = receiverMobile_textFiled!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
       
        default:
            break
        }
        return true
    }
    
    //MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
