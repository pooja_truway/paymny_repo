//
//  RequestPaymentToMobileVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Pooja. All rights reserved.
//

import UIKit

class RequestPaymentToMobileVC: UIViewController , UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet weak var amount_textField : UITextField?
    @IBOutlet weak var payeeMobile_textFiled : UITextField?
    @IBOutlet weak var comment_textField : UITextField?
    @IBOutlet weak var next_Button : UIButton?
    
    // @IBOutlet var menuButton:UIBarButtonItem!
    
    var amount_String : NSString = ""
    var mobile_String : NSString = ""
    var comments_String : NSString = ""
    var MessageString : NSString = ""
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiManager = ApiManager()
        apiManager.delegate = self
        
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 0
           
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
            tapRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
            self.view.addGestureRecognizer(tapRecognizer)
        }
        
        // Do any additional setup after loading the view.
    }
   //MARK: Next Button Clicked
       @IBAction func NextButtonClicked (sender: UIButton)
    {
        amount_String = amount_textField!.text! as NSString
        mobile_String = payeeMobile_textFiled!.text! as NSString
        appConstants.defaults.set(amount_String, forKey: "amountToRequest")
        appConstants.defaults.set(mobile_String, forKey: "payeeMobile")
        print(amount_String)
         let myDouble = NumberFormatter().number(from: amount_String as String)?.doubleValue
        if(myDouble?.isLessThanOrEqualTo(1.0))!
        {
            print("value is less than 0")
            MessageString = "Please Enter Valid Amount"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
        
       else if (amount_String != "") 
        {
            if(mobile_String.length > 0)||(mobile_String.length <= 10 )
            {
                if validate(value: mobile_String as String) == true
                {
                    MoveToNextScreen() // to move to next view
                }
        else
            {
                MessageString = "Please Enter Receiver's Mobile Number"
                appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
        }
                
        }
        else{
            MessageString = "Please Enter Amount To Send"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
    }
    func MoveToNextScreen()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ConfirmRequestPaymentToMobileVC = storyboard.instantiateViewController(withIdentifier: "ConfirmRequestPaymentToMobileVC") as! ConfirmRequestPaymentToMobileVC
         appConstants.defaults.set("3", forKey: "paymentMethod")
        self.present(ConfirmRequestPaymentToMobileVC, animated:false, completion:nil)
        
    }
    //MARK: To Call Api
    func CallApiToRequestMoney()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        let mobile = appConstants.defaults.value(forKey: "mobile") as? String
        
        self.apiManager.ApiRequestMoneyFromMobileCall (action:"paymentRequest",amount:amount_String as String,payeeMobile:mobile_String as String,comments:comments_String as String,userId:userId as! String,mobile:mobile!, paymentType: "")
    }
    
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        print("BACK PRESSED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 1: //for mobile number
            let maxLength = 10
            let currentString: NSString = payeeMobile_textFiled!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 0: //for amount
            let maxLength = 5
            let currentString: NSString = amount_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
                return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                SWRevealViewController.fromScreen = "SEND REQUEST SUCCESSFUL"
                self.present(SWRevealViewController, animated:false, completion:{() in self.completion()})
                
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                SWRevealViewController.fromScreen = "SEND REQUEST FAILED"
                self.present(SWRevealViewController, animated:false, completion:{() in self.completion()})
            }
            
        }
    }
    
    func completion()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        MainScreenViewController.fromScreen = "SEND PAYMENT SUCCESSFUL"
        print("SUCCESS MAIN VIEW")
    }
    
    //MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
