//
//  LoginOTPViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 31/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class LoginOTPViewController: UIViewController,apiManagerDelegate{
    
    @IBOutlet var ResendCode_btn : UIButton!
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    override func viewWillAppear(_ animated: Bool) {
         ResendCode_btn.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color)
         ResendCode_btn.layer.cornerRadius = appConstants.CornerRadius
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
    }

  //MARK: Send Validation Code Button Action
    @IBAction func SendCodeButtonClicked(sender: UIButton)
    {
      //Call OTP Request API
        let user_id = appConstants.defaults.value(forKey: "userId")
        self.apiManager.ApiOTPRequestCall (action:"OTPRequest",userId:user_id as! String )
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage == "success")
            {
                let responseData : NSDictionary = responseDictionary .value(forKey: "userData") as! NSDictionary
              //To handle otp response
                let otp = responseData.value(forKey: "otp") as! NSString
                print(otp)
                appConstants.defaults.set(otp, forKey: "OTP")
                showOTPView() // call this on success response
                            }
            else{
              let MessageString = ("Please Try Again After some time : \(responseMessage)")
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            
        }
    }
//MARK: Show OTP Pop Up
    func showOTPView()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let OTPViewController = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        OTPViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(OTPViewController)
        OTPViewController.fromScreen = "Login"
        self.view.addSubview(OTPViewController.view)
        OTPViewController.didMove(toParentViewController: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
