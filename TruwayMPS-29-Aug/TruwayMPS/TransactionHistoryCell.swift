//
//  TransactionHistoryCell.swift
//  TruwayMPS
//
//  Created by Pooja on 07/07/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

protocol CellDelegate {
    func moreTapped(cell: TransactionHistoryCell)
}


class TransactionHistoryCell: UITableViewCell {
    
    @IBOutlet weak var date_Label: UILabel?
    @IBOutlet weak var title_Label : UILabel?
    @IBOutlet weak var amount_Label : UILabel?
    
    @IBOutlet weak var transactionId_Label: UILabel?
    @IBOutlet weak var transactionId_value: UILabel?
    @IBOutlet weak var url_Label : UILabel?
    
    @IBOutlet weak var viewDetailsButton : UIButton?
    @IBOutlet weak var iconImage : UIImageView?
    @IBOutlet weak var manageSpacing : NSLayoutConstraint?
    @IBOutlet weak var manageHeight : NSLayoutConstraint?
    
    @IBOutlet weak var detailLabel : UILabel?
    
    var isExpanded: Bool = false
    var delegate: CellDelegate?
    var detailsShow : Bool = false
    
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        if sender is UIButton {
            isExpanded = !isExpanded
           
            viewDetailsButton?.setTitle(isExpanded ? "View Details" : "Hide Details", for: .normal)
     
            viewDetailsButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
            
            if(viewDetailsButton?.titleLabel?.text == "View Details")
            {
                transactionId_Label?.isHidden = false
                transactionId_value?.isHidden = false
                
            let transactionId_Label_height: CGFloat = getLabelHeight(transactionId_Label!)
                print("transactionId_Label_height \(transactionId_Label_height)")
                
                url_Label?.isHidden = false
                detailsShow = true
                manageSpacing?.constant = 5
                manageHeight?.constant = 12
            }
            else{
                transactionId_Label?.isHidden = true
                transactionId_value?.isHidden = true
                
                 url_Label?.isHidden = true
                detailsShow = false
                manageSpacing?.constant = 20
                manageHeight?.constant = 0
            }
            delegate?.moreTapped(cell: self)
            
        }
    }
    
    func getLabelHeight(_ label: UILabel) -> CGFloat {
        
        let constraint = CGSize(width: CGFloat(label.frame.size.width), height: CGFloat(CGFloat.greatestFiniteMagnitude))
        
        var size: CGSize
        let context = NSStringDrawingContext()
        let boundingBox: CGSize? = label.text?.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: context).size
        size = CGSize(width: CGFloat(ceil((boundingBox?.width)!)), height: CGFloat(ceil((boundingBox?.height)!)))
        return size.height
    }
    
    public func myInit() {
        
        isExpanded = false
        
       // transactionId_Label.text = "TransactionID : 123673424"
        transactionId_Label?.isHidden = true
        transactionId_value?.isHidden = true
        url_Label?.isHidden = true
        manageSpacing?.constant = 5
       // labelTitle.text = "fxhsg"
        //	labelBody.text = theBody
        //	labelBody.numberOfLines = 0
        //	sizingLabel.text = theBody
       // sizingLabel.numberOfLines = 2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
