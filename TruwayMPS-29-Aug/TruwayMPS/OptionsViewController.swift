//
//  OptionsViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 19/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController, UITextFieldDelegate,apiManagerDelegate {
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    @IBOutlet weak var send_btn : UIButton?
    @IBOutlet weak var request_btn : UIButton?
    
    override func viewWillAppear(_ animated: Bool) {
        send_btn?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color)
        send_btn?.layer.cornerRadius = 5.0
        request_btn?.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SendPayment(Sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SendPaymentViewController = storyboard.instantiateViewController(withIdentifier: "SendPaymentViewController") as! SendPaymentViewController
        self.present(SendPaymentViewController, animated:false, completion:nil)
    }
    
//    @IBAction func RequestPayment(Sender : UIButton)
//    {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let RequestPaymentViewController = storyboard.instantiateViewController(withIdentifier: "RequestPaymentViewController") as! RequestPaymentViewController
//        self.present(RequestPaymentViewController, animated:false, completion:nil)
//    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
               
                
            }
            else{
              let MessageString = responseMessage as String as String as NSString
                appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
            
        }
    }

}
