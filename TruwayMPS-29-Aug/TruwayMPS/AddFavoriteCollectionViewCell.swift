//
//  AddFavoriteCollectionViewCell.swift
//  TruwayMPS
//
//  Created by Pooja on 06/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class AddFavoriteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var FavoriteCellImage: UIImageView!
    @IBOutlet weak var FavoriteCellTitleLabel: UILabel!
}
