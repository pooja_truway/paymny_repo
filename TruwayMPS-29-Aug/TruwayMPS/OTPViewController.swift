//
//  OTPViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 18/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController ,UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet var OTP_View : UIView!
    @IBOutlet var Cancel_btn : UIButton!
    @IBOutlet var Ok_btn : UIButton!
    @IBOutlet var ResendCode_btn : UIButton!
    @IBOutlet var OTP_textField: UITextField!
    var fromScreen : NSString = ""
    
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        OTP_View.layer.cornerRadius = 10
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
    }
    //MARK: Cancel Button Action
    @IBAction func cancelButtonClicked(sender: UIButton)
    {    print("cancel pressed")
         print("From screen :\(fromScreen)")
        
        if(fromScreen == "Registration")
        {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let RegisterPageViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterPageViewController") as! RegisterPageViewController
        self.present(RegisterPageViewController, animated:false, completion:nil)
        }
        else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let LoginOTPViewController = storyBoard.instantiateViewController(withIdentifier: "LoginOTPViewController") as! LoginOTPViewController
            self.present(LoginOTPViewController, animated:false, completion:nil)
        }
    }
    //MARK: OK Button - To send OTP for Verification
    @IBAction func MoveToAdditionalDetailPage()
    {
        let user_id = appConstants.defaults.value(forKey: "userId")
        let otp = appConstants.defaults.value(forKey: "OTP")
        self.apiManager.ApiOTPVerificationCall (action:"OTPVerification",userId:user_id as! String,otp: otp as! String)

    }
    

//MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let AdditionalDetailViewController = storyboard.instantiateViewController(withIdentifier: "AdditionalDetailViewController") as! AdditionalDetailViewController
                self.present(AdditionalDetailViewController, animated:false, completion:nil)
                
            }
            else{
                var MessageString = ""
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
            //MARK: After getting response move to verification page
        }
    }
   
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 1: // for OTP
            let maxLength = 6
            let currentString: NSString = OTP_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }
