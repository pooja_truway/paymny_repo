//
//  TransactionHistoryVCViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 03/07/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class TransactionHistoryVCViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CellDelegate,apiManagerDelegate{
    
    @IBOutlet weak var allButton : UIButton?
    @IBOutlet weak var paidButton : UIButton?
    @IBOutlet weak var receivedButton : UIButton?
    @IBOutlet weak var webButton : UIButton?
    @IBOutlet weak var cancelButton : UIButton?
    
    @IBOutlet weak var tableView : UITableView?
    var selectedIndexPaths = [Any]()
    var indexPathSelecetd : Int = 0
    var viewDetailsClicked : Bool = false
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    var month_array : NSMutableArray = []
    var month_title : NSString = ""
    var previous_month : NSString = ""
    var orderedSet : NSOrderedSet = []
    var history_monthwise : NSMutableArray = []
    
    var transaction_status_array : NSMutableArray = []
    var transaction_status_string : NSString = ""
    
    var filteredArray_month : NSMutableArray = []
    var filteredArray_month1 : NSMutableArray = []
    var  date_array_section0 : NSArray = []
    var  date_array_section1 : NSArray = []
    
    var  title_array_section0 : NSArray = []
    var  title_array_section1 : NSArray = []
    
    var  amount_array_section0 : NSArray = []
    var  amount_array_section1 : NSArray = []
    
    var  transactionId_array_section0 : NSArray = []
    var  transactionId_array_section1 : NSArray = []
    
    var  web_array_section0 : NSArray = []
    var  web_array_section1 : NSArray = []
    
    var  cancel_array_section0 : NSArray = []
    var  cancel_array_section1 : NSArray = []
    
    var  received_array_section0 : NSArray = []
    var  received_array_section1 : NSArray = []
    
    var  paid_array_section0 : NSArray = []
    var  paid_array_section1 : NSArray = []
    
    var  Newpaid_array_section0 : NSMutableArray = []
    var  Newpaid_array_section1 : NSMutableArray = []
    
    var tag : Int = 1
    var selectedIndex : NSInteger! = -1
    
    var cellHeight : NSLayoutConstraint?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.estimatedRowHeight = 150; //Set this to any value that works for you.
        self.apiManager = ApiManager()
        apiManager.delegate = self


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        allButton?.backgroundColor =  UIColor.white
        paidButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
        //
        allButton?.tag = 1
        paidButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        receivedButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        webButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        cancelButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
        paidButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        receivedButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        webButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        cancelButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        getTranscationDetails()
    }
    
    //MARK: api to get transaction details
    func getTranscationDetails()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiTransactionHistoryCall (action:"transactionDetails",userId:userId as! String)
    }
    //MARK: All Button pressed
    var Allpressed = false
    @IBAction func AllButton(sender: UIButton) {
        
          allButton?.backgroundColor =  UIColor.white
          allButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
            allButton?.tag = 1
        tag = 1
       
            paidButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            receivedButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            webButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
          cancelButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
            paidButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            receivedButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            webButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        cancelButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
           
            print("All")
        
        let attributeValue = orderedSet.object(at: 0)
        
        let namePredicate =
            NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
        
        let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
        filteredArray_month = filteredArray as! NSMutableArray
        print("month history array count \(filteredArray_month.count)")
        
        
        /*   let attributeValue1 = orderedSet.object(at: 1)
         let namePredicate1 =
         NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
         
         let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
         filteredArray_month1 = filteredArray1 as! NSMutableArray
         print("july array count \(filteredArray_month1.count)")*/ // for months more than 1
        
        date_array_section0 = filteredArray_month.value(forKey: "trasanctionTime") as! NSArray
        date_array_section1 = filteredArray_month1.value(forKey: "trasanctionTime") as! NSArray
        
        title_array_section0 = filteredArray_month.value(forKey: "title") as! NSArray
        title_array_section1 = filteredArray_month1.value(forKey: "title") as! NSArray
        
        amount_array_section0 = filteredArray_month.value(forKey: "amount") as! NSArray
        amount_array_section1 = filteredArray_month1.value(forKey: "amount") as! NSArray
        
        transactionId_array_section0 = filteredArray_month.value(forKey: "transactionId") as! NSArray
        transactionId_array_section1 = filteredArray_month1.value(forKey: "transactionId") as! NSArray
        tableView?.reloadData()
    }
    
    //MARK: Paid Button
    var Paidpressed = false
    @IBAction func PaidButton(sender: UIButton) {
         Newpaid_array_section0.removeAllObjects()
            paidButton?.backgroundColor =  UIColor.white
            paidButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
            paidButton?.tag = 2
        tag = 2
       
            allButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            receivedButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            webButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        cancelButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
            allButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            receivedButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            webButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
         cancelButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            print("Paid")
        
        let attributeValue = orderedSet.object(at: 0)
        
        let namePredicate =
            NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
        print("full history \(history_monthwise)")
        let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
        filteredArray_month = filteredArray as! NSMutableArray
        print("august \(filteredArray_month.count)")
        
        //Get received transactions for august
        let webFilter = "Paid"
        let webPredicate = NSPredicate(format: "transactionStatus like %@",webFilter as CVarArg)
        let webfilteredArray = filteredArray_month.filter { webPredicate.evaluate(with: $0) };
        paid_array_section0 = webfilteredArray as! NSMutableArray
        print("august paid \(paid_array_section0.count)")
        print("august paid content \(paid_array_section0)")
        
//        let webFilter_Paid = "Recieved"
//        let webPredicate_Paid = NSPredicate(format: "Recieved like %@",webFilter_Paid as CVarArg)
//        let webfilteredArray_Paid = paid_array_section0.filter { webPredicate_Paid.evaluate(with: $0) };
//        paid_array_section0 = webfilteredArray_Paid as! NSMutableArray
        for dict in paid_array_section0
        {
            let abc = (dict as! NSDictionary).value(forKey: "Recieved")
            print("abc : \(abc)")
            
            if let str  = abc as? String
            {
                print("str value : \(str)")
            }
            else{
            print("str value else case: \(dict)")
                Newpaid_array_section0.insert(dict, at:Newpaid_array_section0.count)
            }
        }
        print("new array : \(Newpaid_array_section0.count)")
        paid_array_section0 = Newpaid_array_section0
        
        
        print("august paid after filter\(paid_array_section0.count)")
        print("august paid after filter content \(paid_array_section0)")
        
        /* let attributeValue1 = orderedSet.object(at: 1)
         let namePredicate1 =
         NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
         let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
         filteredArray_month1 = filteredArray1 as! NSMutableArray
         print("july \(filteredArray_month1.count)")*/ // month is more than 1
        
        //Get web transactions for july
        let webFilter1 = "Paid"
        let webPredicate1 = NSPredicate(format: "transactionStatus like %@",webFilter1 as CVarArg)
        let webfilteredArray1 = filteredArray_month1.filter { webPredicate1.evaluate(with: $0) };
        paid_array_section1 = webfilteredArray1 as! NSMutableArray
        print("july web \(paid_array_section1.count)")
        
        //GET DATE AND TIME FOR TRANSACTIONS
        date_array_section0 = paid_array_section0.value(forKey: "trasanctionTime") as! NSArray
        date_array_section1 = paid_array_section0.value(forKey: "trasanctionTime") as! NSArray
        
        title_array_section0 = paid_array_section0.value(forKey: "title") as! NSArray
        title_array_section1 = paid_array_section0.value(forKey: "title") as! NSArray
        
        amount_array_section0 = paid_array_section0.value(forKey: "amount") as! NSArray
        amount_array_section1 = paid_array_section0.value(forKey: "amount") as! NSArray
        
        transactionId_array_section0 = paid_array_section0.value(forKey: "transactionId") as! NSArray
        transactionId_array_section1 = paid_array_section0.value(forKey: "transactionId") as! NSArray
        
        tableView?.reloadData()

      }
    
       //MARK: Received  Button
    var Receivedpressed = false
    @IBAction func ReceivedButton(sender: UIButton) {
        
            receivedButton?.backgroundColor =  UIColor.white
            receivedButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
            receivedButton?.tag = 3
            tag = 3
            allButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            paidButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            webButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        cancelButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
            allButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            paidButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            webButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
         cancelButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            print("Received")
        
        
        let attributeValue = orderedSet.object(at: 0)
        
        let namePredicate =
            NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
        print("full history \(history_monthwise)")
        let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
        filteredArray_month = filteredArray as! NSMutableArray
        print("august \(filteredArray_month.count)")
        
        //Get received transactions for august
        let webFilter = "Recieved"
        let webPredicate = NSPredicate(format: "Recieved like %@",webFilter as CVarArg)
        let webfilteredArray = filteredArray_month.filter { webPredicate.evaluate(with: $0) };
        received_array_section0 = webfilteredArray as! NSMutableArray
        print("august received \(received_array_section0.count)")
        print("august received content \(received_array_section0)")
        
        
        
        /* let attributeValue1 = orderedSet.object(at: 1)
         let namePredicate1 =
         NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
         let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
         filteredArray_month1 = filteredArray1 as! NSMutableArray
         print("july \(filteredArray_month1.count)")*/ // month is more than 1
        
        //Get web transactions for july
        let webFilter1 = "Recieved"
        let webPredicate1 = NSPredicate(format: "Recieved like %@",webFilter1 as CVarArg)
        let webfilteredArray1 = filteredArray_month1.filter { webPredicate1.evaluate(with: $0) };
        received_array_section1 = webfilteredArray1 as! NSMutableArray
        print("july web \(received_array_section1.count)")
        
        //GET DATE AND TIME FOR TRANSACTIONS
        date_array_section0 = received_array_section0.value(forKey: "trasanctionTime") as! NSArray
        date_array_section1 = received_array_section1.value(forKey: "trasanctionTime") as! NSArray
        
        title_array_section0 = received_array_section0.value(forKey: "title") as! NSArray
        title_array_section1 = received_array_section1.value(forKey: "title") as! NSArray
        
        amount_array_section0 = received_array_section0.value(forKey: "amount") as! NSArray
        amount_array_section1 = received_array_section1.value(forKey: "amount") as! NSArray
        
        transactionId_array_section0 = received_array_section0.value(forKey: "transactionId") as! NSArray
        transactionId_array_section1 = received_array_section1.value(forKey: "transactionId") as! NSArray
        
        tableView?.reloadData()
    }
    
    //MARK: web  Button
    var Webpressed = false
    @IBAction func WebButton(sender: UIButton) {
        
            webButton?.backgroundColor =  UIColor.white
            webButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
            //
            webButton?.tag = 4
            tag = 4
            allButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            paidButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
            receivedButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        cancelButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
            allButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            paidButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
            receivedButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
         cancelButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        let attributeValue = orderedSet.object(at: 0)
        
        let namePredicate =
            NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
        print("full history \(history_monthwise)")
        let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
        filteredArray_month = filteredArray as! NSMutableArray
        print("august \(filteredArray_month.count)")
        
        //Get web transactions for august
        let webFilter = "Fail"
        let webPredicate = NSPredicate(format: "transactionStatus like %@",webFilter as CVarArg)
        let webfilteredArray = filteredArray_month.filter { webPredicate.evaluate(with: $0) };
        web_array_section0 = webfilteredArray as! NSMutableArray
        print("august web \(web_array_section0.count)")
         print("august web content \(web_array_section0)")
        
        
        
       /* let attributeValue1 = orderedSet.object(at: 1)
        let namePredicate1 =
            NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
        let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
        filteredArray_month1 = filteredArray1 as! NSMutableArray
        print("july \(filteredArray_month1.count)")*/ // month is more than 1
        
        //Get web transactions for july
        let webFilter1 = "Fail"
        let webPredicate1 = NSPredicate(format: "transactionStatus like %@",webFilter1 as CVarArg)
        let webfilteredArray1 = filteredArray_month1.filter { webPredicate1.evaluate(with: $0) };
        web_array_section1 = webfilteredArray1 as! NSMutableArray
        print("july web \(web_array_section1.count)")
        
        //GET DATE AND TIME FOR TRANSACTIONS
        date_array_section0 = web_array_section0.value(forKey: "trasanctionTime") as! NSArray
        date_array_section1 = web_array_section1.value(forKey: "trasanctionTime") as! NSArray
        
        title_array_section0 = web_array_section0.value(forKey: "title") as! NSArray
        title_array_section1 = web_array_section1.value(forKey: "title") as! NSArray
        
        amount_array_section0 = web_array_section0.value(forKey: "amount") as! NSArray
        amount_array_section1 = web_array_section1.value(forKey: "amount") as! NSArray
        
        transactionId_array_section0 = web_array_section0.value(forKey: "transactionId") as! NSArray
        transactionId_array_section1 = web_array_section1.value(forKey: "transactionId") as! NSArray

        tableView?.reloadData()
        
     }
    
    //MARK: cancel transaction  Button
    var CancelTransactionpressed = false
    @IBAction func CancelTransactionButton(sender: UIButton) {
        cancelButton?.backgroundColor =  UIColor.white
        cancelButton?.setTitleColor(UIColor.blue, for: UIControlState.normal)
        //
        cancelButton?.tag = 5
        tag = 5
        allButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        paidButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        receivedButton?.backgroundColor =  UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        webButton?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1.0)
        
        allButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        paidButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        receivedButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        webButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        print("canceltransactionpressed")
        
        let attributeValue = orderedSet.object(at: 0)
        
        let namePredicate =
            NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
       // print("full history \(history_monthwise)")
        let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
        filteredArray_month = filteredArray as! NSMutableArray
        print("august \(filteredArray_month.count)")
        
        //Get web transactions for august
        let webFilter = "Pending"
        let webPredicate = NSPredicate(format: "transactionStatus like %@",webFilter as CVarArg)
        let webfilteredArray = filteredArray_month.filter { webPredicate.evaluate(with: $0) };
        cancel_array_section0 = webfilteredArray as! NSMutableArray
        print("august cancel \(cancel_array_section0.count)")
        print("august cancel content \(cancel_array_section0)")
        
        
        
       /* let attributeValue1 = orderedSet.object(at: 1)
        let namePredicate1 =
            NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
        let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
        filteredArray_month1 = filteredArray1 as! NSMutableArray
        print("july \(filteredArray_month1.count)")*/ // month greater than 1
        
        //Get web transactions for july
        let webFilter1 = "Pending"
        let webPredicate1 = NSPredicate(format: "transactionStatus like %@",webFilter1 as CVarArg)
        let webfilteredArray1 = filteredArray_month1.filter { webPredicate1.evaluate(with: $0) };
        cancel_array_section1 = webfilteredArray1 as! NSMutableArray
        print("july cancel \(cancel_array_section1.count)")
        print("august cancel content \(cancel_array_section1)")
        
        //GET DATE AND TIME FOR TRANSACTIONS
        date_array_section0 = cancel_array_section0.value(forKey: "trasanctionTime") as! NSArray
        date_array_section1 = cancel_array_section1.value(forKey: "trasanctionTime") as! NSArray
        
        title_array_section0 = cancel_array_section0.value(forKey: "title") as! NSArray
        title_array_section1 = cancel_array_section1.value(forKey: "title") as! NSArray
        
        amount_array_section0 = cancel_array_section0.value(forKey: "amount") as! NSArray
        amount_array_section1 = cancel_array_section1.value(forKey: "amount") as! NSArray
        
        transactionId_array_section0 = cancel_array_section0.value(forKey: "transactionId") as! NSArray
        transactionId_array_section1 = cancel_array_section1.value(forKey: "transactionId") as! NSArray
        
        tableView?.reloadData()
       
    }

    
    // MARK:  TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
       var sectionNumber : Int = 0
        switch(tag){
        case 1:
            sectionNumber = orderedSet.count
        case 2 :
            sectionNumber =  orderedSet.count
        case 3 :
            sectionNumber = orderedSet.count
        case 4 :
            sectionNumber =  orderedSet.count
        case 5 :
             sectionNumber = orderedSet.count
        default :
            break
        }
        
        return sectionNumber
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var rowNumber : Int = 0
        switch(tag)
        {
        case 1 :
            if(section == 0){
                rowNumber = filteredArray_month.count
                print("arr count >>: \(filteredArray_month.count)")
            }
            else{
              //  rowNumber = filteredArray_month1.count
            }
        case 2 :
            print("paid transaction array list \(Newpaid_array_section0.count)")
            
            if(section == 0){
                rowNumber = paid_array_section0.count
                print("paid array count august >>: \(paid_array_section0.count)")
            }
            else{
                rowNumber = paid_array_section1.count
            }

        case 3 :
            print("received transaction array list \(received_array_section0.count)")
            
            if(section == 0){
                rowNumber = received_array_section0.count
                print("received array count august >>: \(received_array_section0.count)")
            }
            else{
                rowNumber = received_array_section1.count
            }

        case 4 :
            print("web transaction array list \(web_array_section0.count)")
            
            if(section == 0){
                rowNumber = web_array_section0.count
                print("web array count august >>: \(web_array_section0.count)")
            }
            else{
                rowNumber = web_array_section1.count
            }
            
        case 5 :
            print("cancel transaction array list \(cancel_array_section0.count)")
            
            if(section == 0){
                rowNumber = cancel_array_section0.count
                print("cancel array count august >>: \(cancel_array_section0.count)")
            }
            else{
                rowNumber = cancel_array_section1.count
            }

        default :
            break
        }
        return rowNumber
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! TransactionHistoryCell!
        
        switch (indexPath.section)
        {
        case 0:
            
            if(tag == 1)
            {//for mobile number
                print("all tab")
            cell?.date_Label?.text = date_array_section0[indexPath.row] as? String
            cell?.title_Label?.text = title_array_section0[indexPath.row] as? String
            cell?.amount_Label?.text = amount_array_section0[indexPath.row] as? String
            cell?.transactionId_value?.text = transactionId_array_section0[indexPath.row] as? String
            cell?.transactionId_Label?.isHidden = false
            cell?.transactionId_value?.isHidden = false
            cell?.url_Label?.isHidden = false
                if(cell?.detailsShow == true)
                {
                    cellHeight?.constant = 120
                }
                else{
                    cellHeight?.constant = 80
                }
            cell?.manageSpacing?.constant = 50
            cell?.myInit()
            cell?.delegate = self
            }
                
            else if (tag == 2)
            {
                cell?.date_Label?.text = date_array_section0[indexPath.row] as? String
                cell?.title_Label?.text = title_array_section0[indexPath.row] as? String
                cell?.amount_Label?.text = amount_array_section0[indexPath.row] as? String
                cell?.transactionId_value?.text = transactionId_array_section0[indexPath.row] as? String
                cell?.transactionId_Label?.isHidden = false
                cell?.transactionId_value?.isHidden = false
                cell?.url_Label?.isHidden = false
                cell?.manageSpacing?.constant = 50
                cell?.myInit()
                cell?.delegate = self
            }

                
            else if (tag == 3)
            {
                cell?.date_Label?.text = date_array_section0[indexPath.row] as? String
                cell?.title_Label?.text = title_array_section0[indexPath.row] as? String
                cell?.amount_Label?.text = amount_array_section0[indexPath.row] as? String
                cell?.transactionId_value?.text = transactionId_array_section0[indexPath.row] as? String
                cell?.transactionId_Label?.isHidden = false
                cell?.transactionId_value?.isHidden = false
                cell?.url_Label?.isHidden = false
                cell?.manageSpacing?.constant = 50
                cell?.myInit()
                cell?.delegate = self
            }

            else if (tag == 4)
            {
                cell?.date_Label?.text = date_array_section0[indexPath.row] as? String
                cell?.title_Label?.text = title_array_section0[indexPath.row] as? String
                cell?.amount_Label?.text = amount_array_section0[indexPath.row] as? String
                cell?.transactionId_value?.text = transactionId_array_section0[indexPath.row] as? String
                cell?.transactionId_Label?.isHidden = false
                cell?.transactionId_value?.isHidden = false
                cell?.url_Label?.isHidden = false
                cell?.manageSpacing?.constant = 50
                cell?.myInit()
                cell?.delegate = self
            }
            
            else if (tag == 5)
            {
                cell?.date_Label?.text = date_array_section0[indexPath.row] as? String
                cell?.title_Label?.text = title_array_section0[indexPath.row] as? String
                cell?.amount_Label?.text = amount_array_section0[indexPath.row] as? String
                cell?.transactionId_value?.text = transactionId_array_section0[indexPath.row] as? String
                cell?.transactionId_Label?.isHidden = false
                cell?.transactionId_value?.isHidden = false
                cell?.url_Label?.isHidden = false
                cell?.manageSpacing?.constant = 50
                cell?.myInit()
                cell?.delegate = self
            }

            
        case 1 :
            
            cell?.date_Label?.text = date_array_section1[indexPath.row] as? String
            cell?.title_Label?.text = title_array_section1[indexPath.row] as? String
            cell?.amount_Label?.text = amount_array_section1[indexPath.row] as? String
             cell?.transactionId_value?.text = transactionId_array_section1[indexPath.row] as? String
            cell?.transactionId_Label?.isHidden = false
            cell?.transactionId_value?.isHidden = false
            cell?.url_Label?.isHidden = false
            cell?.manageSpacing?.constant = 50
            cell?.myInit()
            cell?.delegate = self
            
        default:
            break
        }
        return cell!
    }
    
    // MARK: - my cell delegate
    func moreTapped(cell: TransactionHistoryCell) {
        // this will "refresh" the row heights, without reloading
        print("selecte cell : \(cell)")
         print("height of cell \(cell.manageHeight!)")
        cellHeight = cell.manageHeight!
        
        print("check \(cellHeight)")
        viewDetailsClicked = true
        tableView?.beginUpdates()
        tableView?.endUpdates()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! TransactionHistoryCell!
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
            cell?.transactionId_Label?.isHidden = true
            cell?.transactionId_value?.isHidden = true
        }else{
            selectedIndex = indexPath.row
            cell?.transactionId_Label?.isHidden = false
            cell?.transactionId_value?.isHidden = false
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! TransactionHistoryCell!
//        
//        let titleheight = cell?.title_Label?.frame.size.height
//        let dateHeight = cell?.date_Label?.frame.size.height
//        let buttonHeight = cell?.viewDetailsButton?.frame.size.height
//        let transactionHeight = cell?.manageHeight?.constant
//        
//        let height = titleheight! + dateHeight! + buttonHeight! + transactionHeight!
//        
//        print("bool value of button \(String(describing: cell?.detailsShow))")
//        if(cell?.detailsShow == false)
//        { print("120")
//            return 120
//        }
//        else{
//            print("100")
//            return 100

//        }       
//       if let height = cellHeight?.constant
//        print("height \(height)")
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    //MARK: Header for section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(tableView.frame.size.width), height: CGFloat(18)))
        /* Create custom view to display section header... */
        let label = UILabel(frame: CGRect(x: appConstants.screenWidth/2-20, y: CGFloat(5), width: CGFloat(tableView.frame.size.width), height: CGFloat(20)))
       
        /* Section header is in 0th index... */
       print("section: \(section)")
        print("check : \(orderedSet.object(at: 0))")
       label.font = UIFont(name: "System", size: 8)
        if(section == 0)
        {
        label.text = orderedSet.object(at: section) as? String
            print(label.text!)
        }
        else if (section == 1)
        {
           label.text = orderedSet.object(at: section) as? String
             print(label.text!)
        }
        else{
            label.text = orderedSet.object(at: section) as? String
        }
    
      view.addSubview(label)
        view.backgroundColor = UIColor.groupTableViewBackground
        //your background color...
        return view
    }
    
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    
    //MARK: API Response call
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            appConstants.hideLoadingHUD(for_view: self.view)
            var responseArray : NSArray = value as! NSArray
            
            for dict in responseArray{
              //  print("dict12 \(dict)")
                let responseDict = dict as! NSDictionary
            month_title = responseDict.value(forKey: "month_title") as! NSString
            month_array.add(month_title)
            history_monthwise.add(dict)
                
            transaction_status_string = responseDict.value(forKey: "transactionStatus") as! NSString
          transaction_status_array.add(transaction_status_string)
            }
            
             print("month \(month_array.count)")
             print("history_monthwise count  \(history_monthwise.count)")
            
            //MARK: To remove duplicate values from array
            var array = [Any]() /* capacity: originalArray.count */
            let enumerator: NSEnumerator? = (month_array as NSArray).reverseObjectEnumerator()
            for element: Any in enumerator! {
                array.append(element)
            }
            orderedSet = NSOrderedSet(array: array)
            orderedSet = orderedSet.reversed
            print("orderset \(orderedSet.count)")//to get the list of months
           
            let attributeValue = orderedSet.object(at: 0)
            
            let namePredicate =
                NSPredicate(format: "month_title like %@",attributeValue as! CVarArg);
            
            let filteredArray = history_monthwise.filter { namePredicate.evaluate(with: $0) };
            filteredArray_month = filteredArray as! NSMutableArray
            print("month history array count \(filteredArray_month.count)")
            
            
         /*   let attributeValue1 = orderedSet.object(at: 1)
            let namePredicate1 =
                NSPredicate(format: "month_title like %@",attributeValue1 as! CVarArg);
            
            let filteredArray1 = history_monthwise.filter { namePredicate1.evaluate(with: $0) };
            filteredArray_month1 = filteredArray1 as! NSMutableArray
            print("july array count \(filteredArray_month1.count)")*/ // for months more than 1
            
  
            
            date_array_section0 = filteredArray_month.value(forKey: "trasanctionTime") as! NSArray
            date_array_section1 = filteredArray_month1.value(forKey: "trasanctionTime") as! NSArray
            
            title_array_section0 = filteredArray_month.value(forKey: "title") as! NSArray
           title_array_section1 = filteredArray_month1.value(forKey: "title") as! NSArray
            
           amount_array_section0 = filteredArray_month.value(forKey: "amount") as! NSArray
           amount_array_section1 = filteredArray_month1.value(forKey: "amount") as! NSArray
            
            transactionId_array_section0 = filteredArray_month.value(forKey: "transactionId") as! NSArray
            transactionId_array_section1 = filteredArray_month1.value(forKey: "transactionId") as! NSArray
            tableView?.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
