//
//  RequestPaymentToQRCodeVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Pooja. All rights reserved.
//

import UIKit

class RequestPaymentToQRCodeVC: UIViewController , UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet weak var amount_textField : UITextField?
    @IBOutlet weak var comment_textField : UITextField?
    @IBOutlet weak var next_Button : UIButton?
    @IBOutlet weak var amount_view : UIView?
    @IBOutlet weak var comment_view : UIView?
     @IBOutlet weak var enter_amount_label : UILabel?
    @IBOutlet weak var amount_Requested_label : UILabel?
    @IBOutlet weak var amount_label : UILabel?
     @IBOutlet weak var comment_label : UILabel?
    
    
    @IBOutlet weak var imgQRCode: UIImageView!
   
    var qrcodeImage: CIImage!
    
    // @IBOutlet var menuButton:UIBarButtonItem!
    
    var amount_String : NSString = ""
    var comments_String : NSString = ""
    var MessageString : NSString = ""
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    
    override func viewWillAppear(_ animated: Bool) {
        imgQRCode.isHidden = true
        amount_Requested_label?.isHidden = true
        amount_label?.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiManager = ApiManager()
        apiManager.delegate = self
        
        if revealViewController() != nil {
            revealViewController().rearViewRevealWidth = 0
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
            tapRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
            self.view.addGestureRecognizer(tapRecognizer)
        }
        
        // Do any additional setup after loading the view.
    }
    //MARK: Next Button Clicked
    @IBAction func NextButtonClicked (sender: UIButton)
    {
        amount_String = amount_textField!.text! as NSString
        appConstants.defaults.set(amount_String, forKey: "amountToRequest")
        print(amount_String)
        let myDouble = NumberFormatter().number(from: amount_String as String)?.doubleValue
        if(myDouble?.isLessThanOrEqualTo(1.0))!
        {
            print("value is less than 0")
            MessageString = "Please Enter Valid Amount"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
            
        else if (amount_String != "")
        {
             generateQRCode()
        }
        else{
            MessageString = "Please Enter Amount To Send"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
    }
    //MARK: To generate QR Code
    func generateQRCode()
    {
        let userId = appConstants.defaults.value(forKey: "userId") as! String
        let mobileNo = appConstants.defaults.value(forKey: "mobile") as! String
        
        let json = NSMutableDictionary()
        json.setValue(userId, forKey: "userId")
        json.setValue(mobileNo, forKey : "mobile")
        json.setValue(amount_String, forKey: "amount")
        json.setValue("", forKey: "comments")
        
        print("json to send message = \(json)")
        
        let data1 = self.convertDictionaryToString(dict: json)
        print (data1 as Any)
        
        let data = data1?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        let encoded = data?.base64EncodedData()
        
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(encoded, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        qrcodeImage = filter!.outputImage
        
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: 10, y: 10))
        
        let topImage = UIImage(ciImage: transformedImage)
        let bottomImage = UIImage(named: "ic_qr_logo")
        
        let size = CGSize(width: (topImage.size.width), height: (topImage.size.height) + bottomImage!.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        topImage.draw(in:CGRect(x: 0, y: 0, width: size.width, height: (topImage.size.height)))
        
        bottomImage!.draw(in: CGRect(x:(topImage.size.width)/2-60,y:(topImage.size.height)/2-20,width:150, height:30))
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        //Image compression
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((newImage), 0.02)!)
        let imageSize: Int = imgData.length
        print("size of image in KB: %f ", Double(imageSize) / 1024.0)
        imgQRCode.image = UIImage(data: imgData as Data)
        imgQRCode.isHidden = false
        next_Button?.isHidden = true
        amount_view?.isHidden = true
        comment_view?.isHidden = true
        comment_label?.isHidden = true
        enter_amount_label?.isHidden = true
        amount_Requested_label?.isHidden = false
        amount_label?.isHidden = false
        amount_Requested_label?.text = "Requested Amount"
        amount_label?.text = amount_String as String
    }
   //MARK: To convert string to dictionary
    
    func convertDictionaryToString(dict: NSDictionary) -> String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return json as String
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }

    
    //MARK: Back Button
    @IBAction func BackClicked(sender : UIButton)
    {
        print("BACK PRESSED")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
            
        case 0: //for amount
            let maxLength = 5
            let currentString: NSString = amount_textField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }
    
    
    //MARK: To validate mobile number
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("validate mobile result \(result)")
        return result
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
