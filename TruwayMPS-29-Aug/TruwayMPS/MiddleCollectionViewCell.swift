//
//  MiddleCollectionViewCell.swift
//  TruwayMPS
//
//  Created by Pooja on 24/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class MiddleCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var MiddleCellImage: UIImageView!
    @IBOutlet weak var MiddleCellTitleLabel: UILabel!
}
