//
//  PaymentViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 22/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,apiManagerDelegate{
    
    @IBOutlet weak var amount_label : UILabel?
    @IBOutlet weak var amount_textField : UITextField?
    @IBOutlet weak var mobile_lable : UILabel?
    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var passcode_d1_textField : UITextField?
    @IBOutlet weak var passcode_d2_textField : UITextField?
    @IBOutlet weak var passcode_d3_textField : UITextField?
    @IBOutlet weak var passcode_d4_textField : UITextField?
    @IBOutlet weak var passcode_d5_textField : UITextField?
    @IBOutlet weak var passcode_d6_textField : UITextField?
    @IBOutlet weak var cancel_btn : UIButton?
    @IBOutlet weak var confirm_btn : UIButton?
    
    var appConstants : AppConstants = AppConstants()
    var fromScreen = ""
    var fromNotification = ""
    
    var apiManager : ApiManager = ApiManager()
    
    override func viewWillAppear(_ animated: Bool) {
        
        passcode_d1_textField?.delegate = self
        mobile_lable?.text = appConstants.defaults.value(forKey: "receiversMobile") as? String
        let amount_string = appConstants.defaults.value(forKey: "amountToSend") as? String
        if(amount_string == "")
        {
            amount_textField?.isUserInteractionEnabled = true
        }
        else{
            amount_textField?.isUserInteractionEnabled = false
            amount_textField?.text = amount_string
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Button
    @IBAction func BackClicked (sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        // SWRevealViewController.fromScreen = "SEND PAYMENT SUCCESSFUL"
        self.present(SWRevealViewController, animated:false, completion:nil)
        print("BACKKKKKKKKK")
    }
    
    //MARK: CANCEL Button
    @IBAction func CancelClicked (sender : UIButton)
    {
        
          self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: CONFIRM Button
    @IBAction func ConfirmClicked (sender : UIButton)
    {
        
        if(passcode_d1_textField?.text != "")
        {
            let passwordStr:String = (passcode_d1_textField?.text!)! + (passcode_d2_textField!.text! as String) + (passcode_d3_textField?.text!)! +
                (passcode_d4_textField?.text!)! + (passcode_d5_textField?.text!)! + (passcode_d6_textField?.text!)!
            
            print("passcode entered \(passwordStr)")
            let passwordFromDefaults = appConstants.defaults.value(forKey: "password") as! String
            if(passwordStr != passwordFromDefaults)
            {
                let MessageString = "Please Enter Correct Passcode"
                appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
            else{
                //Call Api
                CallApiToSendMoney()
            }
        }
        else{
            let MessageString = "Please Enter App Passcode"
            appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
        }
    }
    
    //MARK Completion
    func completion()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MainScreenViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        MainScreenViewController.fromScreen = "SEND PAYMENT SUCCESSFUL"
        print("SUCCESS MAIN VIEW")
    }
    
    //MARK: To Call Api
    func CallApiToSendMoney()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        let mobile = appConstants.defaults.value(forKey: "mobile") as? String
        let amount : String =  appConstants.defaults.value(forKey: "amountToSend") as! String
        let receiver_mobile : String = appConstants.defaults.value(forKey: "receiversMobile") as! String
        var transactionId : String = ""
        if let transactionID : String = appConstants.defaults.value(forKey: "transactionId") as? String
        {
            transactionId  = appConstants.defaults.value(forKey: "transactionId") as! String
        }
        else{
            transactionId  = ""
        }
        print("transactionId : \(transactionId)")
        let paymentMethod : String = appConstants.defaults.value(forKey: "paymentMethod") as! String
        
        appConstants.defaults.set("0", forKey: "FromNotifications")
        appConstants.showLoadingHUD(to_view: self.view)
        self.apiManager.ApiSendMoneyToMobileCall (action:"paymentSend",amount:amount ,receiverMobile:receiver_mobile,comments:"" as String,userId:userId as! String,mobile:mobile!,paymentType:paymentMethod, transactionId: transactionId)
    }
    
    //MARK: To cancel payment request
    func CallApiCancelRequest()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        let mobile = appConstants.defaults.value(forKey: "mobile") as? String
        let amount : String =  appConstants.defaults.value(forKey: "amountToSend") as! String
        let receiver_mobile : String = appConstants.defaults.value(forKey: "receiversMobile") as! String
        let transactionId : String = appConstants.defaults.value(forKey: "transactionId") as! String
        
        self.apiManager.ApiCancelRequestCall (action:"cancelRequest",amount:amount ,requested_mobile:receiver_mobile,userId:userId as! String,mobile:mobile!,transactionId: transactionId)
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            appConstants.hideLoadingHUD(for_view: self.view)
            appConstants.defaults.set("", forKey: "transactionId")
            if let responseDictionary : NSDictionary = value as? NSDictionary
            {
                let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
                if(responseMessage.isEqual(to: "success"))
                    
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    SWRevealViewController.fromScreen = "SEND PAYMENT SUCCESSFUL"
                    self.present(SWRevealViewController, animated:false, completion:{() in self.completion()})
                }
                else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    SWRevealViewController.fromScreen = "SEND PAYMENT FAILED"
                    self.present(SWRevealViewController, animated:false, completion:{() in self.completion()})
                }
            }
            else{
                let MessageString = "something went wrong"
                appConstants.showAlert(title: "Warning", message: MessageString as String, controller: self)
            }
        }
    }
    
    // MARK:  TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! PaymentOptionsTableViewCell!
        if indexPath.row == 0
        {
            if let selectedCardName = appConstants.defaults.value(forKey: "selectedCard")
            {
                cell?.PaymentOptionsLabel.text = selectedCardName as? String
            }
            else{
                cell?.PaymentOptionsLabel.text = "CREDIT CARD / DEBIT CARD"
            }
        }
        else if( indexPath.row == 1)
        {
            cell?.PaymentOptionsLabel.text = "CHOOSE ANOTHER CARD"
        }
        else if( indexPath.row == 2)
        {
            cell?.PaymentOptionsLabel.text = "UPI"
        }
        else{
            cell?.PaymentOptionsLabel.text = "DIRECT BANK ACCOUNT"
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print(indexPath.row)
        if(indexPath.row == 1)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let CardVaultVC = storyboard.instantiateViewController(withIdentifier: "CardVaultVC") as! CardVaultVC
            CardVaultVC.fromScreen = ""
            self.present(CardVaultVC, animated: false, completion: nil)
        }
        
    }
    //MARK: To limit characters in text field And move to next field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if(textField.tag == 7)
        {
            
        }
        else
            if ((textField.text?.characters.count)! < 1  && string.characters.count > 0){
                let nextTag = textField.tag + 1;
                
                // get next responder
                let nextResponder = textField.superview?.viewWithTag(nextTag);
                textField.text = string;
                
                if (nextResponder == nil){
                    textField.resignFirstResponder()
                }
                nextResponder?.becomeFirstResponder();
                return false;
            }
            else if ((textField.text?.characters.count)! >= 1  && string.characters.count == 0){
                // on deleting value from Textfield
                let previousTag = textField.tag - 1;
                
                // get next responder
                var previousResponder = textField.superview?.viewWithTag(previousTag);
                
                if (previousResponder == nil){
                    previousResponder = textField.superview?.viewWithTag(1);
                }
                textField.text = "";
                previousResponder?.becomeFirstResponder();
                return false;
        }
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
