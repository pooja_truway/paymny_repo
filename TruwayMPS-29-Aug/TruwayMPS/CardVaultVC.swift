//
//  CardVaultVC.swift
//  TruwayMPS
//
//  Created by Pooja on 12/06/17.
//  Copyright © 2017 Pooja. All rights reserved.
//
import UIKit

class CardVaultVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var CardListTableview : UITableView?
    @IBOutlet weak var addCardView : UIView?
    @IBOutlet weak var headerView : UIView?
    @IBOutlet weak var spacingManager : NSLayoutConstraint?
    
    @IBOutlet weak var radioButton : UIButton?
    var CardListArrayFromDefaults : NSMutableArray = []
    
    var appConstants : AppConstants = AppConstants()
    var fromScreen : NSString = ""
    var selectedIndex: Int = 0
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
     if let CardListArrayFromDefaults =  appConstants.defaults.value(forKey:"cardListArray")
     {
    print("Card List Array Count \((CardListArrayFromDefaults as AnyObject).count)")
        spacingManager?.constant = 190
        CardListTableview?.isHidden = false
    }
     else{
        print("No Cards Added to list")
        spacingManager?.constant = 50
        CardListTableview?.isHidden = true
        }
        
    }
    
    //MARK: Add Card Details
   @IBAction func AddCards()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let AddCardDetailsVC = storyboard.instantiateViewController(withIdentifier: "AddCardDetailsVC") as! AddCardDetailsVC
      //  self.present(AddCardDetailsVC, animated:false, completion:nil)
        
        AddCardDetailsVC.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(AddCardDetailsVC)
        self.view.addSubview(AddCardDetailsVC.view)
        AddCardDetailsVC.didMove(toParentViewController: self)
        
    }
    
    //MARK: Back Button
    @IBAction func Back_Button()
    {
        if fromScreen == "CardAddedScreen"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.present(SWRevealViewController, animated:false, completion:nil)
        }
        else{
              self.dismiss(animated: false, completion: {})
           }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
// MARK:  TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       if let CardListArrayFromDefaults =  appConstants.defaults.value(forKey:"cardListArray")
        {
            let array = appConstants.defaults.value(forKey: "cardListArray") as! NSArray
            self.CardListArrayFromDefaults = NSMutableArray(array: array)
            print(">>>>>>>>>>>>>>:\(CardListArrayFromDefaults)")
        }
        
        if fromScreen == "ConfirmPayment"
        {
            return 3
        }
        else{
       return  CardListArrayFromDefaults.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! CardListTableViewCell!
        
        if appConstants.defaults.value(forKey:"cardListArray") != nil
        {
           let arr  = (appConstants.defaults.value(forKey: "cardListArray") as! NSArray).mutableCopy() as! NSMutableArray
            let cardName : NSArray = arr.value(forKey: "cardSavedAs") as! NSArray
            print("Card List\(cardName)")
            
           if let index =  appConstants.defaults.value(forKey: "indexOfSelectedCard")
           {
            let SelecetdIndexPath = index as! Int
            if(indexPath.row == SelecetdIndexPath)
            {
            cell?.radioButton?.setImage(UIImage(named: "ic_radio_btn_selected"),for:UIControlState.normal)
            }
            else{
              cell?.radioButton?.setImage(UIImage(named: "ic_radio_btn_unselected"),for:UIControlState.normal)
            }
        }
           else {
            if(indexPath.row == 0)
            {
            let image = UIImage(named: "ic_radio_btn_selected") as UIImage?
            cell?.radioButton?.setImage(image, for: UIControlState())
            }
        }
            let card1Name : NSDictionary = arr.object(at: 0) as! NSDictionary
            let str = card1Name.value(forKey: "cardSavedAs")
            appConstants.defaults.set(str, forKey: "selectedCard")
                
            cell?.CardNameLabel.text = cardName[indexPath.row] as? String
             selectedIndex = indexPath.row
            cell?.deleteButton?.addTarget(self, action: #selector(CardVaultVC.DeleteRow), for: .touchUpInside)
        }
        else{
        cell?.CardNameLabel.text = "Master Card"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        print(indexPath.row)
         let cell = tableView.cellForRow(at: indexPath)  as! CardListTableViewCell!
                    let image = UIImage(named: "ic_radio_btn_selected") as UIImage?
            cell?.radioButton?.setImage(image, for: UIControlState())
        appConstants.defaults.set(cell?.CardNameLabel.text, forKey: "selectedCard")
        appConstants.defaults.set(indexPath.row, forKey: "indexOfSelectedCard")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)  as! CardListTableViewCell!
        let image = UIImage(named: "ic_radio_btn_unselected") as UIImage?
        cell?.radioButton?.setImage(image, for: UIControlState())
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
   //MARK: Delete Table Row
    func DeleteRow() {
        self.CardListArrayFromDefaults.removeObject(at: selectedIndex)
        self.appConstants.defaults.set(self.CardListArrayFromDefaults, forKey: "cardListArray")
        print("New Card List After Delete \(self.CardListArrayFromDefaults)")
        self.CardListTableview?.reloadData()
    }
    
     //MARK: TO Add SWIPE Delete Feature
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        
//        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
//            //self.isEditing = false
//            print("delete button tapped  \(indexPath.row)")
//            
//            self.CardListArrayFromDefaults.removeObject(at: indexPath.row)
//            self.appConstants.defaults.set(self.CardListArrayFromDefaults, forKey: "cardListArray")
//            print("New Card List After Delete \(self.CardListArrayFromDefaults)")
//            self.CardListTableview?.reloadData()
//        }
//        delete.backgroundColor = UIColor.red
//        return [delete]
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
