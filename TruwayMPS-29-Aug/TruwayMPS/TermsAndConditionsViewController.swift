//
//  TermsAndConditionsViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 22/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Back Button Action
    @IBAction func BackButtonClick()
    {
        self.dismiss(animated: false, completion: {})
    }

    //MARK: Back Button Action
    @IBAction func ContinueButtonClick()
    {
        self.dismiss(animated: false, completion: {})
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
