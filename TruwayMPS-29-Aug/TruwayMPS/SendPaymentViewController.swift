//
//  SendPaymentViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 19/06/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class SendPaymentViewController: UIViewController,UITextFieldDelegate,apiManagerDelegate {

    @IBOutlet weak var mobileTextField : UITextField?
    @IBOutlet weak var amountTextField : UITextField?
    @IBOutlet weak var background_view : UIView?
    
    var mobile_string : String = ""
    var amount_string : String = ""
    var fromScreen : NSString = ""
    
    var appConstants : AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let checkIfFromNotification : NSString = appConstants.defaults.value(forKey: "Notify") as?  NSString
       {
        if checkIfFromNotification == "fromNotify"
        {
       
      mobileTextField?.text = appConstants.defaults.value(forKey: "receiversMobile") as? String
      amountTextField?.text  = appConstants.defaults.value(forKey: "amountToSend") as? String
       }
        }
        
        background_view?.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color)
        background_view?.layer.cornerRadius = 5.0
    }
    override func viewDidDisappear(_ animated: Bool) {
       // appConstants.defaults.set("", forKey: "receiversMobile")
       // appConstants.defaults.set("", forKey: "amountToSend")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func back ()
    {
        self.dismiss(animated: true, completion: nil)
     }
    
    @IBAction func ApiCall(sender : UIButton)
    {
        mobile_string  = (mobileTextField?.text)!
        amount_string  = (amountTextField?.text)!
        print("mobile : \(mobile_string)")
        print("amount : \(amount_string)")
        
        appConstants.defaults.set(mobile_string, forKey: "receiversMobile")
        appConstants.defaults.set(amount_string, forKey: "amountToSend")
        
        if (amount_string != "")
        {
            if(mobile_string != "")
            {
                CallApiToSendMoneyDemo()
            }
        }
    }
    
    func CallApiToSendMoneyDemo()
    {
        let userId = appConstants.defaults.value(forKey: "userId")
        let mobile = appConstants.defaults.value(forKey: "mobile") as? String
        print("mobile 1 : \(String(describing: mobile))")
        
        self.apiManager.ApiSendMoneyDemo (action:"tempSendpayment",
                                          userId:userId as! String,
                                          mobile:mobile!,
                                          payee_mobile:mobile_string as String
            ,amount:amount_string as String)
    }
    
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            print("message : \(responseMessage)")
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let WebViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            WebViewController.weblink = responseMessage
            self.present(WebViewController, animated:false, completion:nil)
        }
    }
    
    //MARK: To limit characters in text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        switch (textField.tag){
        case 0: //for mobile number
            let maxLength = 10
            let currentString: NSString = mobileTextField!.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        default:
            break
        }
        return true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
