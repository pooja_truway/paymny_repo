//
//  UIView.swift
//  TruwayMPS
//
//  Created by Pooja on 09/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

extension UIView{
    
    func AddBorderToView(radius:CGFloat, color:UIColor = UIColor.clear) -> UIView{
        let rounfView:UIView = self
        rounfView.layer.cornerRadius = CGFloat(radius)
        rounfView.layer.borderWidth = 1
        rounfView.layer.borderColor = color.cgColor
        rounfView.clipsToBounds = true
        return rounfView
    }
    
}
