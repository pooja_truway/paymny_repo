//
//  ElectricityBillViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 22/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class ElectricityBillViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var state_TextField : UITextField?
    @IBOutlet weak var paymentButton : UIButton?
    @IBOutlet weak var electricityBoard_TextField : UITextField?
    @IBOutlet weak var consumerNumber_TextField : UITextField?
    
    @IBOutlet weak var operator_Image : UIImageView?
    @IBOutlet weak var tableView : UITableView?
    
    var fromScreen = "Main"
    
      let  CellImageArray : NSMutableArray = [
    UIImage(named: "ic_bses"),UIImage(named: "ic_tatapower")]
    
    let CellTitleArray : NSMutableArray =  ["BSES","TATA POWER"]
    //State ListArray
    let StateArrayList : NSMutableArray = ["Andra Pradesh","Bihar","Chandigarh","Delhi","Tamil Nadu"]

    
    var operatorString : NSString = ""
    var index : Int = 0
    var MessageString : String = ""
    var state_string : String = ""
    
    var appConstants : AppConstants = AppConstants()
    
    override func viewWillAppear(_ animated: Bool) {
         tableView?.isHidden = true
        electricityBoard_TextField?.text = operatorString as String
        print("index : \(index)")
        if(index == 0)
        {
            
        }
        else{
          //  index = index + 1
            operator_Image?.image = CellImageArray.object(at: index) as? UIImage
        }
        if let electricityState = appConstants.defaults.value(forKey: "electricityState")
        {
            state_TextField?.text = electricityState as? String
        }

         paymentButton?.layer.cornerRadius = 10.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func GetStateListAction()
    {
        tableView?.isHidden = false
    }
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StateArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel?.text = "\(StateArrayList[indexPath.row])"
        cell.textLabel!.font = UIFont(name: (cell.textLabel?.font.fontName)!, size:12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        state_TextField?.text = StateArrayList[indexPath.row] as? String
        state_string = (state_TextField?.text)!
        appConstants.defaults.set(state_string, forKey: "electricityState")
        tableView.isHidden = true
    }
    
    //MARK: Select Operator Button Clicked
    @IBAction func GetOperatorList(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SelectOperatorViewController = storyboard.instantiateViewController(withIdentifier: "SelectOperatorViewController") as! SelectOperatorViewController
        SelectOperatorViewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width , height: self.view.frame.height )
        self.addChildViewController(SelectOperatorViewController)
        SelectOperatorViewController.fromScreen = "Electricity"
        self.view.addSubview(SelectOperatorViewController.view)
        SelectOperatorViewController.didMove(toParentViewController: self)
    }

    //MARK: Back Clicked
    @IBAction func BackClicked(sender : UIButton)
    {
        
        if(fromScreen == "Main")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            self.present(SWRevealViewController, animated:false, completion:nil)
        }
        else{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let UtilityBillViewController = storyboard.instantiateViewController(withIdentifier: "UtilityBillViewController") as! UtilityBillViewController
        self.present(UtilityBillViewController, animated: false, completion: nil)
        }
    }
}
