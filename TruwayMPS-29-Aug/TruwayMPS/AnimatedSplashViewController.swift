//
//  AnimatedSplashViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 19/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class AnimatedSplashViewController: UIViewController {
    
    @IBOutlet var splashView : UIImageView!
     var appConstants: AppConstants = AppConstants()
    
    let i :Int = 0 // to check demo of payment
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
       splashView = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width:appConstants.screenWidth , height: appConstants.screenHeight))
        splashView.image = UIImage(named: "Launch")
        self.view.addSubview(splashView)
        self.view.bringSubview(toFront: splashView)
        self.fade_In()
    }
    
    func fade_In()
    {
        print("fade_in called")
        splashView.alpha = 0.2
        UIView.animate(withDuration: 2.0, delay: 0.2, options: .transitionCrossDissolve, animations: {() -> Void in
            self.splashView.alpha = 1.0
        }, completion: { (_ finished: Bool) -> Void in
        self.fade_out()
        })
    }
    func fade_out()
    {
        print("fade_out called")
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .transitionCrossDissolve, animations: {() -> Void in
            self.splashView.alpha = 0.4
        }, completion: {(_ finished: Bool) -> Void in
            
            if(self.i == 1)
            {
                
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let OptionsViewController = storyboard.instantiateViewController(withIdentifier: "OptionsViewController") as! OptionsViewController
                            self.present(OptionsViewController, animated:false, completion:nil)
            }
            else
            {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(ViewController, animated:false, completion:nil)
            }
            
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
