//
//  AdditionalDetailViewController.swift
//  TruwayMPS
//
//  Created by Pooja on 15/05/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class AdditionalDetailViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate, apiManagerDelegate{
    
     @IBOutlet var scrollView: UIScrollView!
     @IBOutlet var BackgroundImage: UIImageView!
     @IBOutlet var FirstName_View : UIView? = UIView()
     @IBOutlet var LastName_View : UIView? = UIView()
     @IBOutlet var Address_View : UIView? = UIView()
     @IBOutlet var City_View : UIView? = UIView()
     @IBOutlet var State_View : UIView? = UIView()
     @IBOutlet var PostalCode_View : UIView? = UIView()
     @IBOutlet var CountryName_textField : UITextField? = UITextField()
     @IBOutlet var Continue_button : UIButton!
     @IBOutlet var Back_button : UIButton!
     @IBOutlet var Address_textField : UITextField?
     @IBOutlet var firstName_textField : UITextField?
     @IBOutlet var LastName_textField : UITextField?
     @IBOutlet var City_textField : UITextField?
     @IBOutlet var State_textField : UITextField?
     @IBOutlet var PostalCode_textField : UITextField?
    
     var countryname_str : NSString = ""
     var firstName_string : NSString = ""
     var LastName_string : NSString = ""
     var Address_string : NSString = ""
     var City_string : NSString = ""
     var State_string : NSString = ""
     var PostalCode_string : NSString = ""
     var MessageString : String = ""
    
     var appConstants: AppConstants = AppConstants()
     var apiManager : ApiManager = ApiManager()
    
 
//MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
      scrollView.addSubview(FirstName_View!)
        
        // To set the alpha component of views
        FirstName_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        LastName_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        Address_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        City_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        State_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
        PostalCode_View?.backgroundColor = UIColor.black .withAlphaComponent(0.05)
                
         Continue_button.backgroundColor = UIColor().HexToColor(hexString: appConstants.SignIn_button_color).withAlphaComponent(1)
        
        countryname_str = appConstants.defaults.value(forKey: "CountryName") as! NSString
        print(countryname_str)
        CountryName_textField?.text = countryname_str as String
        Back_button.isHidden = true //To Hide Back Button
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiManager = ApiManager()
        apiManager.delegate = self
        self.scrollView.delegate = self
        
    }
    
   @IBAction func Back_Button()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let RegisterPageViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterPageViewController") as! RegisterPageViewController
        self.present(RegisterPageViewController, animated:false, completion:nil)
    }
    
   @IBAction func Continue()
   {
    firstName_string = firstName_textField!.text! as NSString
    LastName_string = LastName_textField!.text! as NSString
    Address_string = Address_textField!.text! as NSString
    City_string = City_textField!.text! as NSString
    State_string = State_textField!.text! as NSString
    PostalCode_string = PostalCode_textField!.text! as NSString
    countryname_str = CountryName_textField!.text! as NSString
   
    
    if(firstName_string == "")||(LastName_string == "")||(Address_string == "")||(City_string == "")||(State_string == "")||(PostalCode_string == "")
    {
        MessageString = "All fields are mandatory"
        appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
    }
    else{
        AdditionalDetailApiCall()
    }
    
    }
    
 //MARK: Api Call
    func  AdditionalDetailApiCall()
    {
       let userId = appConstants.defaults.value(forKey: "userId")
       
        self.apiManager.ApiAdditionalDetailCall (action:"additionalDetails",userId:userId as! String,firstName:firstName_string as String ,lastName:LastName_string as String,address:Address_string as String,city:City_string as String,state:State_string as String,postalCode:PostalCode_string as String,countryName:countryname_str as String )
    }
    
    //MARK: To get Api Success Response
    func apiSuccessResponse(_ response : Dictionary<String,AnyObject>){
        for (key, value) in response {
            print("key=\(key), value=\(value)")
            let responseDictionary : NSDictionary = value as! NSDictionary
            
            let responseMessage : NSString = responseDictionary .value(forKey: "message") as! NSString
            if(responseMessage.isEqual(to: "success"))
            {
              // SecurityQuestionApiCall()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let VerificationInfoViewController = storyboard.instantiateViewController(withIdentifier: "VerificationInfoViewController") as! VerificationInfoViewController
                    self.present(VerificationInfoViewController, animated:false, completion:nil)

            }
            else{
                MessageString = responseMessage as String
                appConstants.showAlert(title: "Warning", message: MessageString, controller: self)
            }
        //MARK: After getting response move to verification page
        }
    }
    
 //MARK: Security Question api Call
func  SecurityQuestionApiCall()
{
self.apiManager.ApiSecurityQuestionCall(action:"securityQuestion")
}

//MARK: To Stop Horizontal Scroll of Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
        else{
            scrollView.isDirectionalLockEnabled = true
        }
    }
    
 //MARK: To Handle textField
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstName_textField {
            textField.resignFirstResponder()
            LastName_textField?.becomeFirstResponder()
        } else if textField == LastName_textField {
             textField.resignFirstResponder()
            Address_textField?.becomeFirstResponder()
        }
        else if textField == Address_textField {
            textField.resignFirstResponder()
            City_textField?.becomeFirstResponder()
    }
        else if textField == City_textField {
            textField.resignFirstResponder()
            State_textField?.becomeFirstResponder()
    }
        else if textField == State_textField {
            textField.resignFirstResponder()
            PostalCode_textField?.becomeFirstResponder()
    }
    
        return true
}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
