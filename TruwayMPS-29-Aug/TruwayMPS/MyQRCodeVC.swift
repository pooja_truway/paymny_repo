//
//  MyQRCodeVC.swift
//  TruwayMPS
//
//  Created by Pooja on 04/07/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit
import MessageUI



class MyQRCodeVC: UIViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var imgQRCodeWithLogo: UIImageView?
    @IBOutlet weak var logoImage: UIImageView!
    var qrcodeImage: CIImage!
    var appConstants : AppConstants = AppConstants()
    
    override func viewWillAppear(_ animated: Bool) {
        performButtonAction()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func shareButtonClicked(sender: UIButton) {
      
            do {
                
                let shareText = "Pay using PayMny"
                let shareURL = URL(string: "http://paymny.com")
                
                let activityArray = [shareText, shareURL,imgQRCode.image] as [Any]
                let activityViewController = UIActivityViewController(activityItems: activityArray , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = sender
                present(activityViewController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBAction method implementation
    func performButtonAction() {
        
        let userId = appConstants.defaults.value(forKey: "userId") as! String
        let mobileNo = appConstants.defaults.value(forKey: "mobile") as! String
       
            let json = NSMutableDictionary()
            json.setValue(userId, forKey: "userId")
            json.setValue(mobileNo, forKey : "mobile")
            json.setValue("", forKey: "amount")
            json.setValue("", forKey: "comments")
            
            print("json to send message = \(json)")
            
            let data1 = self.convertDictionaryToString(dict: json)
            print (data1 as Any)
            
            let data = data1?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            let encoded = data?.base64EncodedData()
        
        
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setValue(encoded, forKey: "inputMessage")
            filter!.setValue("Q", forKey: "inputCorrectionLevel")
            qrcodeImage = filter!.outputImage
        
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: 10, y: 10))
        
        let topImage = UIImage(ciImage: transformedImage)
        let bottomImage = UIImage(named: "ic_qr_logo")
        
        let size = CGSize(width: (topImage.size.width), height: (topImage.size.height) + bottomImage!.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        topImage.draw(in:CGRect(x: 0, y: 0, width: size.width, height: (topImage.size.height)))
        
        bottomImage!.draw(in: CGRect(x:(topImage.size.width)/2-60,y:(topImage.size.height)/2-20,width:150, height:30))
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        //Image compression
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((newImage), 0.02)!)
        let imageSize: Int = imgData.length
        print("size of image in KB: %f ", Double(imageSize) / 1024.0)
        imgQRCode.image = UIImage(data: imgData as Data)
    
    }
    
    func convertDictionaryToString(dict: NSDictionary) -> String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return json as String
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    @IBAction func BackClicked(sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SWRevealViewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.present(SWRevealViewController, animated:false, completion:nil)
    }
  }
