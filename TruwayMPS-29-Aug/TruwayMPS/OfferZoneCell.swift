//
//  OfferZoneCell.swift
//  TruwayMPS
//
//  Created by Pooja on 18/08/17.
//  Copyright © 2017 Truway India. All rights reserved.
//

import UIKit

class OfferZoneCell: UITableViewCell {
    
     @IBOutlet weak var offerBannerImage: UIImageView?
    @IBOutlet weak var textMessage : UITextView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        offerBannerImage?.layer.cornerRadius = 5.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
